﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;
using UnityStandardAssets.CrossPlatformInput;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    public Image bgImg, jsImg, BoundingImg;
    public string HorizontalAxisName;
    public string VerticalAxisName;
    public Vector3 InputDirection { set; get; }
    private Vector3 BoundingInputDirection;

    CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis;
    CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis;
    bool useBound = false;

    private void Start()
    {
        //  bgImg = GetComponent<Image>();
        //  jsImg = GetComponentsInChildren<Image>()[1];
        InputDirection = Vector3.zero;
    }

    private void OnEnable()
    {
        RegisterNewAxis();
    }

    void OnDisable()
    {
        // remove the joysticks from the cross platform input
        m_HorizontalVirtualAxis.Remove();
        m_VerticalVirtualAxis.Remove();
    }

    private void RegisterNewAxis()
    {
        m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(HorizontalAxisName);
        m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(VerticalAxisName);

        CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
        CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
    }


    //EventSystems interfaces
    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos = Vector2.zero;
        Vector2 boundPos = Vector2.zero;


        if (RectTransformUtility.ScreenPointToLocalPointInRectangle
            (bgImg.rectTransform,
                ped.position,
                ped.pressEventCamera,
                out pos) && !useBound)
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            float x = (bgImg.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
            float y = (bgImg.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

            InputDirection = new Vector3(x, 0, y);
            InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;
        }


        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(BoundingImg.rectTransform, ped.position, ped.pressEventCamera, out boundPos))
        {
            boundPos.x = (boundPos.x / BoundingImg.rectTransform.sizeDelta.x);
            boundPos.y = (boundPos.y / BoundingImg.rectTransform.sizeDelta.y);

            float x1 = (BoundingImg.rectTransform.pivot.x == 1) ? boundPos.x * 2 + 1 : boundPos.x * 2 - 1;
            float y1 = (BoundingImg.rectTransform.pivot.y == 1) ? boundPos.y * 2 + 1 : boundPos.y * 2 - 1;

            BoundingInputDirection = new Vector3(x1, 0f, y1);
            BoundingInputDirection = (BoundingInputDirection.magnitude > 1) ? BoundingInputDirection.normalized : BoundingInputDirection;
            jsImg.rectTransform.anchoredPosition = new Vector3(BoundingInputDirection.x * (BoundingImg.rectTransform.sizeDelta.x / 3)
                , BoundingInputDirection.z * (BoundingImg.rectTransform.sizeDelta.y / 3));
        }

        m_HorizontalVirtualAxis.Update(useBound ? BoundingInputDirection.x : InputDirection.x);
        m_VerticalVirtualAxis.Update(useBound ? BoundingInputDirection.z : InputDirection.z);
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        Vector2 pos = Vector2.zero;
        useBound = (RectTransformUtility.ScreenPointToLocalPointInRectangle
             (BoundingImg.rectTransform,
                 ped.position,
                 ped.pressEventCamera,
                 out pos));

        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        useBound = false;
        InputDirection = Vector3.zero;
        jsImg.rectTransform.anchoredPosition = Vector3.zero;

        m_HorizontalVirtualAxis.Update(InputDirection.x);
        m_VerticalVirtualAxis.Update(InputDirection.z);
    }
}
