﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using Photon.Pun;
using TMPro;
using System;

public class randommatchhandling : MonoBehaviourPunCallbacks
{
    [SerializeField] TMP_Text waitingstatus;
    [SerializeField] TMP_Text playerdetail;
    // Start is called before the first frame update
    Action callback_startgame;
    Action<bool> callback_readygame;

    delegate void StartGames();
    event StartGames OnStartGame;

    void Start()
    {
        
    }
    public void SubsribeStartGame(Action callback)
    {
        callback_startgame = callback;
        OnStartGame += LobbyHandler_OnStartGame;
    }

    private void LobbyHandler_OnStartGame()
    {
        callback_startgame();
    }

    public override void OnJoinedRoom()
    {
        
        int playerCount = PhotonNetwork.CurrentRoom.PlayerCount;

        if (playerCount != 2)
        {
            waitingstatus.text = "Waiting For Opponent...";
            playerdetail.text = "connected"+playerCount+"/ 2";
        }
        else
        {
            waitingstatus.text = "All Opponent Found";
            playerdetail.text = "Match is ready to begin";
            if (PhotonNetwork.IsMasterClient)
            {
                //start the game
                Debug.Log("game start");
               // OnStartGame.Invoke();
                //OnStartGame();
            }
        }
    }
}
