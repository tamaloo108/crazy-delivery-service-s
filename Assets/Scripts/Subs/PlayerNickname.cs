﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class PlayerNickname : MonoBehaviour
{
    [SerializeField] PhotonView PView;
    [SerializeField] TMPro.TextMeshProUGUI NameTag;

    Coroutine NameCoroutine;
    WaitUntil PVOccupied;
    WaitForSeconds delay;

    private void Start()
    {
        PVOccupied = new WaitUntil(() => PView != null);
        delay = new WaitForSeconds(1f);

        if (NameCoroutine == null)
        {
            NameCoroutine = StartCoroutine(NameUiUpdater());
        }
    }

    private IEnumerator NameUiUpdater()
    {
        if (PView.IsMine)
        {
            gameObject.SetActive(false);
            yield break;
        }

        while (true)
        {
            yield return PVOccupied;
            NameTag.text = PView.Owner.NickName;
            yield return delay;
        }
    }
}
