﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyParentPlayer : MonoBehaviour
{
    PhotonView myView;

    private void Start()
    {
        myView = GetComponentInChildren<PhotonView>();
    }


    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
