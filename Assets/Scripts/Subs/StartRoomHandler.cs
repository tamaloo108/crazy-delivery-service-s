﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRoomHandler : MonoBehaviour
{

    [SerializeField] Transform Contents;

    public Transform GetContentsTransform()
    {
        return Contents;
    }

}
