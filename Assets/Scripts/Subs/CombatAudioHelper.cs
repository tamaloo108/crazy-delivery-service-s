﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioReverbZone))]
public class CombatAudioHelper : MonoBehaviour
{

    [SerializeField] bool MultipleAudioSources;
    [SerializeField] bool EnableVoices;

    public DriverVoice Voices;
    AudioSource Source;
    List<AudioSource> Sources = new List<AudioSource>();
    private void Start()
    {
        if (!MultipleAudioSources)
        {
            Source = GetComponent<AudioSource>();
        }
        else
        {
            var s = GetComponents<AudioSource>();
            for (int i = 0; i < s.Length; i++)
            {
                Sources.Add(s[i]);
            }
        }
    }

    public void PlaySound(string name, int index)
    {
        if (index < Sources.Count && !(index < 0))
        {
            UniversalSoundHandler.Instance.PlaySounds(name, Sources[index]);
        }
    }

    public void StopSound(int index)
    {
        UniversalSoundHandler.Instance.StopSounds(Sources[index]);
    }

    public void StopSound()
    {
        UniversalSoundHandler.Instance.StopSounds(Source);
    }

    public void PlaySound(string name)
    {
        UniversalSoundHandler.Instance.PlaySounds(name, Source);
    }

    public AudioMixer GetMixer()
    {
        return Source.outputAudioMixerGroup.audioMixer;
    }
}

[System.Serializable]
public class DriverVoice
{
    public string m_UseSkill;
    public string m_Knocked;
    public string m_Idle;
    public string m_Respond;
}