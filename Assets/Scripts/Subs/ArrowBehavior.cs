﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBehavior : MonoBehaviour
{
    ArrowState myArrowState;
    Coroutine ArrowStateCoroutine;

    WaitForEndOfFrame delay = new WaitForEndOfFrame();
    Animator anim;
    string[] AnimName;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        AnimName = new string[3] { "Hidden", "Point", "Secure" };
    }

    public void SetAnimSpeed(float multiplier)
    {
        anim.speed = multiplier;
    }

    public void SetState(ArrowState state)
    {
        myArrowState = state;
        UpdateState();
    }

    private void UpdateState()
    {
        switch (myArrowState)
        {
            case ArrowState.Hidden:
                anim.Play(AnimName[0]);
                break;

            case ArrowState.Point:
                anim.Play(AnimName[1]);
                break;

            case ArrowState.Secured:
                anim.Play(AnimName[2]);
                break;
        }

    }
}


public enum ArrowState
{
    Hidden = 0, //when not on sending pedestrian.
    Point = 1, //pointing to target place.
    Secured = 2 //already within drop point of target.
}