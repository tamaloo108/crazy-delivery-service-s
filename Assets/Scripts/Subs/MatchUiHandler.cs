﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Photon.Realtime;
using System.Linq;

public class MatchUiHandler : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI Timer;
    [SerializeField] TextMeshProUGUI MyScoreUI;
    [SerializeField] TextMeshProUGUI MyNameUI;
    [SerializeField] TextMeshProUGUI[] RankScore;
    [SerializeField] TextMeshProUGUI[] RankName;
    [SerializeField] bool SwitchViewMode = false;

    int MyScore = 0;
    bool inScoreView = false;

    Player[] PlayerMatchScores;

    private void Start()
    {
        SetLeaderboardContent();
    }

    public void SetScore(int val)
    {
        MyScore = val;
        MyScoreUI.text = MyScore.ToString();
    }

    public void SetTimerTime(int val)
    {
        Timer.text = val.ToString();
    }

    public void SetAllPlayerScores(Player[] playerScores)
    {
        try
        {
            PlayerMatchScores = playerScores.OrderByDescending(x => x.GetScore()).ToArray();

            for (int i = 0; i < PlayerMatchScores.Length; i++)
            {
                if (i < 3)
                {
                    RankScore[i].text = PlayerMatchScores[i].GetScore().ToString();
                    RankName[i].text = PlayerMatchScores[i].NickName;
                }

                if (playerScores[i].IsLocal)
                {
                    MyScoreUI.text = playerScores[i].GetScore().ToString();
                    MyNameUI.text = playerScores[i].NickName;
                }
            }

        }
        catch (Exception e)
        {

        }

    }

    void SetLeaderboardContent()
    {
        for (int i = 0; i < RankName.Length; i++)
        {
            RankName[i].enabled = SwitchViewMode ? !inScoreView : true;
            RankScore[i].enabled = SwitchViewMode ? inScoreView : true;
        }

        MyNameUI.enabled = SwitchViewMode ? !inScoreView : true;
        MyScoreUI.enabled = SwitchViewMode ? inScoreView : true;
    }

    public void SwitchToScoreView()
    {
        inScoreView = !inScoreView;
        SetLeaderboardContent();
    }
}


public static class MatchUIExtentension
{
    public static void SetMyScore(this MatchUiHandler mui, int val)
    {
        mui.SetScore(val);
    }


    public static void SetTimerTimes(this MatchUiHandler mui, int val)
    {
        mui.SetTimerTime(val);
    }
}