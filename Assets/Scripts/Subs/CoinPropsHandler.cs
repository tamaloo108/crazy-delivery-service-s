﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPropsHandler : MonoBehaviour
{
    [SerializeField] ParticleSystem CoinDisappearEffect;

    Animator anims;
    WaitForSeconds delayBeforeTransition;
    WaitForEndOfFrame delay;
    WaitForSeconds RandomDelay;

    Vector3 velo;

    float TravelSmoothTime;
    float TravelTime;
    private void Start()
    {
        anims = GetComponent<Animator>();
        delayBeforeTransition = new WaitForSeconds(.6f);
        delay = new WaitForEndOfFrame();
        RandomDelay = new WaitForSeconds(UnityEngine.Random.Range(1f, 2f));
        TravelSmoothTime = UnityEngine.Random.Range(20f, 22f);
        TravelTime = 2.25f;
    }


    public IEnumerator FlewToTarget(Transform TargetPosition)
    {
        yield return RandomDelay;
        while (TravelTime > 0f)
        {
            float dist = Vector3.Distance(transform.position, TargetPosition.position);
            if (dist < 0.1f || TravelTime <= 0.1f)
            {
                anims.SetTrigger("Disappear");
                yield return delayBeforeTransition;
                Destroy(gameObject);
                yield break;
            }
            yield return delay;

            transform.position = Vector3.MoveTowards(transform.position, TargetPosition.position, Time.deltaTime * TravelSmoothTime);
            TravelTime -= Time.deltaTime;
        }
    }

    public void PlayCoins(Transform TargetPosition)
    {
        StartCoroutine(FlewToTarget(TargetPosition));
    }

    public void PlayEffect()
    {
        CoinDisappearEffect.Simulate(0f, true, false);
        CoinDisappearEffect.Play();
    }

}
