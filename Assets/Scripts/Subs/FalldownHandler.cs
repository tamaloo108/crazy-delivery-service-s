﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;
using UnityEngine.AddressableAssets;


public class FalldownHandler : MonoBehaviour
{
    [SerializeField] AssetReference FadeScreen;
    [SerializeField] AssetReference TwinkleStar;
    StateProcessor States;

    Animator FadeScreenAnimator;
    GameObject Victim;
    Coroutine FallingDown;
    WaitForSeconds DownDelay = new WaitForSeconds(1.25f);
    WaitForSeconds FastDownDelay = new WaitForSeconds(.25f);
    private void Start()
    {
        States = new StateProcessor();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Pedestrian"))
        {
            var g = other.GetComponent<PedestrianHandler>();
            g.DoneTravelling();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player Collider"))
        {
            if (FallingDown == null)
            {
                Victim = other.transform.parent.GetChild(0).gameObject;
                FallingDown = StartCoroutine(IsDoingFall(other.transform.parent.GetChild(0)));
            }
        }
    }

    private IEnumerator IsDoingFall(Transform other)
    {
        if (FadeScreenAnimator == null)
            UniversalAddresableLoader.Instance.Spawn(FadeScreen, Vector3.zero, null, OnDoneSpawnFade);
        else
        {
            FadeScreenAnimator.gameObject.SetActive(true);
            FadeScreenAnimator.Play("FadeIn");
        }

        StartCoroutine(States.FallDownStunEffect(other.GetComponent<VehicleController>()));
        other.GetComponent<NgojekHandler>().ReleasePedestrian();
        yield return FastDownDelay;
        other.GetComponent<VehicleController>().SetVelocity(Vector3.zero);
        other.GetComponent<NgojekHandler>().FallDown();
        yield return DownDelay;
        UniversalAddresableLoader.Instance.Spawn(TwinkleStar, Vector3.zero, null, OnDoneSpawnStar);
        FadeScreenAnimator.Play("FadeOut");
        yield return FastDownDelay;
        FadeScreenAnimator.gameObject.SetActive(false);
        FallingDown = null;
    }

    private void OnDoneSpawnStar(GameObject obj)
    {
        obj.transform.SetParent(Victim.transform);
        obj.transform.localPosition = Vector3.up * 1.5f;
        Destroy(obj, 2f);
    }

    private void OnDoneSpawnFade(GameObject obj)
    {
        FadeScreenAnimator = obj.GetComponent<Animator>();

    }
}
