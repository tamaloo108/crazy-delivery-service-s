﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    [SerializeField] Transform Target;
    Coroutine FollowTo;
    WaitForSeconds Delay = new WaitForSeconds(0.1f);
    // Start is called before the first frame update
    void Start()
    {
        if (FollowTo == null)
        {
            FollowTo = StartCoroutine(FollowToTarget());
        }
    }

    private IEnumerator FollowToTarget()
    {
        while (true)
        {
            Target.SetParent(null);
            yield return Delay;
            transform.position = Target.position;
            yield return Delay;
            Target.SetParent(Target);
        }
    }
}
