﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class IndividualScoreboardHandler : MonoBehaviour
{
    
    [SerializeField] TextMeshProUGUI MyNameUI;
    [SerializeField] TextMeshProUGUI MyScoreUI;
    [SerializeField] TextMeshProUGUI MyRankUI;

    public void SetScoreboard(string MyName, string MyScore, string MyRank)
    {
        SetMyName(MyName);
        SetMyScore(MyScore);
        SetMyRank(MyRank);
    }

    private void SetMyRank(string myRank)
    {
        MyRankUI.text = myRank;
    }

    private void SetMyScore(string myScore)
    {
        MyScoreUI.text = myScore;
    }

    private void SetMyName(string myName)
    {
        MyNameUI.text = myName;
    }
}
