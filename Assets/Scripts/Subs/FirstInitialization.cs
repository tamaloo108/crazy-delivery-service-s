﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class FirstInitialization : MonoBehaviour
{

    private void Awake()
    {
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
        Debug.unityLogger.logEnabled=false;
#endif
    }

    void Start()
    {
        UniversalSceneLoader.Instance.LoadScene("GameLauncher", OnDoneLoadScene);
        Screen.SetResolution(1920, 1080, true, 60);
    }

    private void OnDoneLoadScene()
    {
        Scene myScene = SceneManager.GetSceneByName("GameLauncher");
        if (myScene.isLoaded)
        {
            SceneManager.SetActiveScene(myScene);
        }
        UniversalHandler.Instance.GlobalCamera = Camera.main;
        Destroy(gameObject, 1f);
    }

}
