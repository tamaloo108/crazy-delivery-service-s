﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudioHandler : MonoBehaviour
{
    [SerializeField] AudioData AudioList;
    [SerializeField] AudioSource BGMSource;
    [SerializeField] AudioSource SFXSource1;
    [SerializeField] AudioSource SFXSource2;
    [SerializeField] AudioSource VASource;

    private void Awake()
    {
        UniversalSoundHandler.Instance.InitSounds(AudioList);
    }

    private void Start()
    {
        UniversalSoundHandler.Instance.AssignEvent(PlayBGM, PlaySFX1, PlaySFX2, PlayVA, ChangeBGMPitch);

        UniversalSoundHandler.Instance.MenuPlayBGM.Invoke("BGM_MainMenu_1");
    }

    public void ChangeBGMPitch(float v)
    {
        BGMSource.pitch = v;
    }

    public void PlaySFX1(string name)
    {
        UniversalSoundHandler.Instance.PlaySounds(name, SFXSource1);
    }

    public void PlaySFX2(string name)
    {
        UniversalSoundHandler.Instance.PlaySounds(name, SFXSource2);
    }

    public void PlayBGM(string name)
    {
        UniversalSoundHandler.Instance.PlaySounds(name, BGMSource);
    }

    public void PlayVA(string name)
    {
        UniversalSoundHandler.Instance.PlaySounds(name, VASource);
    }


    private void OnDisable()
    {

    }
}
