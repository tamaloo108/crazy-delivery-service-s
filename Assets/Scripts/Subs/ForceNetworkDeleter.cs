﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class ForceNetworkDeleter : MonoBehaviourPunCallbacks
{
    GameObject TargetObj;

    public void DeleteObj(GameObject g)
    {
        Destroy(g);
    }

    public void DeleteObj(GameObject g, float delay)
    {
        Destroy(g, delay);
    }

    internal void AssignObject(GameObject _g)
    {
        TargetObj = _g;
    }

    [PunRPC]
    void RPC_Deleter(float delay)
    {
        Destroy(TargetObj, delay);
        TargetObj = null;
    }

}
