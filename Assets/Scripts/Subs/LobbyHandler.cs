﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using Photon.Pun;
using System;

public class LobbyHandler : MonoBehaviour
{
    [SerializeField] Transform PlayerListContentView;
    [SerializeField] AssetReference PlayerListContent;
    [SerializeField] Button StartGame;
    [SerializeField] Button LeaveRoom;
    [SerializeField] Button courrierselect;


    Action callback_startgame;
    Action<bool> callback_readygame;

    delegate void StartGames();
    event StartGames OnStartGame;

    bool isReady = false;

    public bool GetReadyState()
    {
        return isReady;
    }

    public Button GetLeaveRoomButton()
    {
        return LeaveRoom;
    }

    public void SetReadyState(bool v)
    {
        isReady = v;
    }

    public Button courierselectiononroom() 
    {
        return courrierselect;
    }

    private void Update()
    {
        StartGame.gameObject.SetActive(PhotonNetwork.IsMasterClient);
    }

    public AssetReference GetPlayerListContent()
    {
        return PlayerListContent;
    }

    private void OnDisable()
    {
        // OnStartGame -= LobbyHandler_OnStartGame;
    }

    public void UnsubscribeGame(Action callback)
    {
        callback_startgame = callback;
        OnStartGame -= LobbyHandler_OnStartGame;
    }

    public void SubsribeStartGame(Action callback)
    {
        callback_startgame = callback;
        OnStartGame += LobbyHandler_OnStartGame;
    }

    private void LobbyHandler_OnStartGame()
    {
        callback_startgame();
    }

    public Transform GetPlayerListContentView()
    {
        return PlayerListContentView;
    }

    public bool AllPlayerReady()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            var lists = PhotonNetwork.PlayerList;
            foreach (var item in lists)
            {
                if ((bool)item.CustomProperties["PLAYER_READY"] == false) return false;
            }
        }
        return true;
    }

    public void SetStartGame()
    {
        if (PhotonNetwork.IsMasterClient && AllPlayerReady())
        {
            UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1");
            //start the game
            OnStartGame.Invoke();
        }
    }

    public void SetReadyGame()
    {
        isReady = !isReady;
        var k = PhotonNetwork.LocalPlayer;
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h.Add("PLAYER_READY", isReady);
        k.SetCustomProperties(h);
        UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke(isReady ? "SFX_ButtonClick_1" : "SFX_ButtonBack_1");
        callback_readygame(isReady);
    }

    public void setReadyGame(Action<bool> callback)
    {
        callback_readygame = callback;
    }
}
