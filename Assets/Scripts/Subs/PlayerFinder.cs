﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerFinder : MonoBehaviour
{
    Coroutine Finder;
    WaitForSeconds delay = new WaitForSeconds(0.01f);
    WaitUntil PlayerFound;
    WaitUntil PlayerSpawned;
    [SerializeField] CinemachineVirtualCamera PlayerCam;

    private void Start()
    {
        PlayerFound = new WaitUntil(() => PlayerCam.m_Follow == null && PlayerCam.m_LookAt == null);
        if (Finder == null)
        {
            Finder = StartCoroutine(FinderCounter());
        }
    }

    private IEnumerator FinderCounter()
    {
        while (true)
        {
            yield return delay;
            yield return PlayerFound;

            var player = GameObject.FindGameObjectsWithTag("Player");
            foreach (var item in player)
            {
                if (item != null)
                {
                    var view = item.GetComponent<PhotonView>();
                    if (view.IsMine)
                    {
                        Camera.main.GetComponent<CinemachineBrain>().enabled = true;
                        PlayerCam.m_Follow = item.transform;
                        PlayerCam.m_LookAt = item.transform;
                    }
                }

            }
            
        }
    }
}
