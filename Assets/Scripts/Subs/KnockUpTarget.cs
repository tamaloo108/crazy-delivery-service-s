﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class KnockUpTarget : MonoBehaviour
{

    [SerializeField] string TargetMask = "Player";
    [SerializeField] float Duration = 1f;
    [SerializeField] float Forces = 20f;
    [SerializeField] float CameraShakeDuration = .2f;
    [SerializeField] float OffsetX = 10;
    [SerializeField] float OffsetY = 10;
    [SerializeField] float OffsetZ = 10;
    [SerializeField] LayerMask hittableMask;

    Coroutine Tabrak;
    Vector3 LocalPosition;
    Vector3 WorldPosition;

    void GetPoints(Transform target)
    {
        Ray raycast = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(raycast, out hit, 1000f, hittableMask))
        {
            //if pointing at the object you want to get collision point for
            if (hit.transform.gameObject == target.gameObject)
            {
                //have locPos = the local position of the hit point
                WorldPosition = hit.point;
                LocalPosition = transform.InverseTransformPoint(hit.point);

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(TargetMask))
        {
            GetPoints(other.transform);
            UniversalHandler.Instance.VFX.DrawVFX(other.transform.position, "Impact_General");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals(TargetMask))
        {
            GetPoints(other.transform);
            if (Tabrak == null)
            {
                Tabrak = StartCoroutine(KnockEmUp(other.transform.parent.GetChild(1).gameObject, other.transform.up, -other.transform.forward));
            }
        }
    }

    IEnumerator KnockEmUp(GameObject g, Vector3 Up, Vector3 Back)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        var pView = g.transform.parent.GetChild(0).GetComponent<Photon.Pun.PhotonView>();
        if (pView.IsMine)
        {
            InitCamOffset();
            DoCameraShake();
        }

        var dur = Duration;
        var rb = g.GetComponent<Rigidbody>();
        while (dur > 0f)
        {
            Debug.Log("knocked");
            rb.AddForce(Up * Forces, ForceMode.VelocityChange);
            rb.AddForce(Up * (Forces * 2f), ForceMode.VelocityChange);
            dur -= Time.deltaTime;
            yield return delay;
        }

        Tabrak = null;
        yield break;
    }

    private void DoCameraShake()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        perlin.SetCameraShake(20f, 20f, CameraShakeDuration);
    }

    private void InitCamOffset()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        var c_offsets = new Vector3(UnityEngine.Random.Range(-OffsetX, OffsetX), UnityEngine.Random.Range(-OffsetY, OffsetY), UnityEngine.Random.Range(-OffsetZ, OffsetZ));
        perlin.InitNoiseValue(c_offsets);
    }
}
