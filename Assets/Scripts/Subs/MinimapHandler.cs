﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.AddressableAssets;

public class MinimapHandler : MonoBehaviour
{
    [SerializeField] Vector3 Offset;
    [SerializeField] CinemachineVirtualCamera Cam;
    [SerializeField] AssetReference MinimapHUD;
    Transform target;
    public bool isFollow = false;
    private Vector3 _velocity;
    [SerializeField] GameObject MinimapInstance;

    private void Start()
    {
        //  InitCamera();
        DeactivateMinimap();
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
        Cam.m_Follow = target;
        Cam.m_LookAt = target;
    }

    public void InitCamera()
    {
        UniversalAddresableLoader.Instance.Spawn(MinimapHUD, Vector3.zero, null, OnDoneSpawn);
    }

    private void OnDoneSpawn(GameObject obj)
    {
        var MainCamera = Camera.main;
        Canvas _canvas = obj.GetComponent<Canvas>();
        _canvas.worldCamera = MainCamera;
        MinimapInstance = obj;
        UniversalSceneLoader.Instance.TransferObjectScene(MinimapInstance, "Core");
    }

    void Update()
    {
        if (isFollow)
        {
            FollowTarget();
        }
    }

    private void FollowTarget()
    {
        try
        {
            var pos = target.position;
            var newPos = new Vector3(pos.x, transform.position.y, pos.z);
            var damp = Vector3.SmoothDamp(transform.position + Offset, newPos, ref _velocity, 0.01f, 100f);
            transform.position = damp;
        }
        catch (Exception)
        {

        }
    }

    internal void DeactivateMinimap()
    {
        MinimapInstance.SetActive(false);
    }

    internal void ActivateMinimap()
    {
        MinimapInstance.SetActive(true);
    }
}
