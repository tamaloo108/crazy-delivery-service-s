﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ControllerHUDHandler : MonoBehaviour
{
    /// <summary>
    /// Power Button UI Order :
    /// 1.Counter
    /// 2.Sprites reflect corresponding skill.
    /// </summary>

    [SerializeField] GameObject PickButton;
    [SerializeField] GameObject ClientSpecialButton;
    [SerializeField] GameObject DriverSpecialButton;
    [SerializeField] GameObject BoxSpecialButton;
    [SerializeField] GameObject DropButton;
    //special AutoPickup
    [SerializeField] GameObject AutoPickUpIcon;
    [SerializeField] Image PickUpFiller;
    [SerializeField] GameObject AutoDropIcon;
    [SerializeField] Image DropFiller;


    [SerializeField] PowerScriptableObject PowerUISettings;

    private void Start()
    {
        ClientSpecialButton.SetActive(false);
        DriverSpecialButton.SetActive(false);
        BoxSpecialButton.SetActive(false);
        PickButton.SetActive(false);
        DropButton.SetActive(false);
    }

    public void OnAutoPickDropState(bool onPick, float v)
    {
        if (!UniversalHandler.Instance.GameSettings.IsAutoPickup()) return;
        if (onPick)
        {
            AutoPickUpIcon.SetActive(onPick);
            AutoDropIcon.SetActive(!onPick);
            PickUpFiller.fillAmount = v;
        }
        else
        {
            AutoDropIcon.SetActive(!onPick);
            AutoPickUpIcon.SetActive(onPick);
            DropFiller.fillAmount = v;
        }
    }

    public void ResetAutoPickUpValue(float v)
    {
        PickUpFiller.fillAmount = v;
        DropFiller.fillAmount = v;
        AutoPickUpIcon.SetActive(false);
        AutoDropIcon.SetActive(false);
    }

    public void SetPickButtonState(bool v, bool OnPick)
    {
        if (UniversalHandler.Instance.GameSettings.IsAutoPickup()) return;

        PickButton.SetActive(OnPick);
        DropButton.SetActive(!OnPick);
    }

    float ConvertRange(float Val, float OldMin, float newMin, float newMax, float oldMax)
    {
        return (((Val - OldMin) * newMax) / oldMax) + newMin;
    }

    public void SetClientAmmoValue(int val, int Max)
    {
        ClientSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount = 1f;
        var RawCurValue = Max - val;
        var CurAmount = ClientSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount;
        ClientSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount = ConvertRange(RawCurValue, 0f, 0f, 1f, Max);
    }

    public void SetDriverAmmoValue(int val, int Max)
    {
        DriverSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount = 1f;
        var RawCurValue = Max - val;
        var CurAmount = DriverSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount;
        DriverSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount = ConvertRange(RawCurValue, 0f, 0f, 1f, Max);
    }

    public void SetBoxAmmoValue(int val, int Max)
    {
        BoxSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount = 1f;
        var RawCurValue = Max - val;
        var CurAmount = BoxSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount;
        BoxSpecialButton.transform.GetChild(0).GetComponent<Image>().fillAmount = ConvertRange(RawCurValue, 0f, 0f, 1f, Max);
    }

    public void SetClientSpecialButtonState(bool v)
    {
        ClientSpecialButton.SetActive(v);
    }

    public void SetDriverSpecialButtonState(bool v)
    {
        DriverSpecialButton.SetActive(v);
    }

    public void setBoxPowerButtonState(bool v)
    {
        BoxSpecialButton.SetActive(v);
    }

    public void SetClientSpecialButtonState(bool v, PowersEnum PowerType, int CurUsage, int Max)
    {
        ClientSpecialButton.SetActive(v);
        ClientSpecialButton.transform.GetChild(0).GetComponent<Image>().sprite = PowerUISettings.GetPowerCounter(PowerType);
        ClientSpecialButton.transform.GetChild(1).GetComponent<Image>().sprite = PowerUISettings.GetPowerImage(PowerType);
        SetClientAmmoValue(CurUsage, Max);
    }

    public void SetDriverSpecialButtonState(bool v, PowersEnum PowerType, int CurUsage, int Max)
    {
        DriverSpecialButton.SetActive(v);
        DriverSpecialButton.transform.GetChild(0).GetComponent<Image>().sprite = PowerUISettings.GetPowerCounter(PowerType);
        DriverSpecialButton.transform.GetChild(1).GetComponent<Image>().sprite = PowerUISettings.GetPowerImage(PowerType);
        SetDriverAmmoValue(CurUsage, Max);
    }

    public void setBoxPowerButtonState(bool v, PowersEnum PowerType, int CurUsage, int Max)
    {
        BoxSpecialButton.SetActive(v);
        BoxSpecialButton.transform.GetChild(0).GetComponent<Image>().sprite = PowerUISettings.GetPowerCounter(PowerType);
        BoxSpecialButton.transform.GetChild(1).GetComponent<Image>().sprite = PowerUISettings.GetPowerImage(PowerType);
        SetBoxAmmoValue(CurUsage, Max);
    }
}
