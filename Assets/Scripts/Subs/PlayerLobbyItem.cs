﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerLobbyItem : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI PlayerName;
    [SerializeField] GameObject ReadyIndicatorUI;
    [SerializeField] Animator anim;

    public void SetPlayerName(string name)
    {
        PlayerName.text = name;
        playslider();
    }

    private void playslider()
    {
        if (PlayerName.text.Length < 9)
        {
            anim.SetBool("teks panjang", false);
        }
        else if (PlayerName.text.Length >= 9)
        {
            anim.SetBool("teks panjang", true);
        }
        else {
            Debug.Log("nama 9 huruf");
        }
    }

    public void SetReadyState(bool ready)
    {
        if (ready)
        {
            ReadyIndicatorUI.SetActive(true);
        }
        else {
            ReadyIndicatorUI.SetActive(false);
        }
    }
}
