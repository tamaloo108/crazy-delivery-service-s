﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.AddressableAssets;

public class RoomListHandler : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI RoomName;
    [SerializeField] TextMeshProUGUI MapName;
    [SerializeField] Button JoinButton;
    [SerializeField] TextMeshProUGUI MaxPlayerText;
    [SerializeField] Animator anim;

    private WaitUntil reloadlobbyanim;
    bool reloadanim;
    int first = 0;


    int MaxPlayer = 99;
    int CurrentPlayer = 0;
    //bool animationset;

    private void Start()
    {
        reloadlobbyanim = new WaitUntil(() => UniversalSceneLoader.Instance.isreloadlobbyanimation(reloadanim));
    }

    public void SetMapName(string map) {
        MapName.text = map;
    }

    public void SetRoomName(string name)
    {
        RoomName.text = name;
        //StartCoroutine(playslider());
    }

    public void replayanim() {
        Debug.Log("replay animation "+reloadanim);
        StartCoroutine(playslider());
    }

    private void OnEnable() {
        if (first != 0)
        {
            StartCoroutine(playslider());
        }
        else 
        {
            first += 1;        
        }
    }

    private void OnDisable()
    {
        StopCoroutine(playslider());
        Debug.Log("stopcoroutine");
    }


    IEnumerator playslider() {
        Debug.Log("waiting parameter true");
        yield return reloadlobbyanim;
        Debug.Log("playing slider");
        if (RoomName.text.Length < 12) {
            Debug.Log("teks tidak panjang" + RoomName.text.Length);
            anim.SetBool("teks panjang", false);
        } else if (RoomName.text.Length > 12) {
            Debug.Log("teks panjang" + RoomName.text.Length );
            anim.SetBool("teks panjang", true);
        }
        reloadanim = false;
    }

    //public void scrollanimation() {
    //    if (roomnamescroll.horizontalNormalizedPosition <= 0.05f) {
    //        Debug.Log("scrolled near start");
    //    }
    //    if (roomnamescroll.horizontalNormalizedPosition >= 0.95f) {
    //        Debug.Log("scrolled near end");
    //    }
    //}

    public void SetMaxPlayer(int m)
    {
        MaxPlayer = m;
        MaxPlayerText.text = string.Format("{0} / {1} ", CurrentPlayer, MaxPlayer);
    }

    public void SetCurrentPlayer(int value)
    {
        CurrentPlayer = value;
        MaxPlayerText.text = string.Format("{0} / {1} ", CurrentPlayer, MaxPlayer);
    }

    public void JoinRoomOnClick(Action<string> JoinRoom)
    {
        JoinButton.onClick.AddListener(() => JoinRoom(RoomName.text));
    }
}
