﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "New Power Up UI Settings", menuName = "CDS/Power Up UI Settings")]
public class PowerScriptableObject : ScriptableObject
{
    [SerializeField] List<PowerUpStruct> PowerUpParameter;

    public Sprite GetPowerImage(PowersEnum power)
    {
        return PowerUpParameter.Find(x => x.PowerUpId.Equals(power)).PowerUpBaseImage;
    }

    public Sprite GetPowerCounter(PowersEnum power)
    {
        return PowerUpParameter.Find(x => x.PowerUpId.Equals(power)).PowerUpImageCounter;
    }
}

[System.Serializable]
public struct PowerUpStruct
{
    public PowersEnum PowerUpId;
    public Sprite PowerUpBaseImage;
    public Sprite PowerUpImageCounter;
}
