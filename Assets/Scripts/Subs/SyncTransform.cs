﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncTransform : MonoBehaviour
{/// <summary>
///  Help Cinemachine Virtual Camera to sync target transform using animator as condition.
/// </summary>
    [SerializeField] Transform Target;
    [SerializeField] Animator CameraAnimator;
    [SerializeField] string StateName;
    [SerializeField] bool Inverse = false;

    // Update is called once per frame
    void Update()
    {
        if (CameraAnimator.GetBool(StateName) == Inverse)
        {
            transform.position = Target.position;
            transform.rotation = Target.rotation;
        }
    }
}
