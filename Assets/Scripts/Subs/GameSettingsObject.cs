﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


[CreateAssetMenu(fileName = "New Game Settings", menuName = "CDS/Game Settings")]
public class GameSettingsObject : ScriptableObject
{
    public GameOptions m_GameOptions;
    [SerializeField] List<VolumeProfile> m_PPQuality;

    public VolumeProfile GetVolumeProfile(int v)
    {
        return m_PPQuality[v];
    }

}

[System.Serializable]
public struct GameOptions
{
    [Header("Graphics Settings")]
    public mOverallQuality Quality;
    public mPostProcessingQuality PostProcessingQuality;
    [Range(90f, 500f)] public float MaxDrawDistance;
    public bool Bloom;
    public bool AntiAliasing;

    [Header("Sound Settings")]
    [Range(0f, 1f)] public float MasterVolume;
    [Range(0f, 1f)] public float BGMVolume;
    [Range(0f, 1f)] public float SFXVolume;
    [Range(0f, 1f)] public float VoiceVolume;

    [Header("Gameplay Settings")]
    public bool AutoPickUp;
    public bool EnableTutorial;
    public bool LowFPSMode;
}


public enum mOverallQuality
{
    Low = 0,
    Medium = 1,
    High = 2
}

public enum mPostProcessingQuality
{
    Low = 0,
    Medium = 1,
    High = 2
}
