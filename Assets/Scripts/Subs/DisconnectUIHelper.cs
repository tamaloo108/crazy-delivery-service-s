﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisconnectUIHelper : MonoBehaviour
{
    [SerializeField] Button RetryButton;
    [SerializeField] Button QuitButton;


    public Button GetRetryButton()
    {
        return RetryButton;
    }

    public Button GetQuitButton()
    {
        return QuitButton;
    }
}
