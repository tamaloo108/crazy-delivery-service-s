﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettingsInstanceHandler : MonoBehaviour
{
    [SerializeField] List<Toggle> TextureQualityToggles;
    [SerializeField] List<Toggle> PostProcessingToggles;
    [SerializeField] List<Toggle> AntiAliasingToggles;
    [SerializeField] List<Toggle> BloomToggles;
    [SerializeField] List<Toggle> FrameRateToggles;
    [SerializeField] List<Toggle> AutoPickToggles;
    [SerializeField] List<Toggle> EnableTutorialToggles;

    [SerializeField] ToggleGroup TextureQualityGroup;
    [SerializeField] ToggleGroup PostProcessingGroup;
    [SerializeField] ToggleGroup AntiAliasingGroup;
    [SerializeField] ToggleGroup BloomGroup;
    [SerializeField] ToggleGroup FrameRateGroup;
    [SerializeField] ToggleGroup AutoPickGroup;
    [SerializeField] ToggleGroup EnableTutorialGroup;

    [SerializeField] Slider SFXSlider;
    [SerializeField] Slider BGMSlider;
    [SerializeField] Slider MasterSlider;
    [SerializeField] Slider VASlider;
    [SerializeField] Slider DrawDistSlider;

    GameSettingsObject GameOptions;

    public void SetGameOptions(GameSettingsObject v, GameSettingHandler handler)
    {
        GameOptions = v;
        GetToggleGroupActive((int)GameOptions.m_GameOptions.Quality, TextureQualityToggles, out TextureQualityToggles);
        GetToggleGroupActive((int)GameOptions.m_GameOptions.PostProcessingQuality, PostProcessingToggles, out PostProcessingToggles);
        GetToggleGroupActive(GameOptions.m_GameOptions.AntiAliasing, AntiAliasingToggles, out AntiAliasingToggles);
        GetToggleGroupActive(GameOptions.m_GameOptions.Bloom, BloomToggles, out BloomToggles);
        GetToggleGroupActive(GameOptions.m_GameOptions.LowFPSMode, FrameRateToggles, out FrameRateToggles);
        GetToggleGroupActive(GameOptions.m_GameOptions.AutoPickUp, AutoPickToggles, out AutoPickToggles);

        GetSliderValue(GameOptions.m_GameOptions.BGMVolume, BGMSlider);
        GetSliderValue(GameOptions.m_GameOptions.SFXVolume, SFXSlider);
        GetSliderValue(GameOptions.m_GameOptions.MasterVolume, MasterSlider);
        GetSliderValue(GameOptions.m_GameOptions.VoiceVolume, VASlider);

        float m_MaxDrawDistConverted = ConvertRange(GameOptions.m_GameOptions.MaxDrawDistance, 90f, 0f, 1f, 500f);
        GetSliderValue(m_MaxDrawDistConverted, DrawDistSlider);
    }

    float ConvertRange(float Val, float OldMin, float newMin, float newMax, float oldMax)
    {
        return (((Val - OldMin) * newMax) / oldMax) + newMin;
    }

    Toggle GetActiveToggle(IEnumerable<Toggle> ToggleList)
    {
        var ik = ToggleList.GetEnumerator();
        while (ik.MoveNext())
        {
            if (ik.Current.isOn)
                return ik.Current;
        }

        return null;
    }

    private void SetGameSettingsValue()
    {
        //Quality
        var temp_quality = TextureQualityGroup.ActiveToggles();
        GameOptions.m_GameOptions.Quality = (mOverallQuality)TextureQualityToggles.FindIndex(x => x.Equals(GetActiveToggle(temp_quality)));

        //PostProcessing
        var temp_pp = PostProcessingGroup.ActiveToggles();
        GameOptions.m_GameOptions.PostProcessingQuality = (mPostProcessingQuality)PostProcessingToggles.FindIndex(x => x.Equals(GetActiveToggle(temp_pp)));

        //AntiAliasing
        var temp_aa = AntiAliasingGroup.ActiveToggles();
        GameOptions.m_GameOptions.AntiAliasing = AntiAliasingToggles.FindIndex(x => x.Equals(GetActiveToggle(temp_aa))) == 1 ? true : false;

        //Bloom
        var temp_bloom = BloomGroup.ActiveToggles();
        GameOptions.m_GameOptions.Bloom = BloomToggles.FindIndex(x => x.Equals(GetActiveToggle(temp_bloom))) == 1 ? true : false;

        //LowFPS
        var temp_lfps = FrameRateGroup.ActiveToggles();
        GameOptions.m_GameOptions.LowFPSMode = FrameRateToggles.FindIndex(x => x.Equals(GetActiveToggle(temp_lfps))) == 1 ? true : false;

        //AutoPick
        var temp_auto = AutoPickGroup.ActiveToggles();
        GameOptions.m_GameOptions.AutoPickUp = AutoPickToggles.FindIndex(x => x.Equals(GetActiveToggle(temp_auto))) == 1 ? true : false;

        GameOptions.m_GameOptions.BGMVolume = BGMSlider.value;
        GameOptions.m_GameOptions.SFXVolume = SFXSlider.value;
        GameOptions.m_GameOptions.MasterVolume = MasterSlider.value;
        GameOptions.m_GameOptions.VoiceVolume = VASlider.value;
        GameOptions.m_GameOptions.MaxDrawDistance = ConvertRange(DrawDistSlider.value, 0f, 90f, 500f, 1f);
    }

    private void GetSliderValue(float bGMVolume, Slider Slider)
    {
        Slider.value = bGMVolume;
    }

    private void GetToggleGroupActive(int selVal, List<Toggle> Toggles, out List<Toggle> TargetToggles)
    {
        Debug.Log(selVal);
        Toggles[selVal].isOn = true;
        TargetToggles = Toggles;
    }

    //exclusive two toggle group.
    private void GetToggleGroupActive(bool value, List<Toggle> Toggles, out List<Toggle> TargetToggles)
    {
        Toggles[1].isOn = value;
        TargetToggles = Toggles;
    }

    public void ApplySettings()
    {
        SetGameSettingsValue();
        UniversalHandler.Instance.GameSettings.ApplySettings();
    }

    public void ExitSettings()
    {
        UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1");
    }

    public void DoneSettings()
    {
        UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1");
    }
}
