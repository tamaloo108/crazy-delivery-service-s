﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class CharacterDetailHandler : MonoBehaviour
{
    [SerializeField] CharacterData MyCharData;
    [SerializeField] TextMeshProUGUI MyCharName;
    [SerializeField] TextMeshProUGUI MyCharDesc;
    [SerializeField] TextMeshProUGUI MySkillName;
    [SerializeField] TextMeshProUGUI MyAttribName;
    [SerializeField] Image MySkill;
    [SerializeField] Image MyAttrib;
    [SerializeField] Image TopSpeedImg;
    [SerializeField] Image HandlingImg;
    [SerializeField] Image AccelImg;




    public void InitParameter()
    {
        var datas = MyCharData.GetMyData(UniversalHandler.Instance.getcharactercode());

        MyCharName.text = datas.CharName;
        MyCharDesc.text = datas.CharDesc;
        MySkill.sprite = datas.CharSkill;
        MyAttrib.sprite = datas.CharAttribute;
        TopSpeedImg.fillAmount = datas.TopSpeed;
        HandlingImg.fillAmount = datas.Handling;
        AccelImg.fillAmount = datas.Accel;

        MyAttribName.text = GetMyAttributes(datas.Attributes);
        MySkillName.text = datas.SkillName;
    }

    public string GetMyAttributes(CharacterAttributes attrib)
    {
        switch (attrib)
        {
            case CharacterAttributes.Defensive:
                return "Defensive";

            case CharacterAttributes.Offensive:
                return "Offensive";

            default:
                return "Utility";
        }
    }
}
