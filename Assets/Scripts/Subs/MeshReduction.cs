﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshReduction : MonoBehaviour
{
    [SerializeField] float Quality = .6f;
    SkinnedMeshRenderer meshes;
    MeshFilter mesh_;
    void Start()
    {
        try
        {
            meshes = GetComponent<SkinnedMeshRenderer>();
            if (!meshes)
            {
                mesh_ = GetComponent<MeshFilter>();
                var Simplifly = new UnityMeshSimplifier.MeshSimplifier();
                Simplifly.Initialize(mesh_.sharedMesh);
                Simplifly.SimplifyMesh(Quality);
                meshes.sharedMesh = Simplifly.ToMesh();
            }
            else
            {
                var Simplifly = new UnityMeshSimplifier.MeshSimplifier();
                Simplifly.Initialize(meshes.sharedMesh);
                Simplifly.SimplifyMesh(Quality);
                meshes.sharedMesh = Simplifly.ToMesh();
            }
        }
        catch (System.Exception)
        {

        }

    }

}
