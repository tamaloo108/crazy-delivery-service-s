﻿using Photon.Pun;
using UnityEngine;

public interface IPedestrianHandler
{
    bool isOnRagdoll();
    void PlayAnim(string name);
    void OnTravelState(bool state);
}