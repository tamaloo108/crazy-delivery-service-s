﻿using UnityEngine;

public interface IDestinationHandler
{
    Vector3 GetCurrentDestination();
    Transform GetCurrentDestinationTransform();
    void SetDestination();
    void SetTravelState(bool state);
    bool GetTravelState();
    void SeatModeState(bool state, Transform seat);
}