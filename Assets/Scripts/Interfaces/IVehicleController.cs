﻿using UnityEngine;

public interface IVehicleController
{
    void DoJump();
    void DoSteering();
    void ChangeWheelAngle(WheelCollider collider);
    void BalanceVehicle(bool active);
    void Accel(WheelCollider collider);
    bool GetPickup();
    void Accel();
    void SetWheelieGravity();
    void SyncMesh();
    bool GetSpecialButton();
    bool GetBoxSpecialButton();
    bool GetDriverSpecialButton();
    bool OnMovementInputDetected();
    void FollowCollider(Vector3 offset);
    void ForceSetPosition(Vector3 position);
    void SetState(VehicleStates state);
    void ActivateBoostAnim(bool state);
    void ShakeCamera();
    VehicleStates GetState();
    void Push(int direction, float force);
}