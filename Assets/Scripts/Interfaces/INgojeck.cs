﻿using UnityEngine;

public interface INgojeck
{
    void SetDestination(Vector3 dest);
    void SetOnTravelState(bool state);
    bool GetOnTravelState();
    void ReleasePedestrian();
    void Fall();
    void SetPowerFromBox(PowerUpManager box);
    PowerUpManager GetPowerFromBox();
    PowerUpManager GetPowerFromClient();
}