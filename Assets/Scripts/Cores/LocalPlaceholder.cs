﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPlaceholder : MonoBehaviour
{
    [SerializeField] GameObject PlayerNetworkedVehicle;
    [SerializeField] GameObject PlayerLocalVehicle;
    [SerializeField] LayerMask TargetLayerMask;

    LevelManager Manager;

    bool SetTargetMask = false;

    public void SetManager(LevelManager m)
    {
        Manager = m;
    }

    private void OnEnable()
    {
        var VehicleHandler_ = GetComponentInChildren<VehicleHandler>();
        VehicleHandler_.LetUpdateLocalPlayerVehicle += VehicleHandler__LetUpdateLocalPlayerVehicle;
    }

    private void OnDisable()
    {
        var VehicleHandler_ = GetComponentInChildren<VehicleHandler>();
        VehicleHandler_.LetUpdateLocalPlayerVehicle -= VehicleHandler__LetUpdateLocalPlayerVehicle;
    }

    private void VehicleHandler__LetUpdateLocalPlayerVehicle()
    {
        PlayerLocalVehicle.transform.localPosition = PlayerNetworkedVehicle.transform.localPosition;
        PlayerLocalVehicle.transform.localRotation = PlayerNetworkedVehicle.transform.localRotation;
    }

    private void Update()
    {
        if (!SetTargetMask)
        {
            if (Photon.Pun.PhotonNetwork.NetworkClientState == Photon.Realtime.ClientState.Disconnecting || Photon.Pun.PhotonNetwork.NetworkClientState == Photon.Realtime.ClientState.Disconnected)
            {
                VehicleHandler__OnDisconnectDetected();
                Manager.UpdateLastDisconnectPosition(transform.localPosition);
            }
        }

        if (SetTargetMask && Photon.Pun.PhotonNetwork.NetworkClientState == Photon.Realtime.ClientState.Joined)
        {
            GetComponent<AutoDestroyParentPlayer>().DestroyMe();
        }
    }

    private void VehicleHandler__OnDisconnectDetected()
    {
        foreach (var item in PlayerLocalVehicle.GetComponentsInChildren<Transform>())
        {
            item.gameObject.layer = LayerMask.NameToLayer("Player");
        }

        SetTargetMask = true;
    }
}
