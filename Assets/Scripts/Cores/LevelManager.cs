﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using UnityEngine.AddressableAssets;
using System.Linq;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System;
using ExitGames.Client.Photon;
using System.Threading.Tasks;
using Cinemachine;

public class LevelManager : MonoBehaviourPunCallbacks
{
    [SerializeField] List<string> PedestrianName;
    [SerializeField] List<Transform> SpawnTransform;
    [SerializeField] List<Transform> PlayerSpawnTransform;
    [SerializeField] List<Transform> BoxSpawnTransform;
    [SerializeField] AssetReference ControllerHUD;
    [SerializeField] AssetReference MatchHUD;
    [SerializeField] AssetReference LoadingRefHUD;
    [SerializeField] int MaxPedestrianOnScreen = 17;
    [SerializeField] int MaxBoxOnScreen = 15;
    [SerializeField] int MaxPedestrianForMatch = 30;
    [SerializeField] float GameDuration = 5f;
    [SerializeField] float RandomX = 2f;
    [SerializeField] float RandomY = 2f;
    [SerializeField] float BoxRandomX = 1f;
    [SerializeField] float BoxRandomZ = 2f;
    [SerializeField] Camera LevelCam;

    WaitUntil WaitTilReady;
    WaitUntil IsReachCap;
    WaitUntil IsPodiumSceneLoaded;
    WaitUntil IsPodiumSceneReady;
    WaitUntil MatchHUDInstanceSpawned;
    WaitUntil isLoadingEstablish;
    WaitForSeconds DestroyDelay = new WaitForSeconds(2.7f);
    WaitForSeconds LoadingDelay = new WaitForSeconds(.5f);
    WaitForSeconds SpawnDelay = new WaitForSeconds(.3f);
    WaitForSeconds delay = new WaitForSeconds(0.1f);
    WaitForEndOfFrame fastDelay = new WaitForEndOfFrame();
    PhotonView myPV;
    Coroutine PedestrianSpawner;
    Coroutine SetToLoadLevel;
    Coroutine GameObserver;
    Coroutine GameEnd;

    GameObject Minicam;

    Dictionary<int, Collider> M_Spawn = new Dictionary<int, Collider>();
    Dictionary<int, Collider> P_Spawn = new Dictionary<int, Collider>();
    Dictionary<int, Collider> B_Spawn = new Dictionary<int, Collider>();
    Dictionary<int, Transform> PedestrianPool = new Dictionary<int, Transform>();
    Player[] Player;

    int curPedestrianNum = 0;
    int curBoxNum = 0;
    bool timerIsSet = false;
    bool SceneIsLoaded = false;
    bool StartCounting = false;
    private int StartTimestamp;
    bool OnSwitchBGM = false;
    bool DoneSpawnSingle = false;

    public bool GameIsOn { get; private set; }

    MatchUiHandler MatchHUDInstance;
    GameObject LoadingInstance;
    GameObject ControllerHUDInstance;

    public Vector3 MyLastDisconnectPosition { get; private set; }

    private void Awake()
    {
        myPV = GetComponent<PhotonView>();
        IsReachCap = new WaitUntil(() => curPedestrianNum < MaxPedestrianOnScreen);
        MatchHUDInstanceSpawned = new WaitUntil(() => MatchHUDInstance != null);
        WaitTilReady = new WaitUntil(() => StartCounting);
        IsPodiumSceneLoaded = new WaitUntil(() => UniversalSceneLoader.Instance.IsSceneLoaded("Podium"));
        IsPodiumSceneReady = new WaitUntil(() => UniversalSceneLoader.Instance.IsSceneActive("Podium"));
        isLoadingEstablish = new WaitUntil(() => LoadingInstance != null);
        PopulateSpawnPosition();
        GameIsOn = true;
        //PedestrianName.Add("Pedestrians");
        //PedestrianName.Add("Pedestrians_invincible");
    }

    public void UpdateLastDisconnectPosition(Vector3 pos)
    {
        MyLastDisconnectPosition = pos;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        CountdownTimer.OnCountdownTimerHasExpired += OnCountdownExpired;
        PhotonNetwork.NetworkingClient.EventReceived += EventReceiver;

        UniversalHandler.Instance.InitCameraShake += DoCameraShake;
        UniversalHandler.Instance.InitCameraOffset += InitCamOffset;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        CountdownTimer.OnCountdownTimerHasExpired -= OnCountdownExpired;
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceiver;

        UniversalHandler.Instance.InitCameraShake -= DoCameraShake;
        UniversalHandler.Instance.InitCameraOffset -= InitCamOffset;
    }

    private void Start()
    {
        Hashtable prop = new Hashtable
        {
            { UniversalHandler.PLAYER_LOAD_LEVEL, true }
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(prop);
        var cam = GameObject.FindGameObjectWithTag("CinemachineStateDriven").gameObject.GetComponent<Cinemachine.CinemachineStateDrivenCamera>();
        UniversalHandler.Instance.SetDrivenCamera(cam);
        if (SetToLoadLevel == null)
        {
            SetToLoadLevel = StartCoroutine(LoadLevelCoroutine());
        }

    }

    private void DoCameraShake()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        perlin.SetCameraShake(20f, 20f, 0.03f);
    }

    private void InitCamOffset()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        var c_offsets = new Vector3(UnityEngine.Random.Range(-10f, 10f), UnityEngine.Random.Range(-10f, 10f), UnityEngine.Random.Range(-10f, 10f));
        perlin.InitNoiseValue(c_offsets);
    }

    private void DoCameraShake(float amplitudo, float frequency, float duration)
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        perlin.SetCameraShake(amplitudo, frequency, duration);
    }

    private void InitCamOffset(float MinOffset, float MaxOffset)
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        var c_offsets = new Vector3(UnityEngine.Random.Range(MinOffset, MaxOffset), UnityEngine.Random.Range(MinOffset, MaxOffset), UnityEngine.Random.Range(MinOffset, MaxOffset));
        perlin.InitNoiseValue(c_offsets);
    }

    private IEnumerator LoadLevelCoroutine()
    {
        var player = PhotonNetwork.LocalPlayer.CustomProperties;

        while (!SceneIsLoaded)
        {
            yield return delay;

            Hashtable prop = new Hashtable
                {
                    { UniversalHandler.PLAYER_LOAD_LEVEL, true }
                };
            PhotonNetwork.LocalPlayer.SetCustomProperties(prop);
            player = PhotonNetwork.LocalPlayer.CustomProperties;

        }

        SetToLoadLevel = null;

        Debug.Log("is cancelled");
        yield break;
    }

    private void SetPlayerProp(object key, object value)
    {
        Hashtable prop = new Hashtable
        {
            { key,value }
        };

        PhotonNetwork.LocalPlayer.SetCustomProperties(prop);
    }


    private void OnCountdownExpired()
    {
        StartGame();
    }

    void StartGame()
    {
        UniversalSoundHandler.Instance.MenuPlayBGM.Invoke("BGM_Map_USA_1");
        OnDoneUnloadScene();
        //UniversalHandler.Instance.GlobalCamera.gameObject.SetActive(false);
        //UniversalSceneLoader.Instance.UnloadScene(1, OnDoneUnloadScene);e Settgins
        UniversalAddresableLoader.Instance.Spawn(ControllerHUD, Vector3.zero, null, OnDoneSpawnControllerHUD);

        ForceNetworkDeleter f = GetComponent<ForceNetworkDeleter>();
        UniversalHandler.Instance.SetDeleteUtility(f);
        UpdatePlayerList();

        if (PedestrianSpawner == null && PhotonNetwork.IsMasterClient)
        {
            PedestrianSpawner = StartCoroutine(TargetSpawner());
        }

        if (GameObserver == null && PhotonNetwork.IsMasterClient)
        {
            Debug.Log("til ready : " + StartCounting);
            GameObserver = StartCoroutine(Observer());
        }

    }

    private IEnumerator Observer()
    {
        yield return WaitTilReady;
        var SwitchDuration = Mathf.Round(GameDuration / 2.2f);

        while (GameIsOn)
        {
            yield return fastDelay;

            if (GameDuration <= SwitchDuration && !OnSwitchBGM)
            {
                OnSwitchBGM = true;
                UniversalSoundHandler.Instance.BGMChangePitch.Invoke(1.125f);
            }

            if (GameDuration < 0f || MaxPedestrianForMatch == 0f)
                break;

            //Debug.Log(GameDuration);
            GameDuration -= .15f;
            yield return MatchHUDInstanceSpawned;
            yield return delay;
            UpdatePlayerTime((int)GameDuration);
            UpdatePlayerScore();

            photonView.RPC("RPC_UpdateScoreGlobally", RpcTarget.All);
            photonView.RPC("RPC_UpdateTimeGlobally", RpcTarget.All, GameDuration);
        }

        //game is done, now calculating score.
        GameIsOn = false;
        UniversalSoundHandler.Instance.BGMChangePitch.Invoke(1f);
        UniversalSoundHandler.Instance.MenuPlayBGM.Invoke("BGM_MainMenu_1");
        if (Minicam != null)
        {
            Minicam.GetComponent<MinimapHandler>().DeactivateMinimap();
        }
        object[] content = new object[] { GameIsOn };
        //LevelCam.gameObject.SetActive(false);
        //UniversalHandler.Instance.GlobalCamera.gameObject.SetActive(true);
        RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(UniversalHandler.MATCH_END, content, eventOptions, SendOptions.SendReliable);
    }

    private void UpdatePlayerScore()
    {
        MatchHUDInstance.SetAllPlayerScores(PhotonNetwork.PlayerList);
    }

    private void UpdatePlayerTime(int time)
    {
        MatchHUDInstance.SetTimerTime(time);
    }

    private void UpdatePlayerList()
    {
        var PhotonPlayer = PhotonNetwork.PlayerList;
        Player = PhotonPlayer;
    }

    private void OnDoneUnloadScene()
    {
        UniversalSceneLoader.Instance.SetActiveScene(UniversalSceneLoader.Instance.getmapname());
        var pHandler = GameObject.FindGameObjectWithTag("Game Settings").GetComponent<PlayerHandler>();
        var combatServerHandler = pHandler.gameObject.GetComponent<GameServerHandler>();
        combatServerHandler.enabled = true;
    }

    private void OnDoneSpawnControllerHUD(GameObject obj)
    {
        ControllerHUDInstance = obj;
        switch (UniversalHandler.Instance.getcharactercode())
        {

            case UniversalHandler.SELECT_FREEMAN:
                spawnbike("PhotonResources/PlayerBase/Bike");
                break;

            case UniversalHandler.SELECT_MBAH_KEKOK:
                spawnbike("PhotonResources/PlayerBase/MbahKekok_Vehicle");
                break;

            case UniversalHandler.SELECT_TAXIMAN:
                spawnbike("PhotonResources/PlayerBase/TaxiBaron_Vehicle");
                break;

        }
    }

    private void spawnbike(string directory)
    {
        var CinemachineStateCam = GameObject.FindGameObjectWithTag("CinemachineStateDriven").GetComponent<Cinemachine.CinemachineStateDrivenCamera>();
        UniversalAddresableLoader.Instance.Spawn(MatchHUD, Vector3.zero, null, OnDoneSpawnMatchHUD);
        //PhotonNetwork.Instantiate(Path.Combine("PhotonResources", "Bike"), new Vector3(-98.47f, -13.64f, 59.81f), Quaternion.identity);
        UniversalHandler.Instance.DefaultCameraHandler = CinemachineStateCam.ChildCameras[0].GetComponent<CinemachineVirtualCamera>().GetComponent<CinemachineCameraOffset>().m_Offset;
        var RandomIDSPawn = PlayerSpawnTransform[UnityEngine.Random.Range(0, PlayerSpawnTransform.Count)].GetInstanceID();
        var RandomPos = MyLastDisconnectPosition == Vector3.zero ? GetPlayerRandomPosition(RandomIDSPawn) : MyLastDisconnectPosition;
        var g = PhotonNetwork.Instantiate(Path.Combine(directory), RandomPos, Quaternion.identity);
        var pView = g.transform.GetChild(0).GetComponent<PhotonView>();
        var HUDController = ControllerHUDInstance.GetComponent<ControllerHUDHandler>();
        pView.transform.GetComponent<NgojekHandler>().SetHUD(HUDController);
        var pHandler = GameObject.FindGameObjectWithTag("Game Settings").GetComponent<PlayerHandler>();
        var LocalHandler = g.GetComponent<LocalPlaceholder>();

        if (pView.IsMine)
        {
            pHandler.enabled = true;
            //pView.RequestOwnership();
            SetCurrentVehicle(pHandler, pView);
            var sphere = g.transform.GetChild(1).transform;
            sphere.gameObject.SetActive(true);
            LocalHandler.SetManager(this);
        }
        else
        {
            var sphere = g.transform.GetChild(1).transform;
            Destroy(sphere);
        }
    }

    private void OnDoneSpawnMatchHUD(GameObject obj)
    {
        MatchHUDInstance = obj.GetComponent<MatchUiHandler>();
    }

    private void PopulateSpawnPosition()
    {
        for (int i = 0; i < SpawnTransform.Count; i++)
        {
            var _col = SpawnTransform[i].GetComponent<Collider>();
            M_Spawn.Add(SpawnTransform[i].GetInstanceID(), _col);
        }

        for (int i = 0; i < PlayerSpawnTransform.Count; i++)
        {
            var _col = PlayerSpawnTransform[i].GetComponent<Collider>();
            P_Spawn.Add(PlayerSpawnTransform[i].GetInstanceID(), _col);
        }

        for (int i = 0; i < BoxSpawnTransform.Count; i++)
        {
            var _col = BoxSpawnTransform[i].GetComponent<Collider>();
            B_Spawn.Add(BoxSpawnTransform[i].GetInstanceID(), _col);
        }
    }

    private IEnumerator TargetSpawner()
    {

        for (int i = 0; i < MaxBoxOnScreen; i++)
        {
            curBoxNum = i;
            myPV.RPC("RPC_BoxSpawn", RpcTarget.All, curBoxNum);
            yield return delay;
        }

        while (true)
        {
            if (curPedestrianNum == MaxPedestrianOnScreen)
            {
                StartCounting = true;
            }
            yield return IsReachCap;
            int ids = SpawnTransform[UnityEngine.Random.Range(0, SpawnTransform.Count)].GetInstanceID();
            var SelSpawnPosition = GetRandomPosition(ids);
            myPV.RPC("RPC_SpawnObject", RpcTarget.All, SelSpawnPosition);
            yield return SpawnDelay;
            myPV.RPC("RPC_SetIsLoadedLevel", RpcTarget.All, true);
            yield return delay;
        }
    }

    private Vector3 GetRandomPosition(int instanceID)
    {
        var _x = UnityEngine.Random.Range(-RandomX, RandomX);
        var _z = UnityEngine.Random.Range(-RandomX / 2, RandomX / 2);
        var s = M_Spawn[instanceID];
        var _tempPos = new Vector3(_x, RandomY, _z);
        var position = s.ClosestPointOnBounds(_tempPos);
        var _offset = new Vector3(UnityEngine.Random.Range(-RandomY, RandomY) / 2f, 0f, UnityEngine.Random.Range(-RandomY, RandomY) / 2f);
        return position + _offset;
    }

    private Vector3 GetBoxRandomPosition(int instanceID)
    {
        var _x = UnityEngine.Random.Range(-BoxRandomX, BoxRandomX);
        var _z = UnityEngine.Random.Range(-BoxRandomZ, BoxRandomZ);
        var s = B_Spawn[instanceID];
        var _tempPos = new Vector3(_x, s.transform.position.y, _z);
        var position = s.ClosestPointOnBounds(_tempPos);
        var _offset = new Vector3(UnityEngine.Random.Range(-BoxRandomX, BoxRandomX) / 2f, 0f, UnityEngine.Random.Range(-BoxRandomZ, BoxRandomZ) / 2f);
        return position + _offset;
    }

    private Vector3 GetPlayerRandomPosition(int instanceID)
    {
        var _x = UnityEngine.Random.Range(-RandomX, RandomX);
        var _z = UnityEngine.Random.Range(-RandomX / 2, RandomX / 2);
        var s = P_Spawn[instanceID];
        var _tempPos = new Vector3(_x, s.transform.position.y, _z);
        var position = s.ClosestPointOnBounds(_tempPos);
        var _offset = new Vector3(UnityEngine.Random.Range(-RandomY, RandomY) / 2f, 0f, UnityEngine.Random.Range(-RandomY, RandomY) / 2f);
        return position + _offset;
    }

    private bool PlayersLoadLevel()
    {
        foreach (var Player in PhotonNetwork.PlayerList)
        {
            object playerloaded;

            if (Player.CustomProperties.TryGetValue(UniversalHandler.PLAYER_LOAD_LEVEL, out playerloaded))
            {
                if ((bool)playerloaded)
                {
                    continue;
                }
            }

            return false;
        }

        return true;
    }

    async Task DelayedDetroy(GameObject g)
    {
        await DestroyDelay;
        PhotonNetwork.Destroy(g.gameObject);
    }

    private async void RemovePedestrian(int ids)
    {
        if (PedestrianPool.ContainsKey(ids))
        {
            var g = PedestrianPool[ids];
            curPedestrianNum--;
            MaxPedestrianForMatch--;
            g.GetComponent<PedestrianHandler>().OnOrderDone -= RemovePedestrian;
            await DelayedDetroy(g.gameObject);
            PedestrianPool.Remove(ids);
        }
    }

    void SetCurrentVehicle(IPlayerHandler handler, PhotonView player)
    {
        handler.SetPlayerVehicle(player);
    }

    #region Photon

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        if (!targetPlayer.IsMasterClient && !GameIsOn)
        {
            return;
        }

        int StartTimestamp;
        timerIsSet = CountdownTimer.TryGetStartTime(out StartTimestamp);

        if (changedProps.ContainsKey(UniversalHandler.PLAYER_LOAD_LEVEL))
        {
            if (PlayersLoadLevel())
            {
                if (!timerIsSet)
                {
                    CountdownTimer.SetStartTime();
                }
            }
            else
            {
                Debug.Log("Wait for other player...");
            }
        }

    }


    [PunRPC]
    public void RPC_SpawnObject(Vector3 pos)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (PhotonNetwork.IsConnected)
            {
                var rand = UnityEngine.Random.Range(0, PedestrianName.Count);
                //var g = PhotonNetwork.InstantiateRoomObject(Path.Combine("PhotonResources", "Pedestrians", PedestrianName[rand]), pos, Quaternion.identity);
                var g = PhotonNetwork.InstantiateRoomObject("PhotonResources/Pedestrians/" + PedestrianName[rand], pos, Quaternion.identity);
                g.GetComponent<PedestrianHandler>().OnOrderDone += RemovePedestrian;
                UniversalSceneLoader.Instance.TransferObjectScene(g, UniversalSceneLoader.Instance.getmapname());
                if (!PedestrianPool.ContainsKey(g.GetHashCode()))
                {
                    PedestrianPool.Add(g.GetHashCode(), g.transform);
                }
                curPedestrianNum++;
            }
        }
    }

    [PunRPC]
    public void RPC_UpdateScoreGlobally()
    {
        UpdatePlayerScore();
    }

    [PunRPC]
    void RPC_BoxSpawn(int curNumber)
    {
        curBoxNum = curNumber;
        var position = GetBoxRandomPosition(BoxSpawnTransform[UnityEngine.Random.Range(0, BoxSpawnTransform.Count)].GetInstanceID());
        var g = PhotonNetwork.InstantiateRoomObject("PhotonResources/" + "PowerBox", position, Quaternion.identity);
        curBoxNum++;
    }

    [PunRPC]
    public void RPC_UpdateTimeGlobally(float time)
    {
        var t = (int)time;
        UpdatePlayerTime(t);
    }

    [PunRPC]
    private void RPC_SetIsLoadedLevel(bool v)
    {
        SceneIsLoaded = v;
    }


    private void EventReceiver(EventData obj)
    {
        var _dataRec = obj.Code;

        switch (_dataRec)
        {
            case UniversalHandler.MATCH_END:
                if (GameEnd == null)
                {
                    GameEnd = StartCoroutine(EndGame(obj));
                }
                break;
        }

    }

    private IEnumerator EndGame(EventData obj)
    {
        //Hashtable prop = new Hashtable();
        //prop.Add(UniversalHandler.PLAYER_LOAD_LEVEL, false);
        //PhotonNetwork.LocalPlayer.SetCustomProperties(prop);
        object[] data = (object[])obj.CustomData;
        StartCounting = false;
        UniversalAddresableLoader.Instance.Spawn(LoadingRefHUD, Vector3.zero, null, OnDoneSpawnLoading);
        yield return isLoadingEstablish;
        yield return LoadingDelay;
        UniversalSceneLoader.Instance.PhotonLoadScene("Podium");
        GameIsOn = (bool)data[0];
        yield return IsPodiumSceneLoaded;
        GameEnd = null;
        yield break;
    }

    private void OnDoneSpawnLoading(GameObject obj)
    {
        LoadingInstance = obj;
    }

    #endregion
}
