﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CDS.Power;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;

public class VehicleHandler : MonoBehaviourPunCallbacks, IPunOwnershipCallbacks
{
    [SerializeField] VehicleUser M_VehicleUser;
    [SerializeField] VehicleControlType M_VehicleControl;
    [SerializeField] bool RearWheelNotSteering;
    [SerializeField] float SteerAngle = 20f;
    [SerializeField] float MaxSteerAngle = 45f;
    [SerializeField] float MaxTorque = 300f;
    [SerializeField] Vector3 CenterOfMass;
    [SerializeField] Vector3 VehicleOffset;

    [SerializeField] GameObject VehicleMesh;
    [SerializeField] Collider SphereWheelie;
    [SerializeField] Transform VehicleNormal;

    [SerializeField] PowerUpManager MyDriverSkill;

    Animator MyAnim;
    VehicleController myController;
    List<WheelColliderTransformSync> WheelColliderSync = new List<WheelColliderTransformSync>();
    ControllerProcessor CP = new ControllerProcessor();
    StateProcessor States = new StateProcessor();
    NgojekHandler Ngojek;
    CombatAudioHelper SoundHelper;
    Coroutine PowerCoroutine = null;
    Coroutine CooldownCoroutine = null;
    WaitForEndOfFrame delay;

    public delegate void UpdateLocalPlayerVehicle();
    public event UpdateLocalPlayerVehicle LetUpdateLocalPlayerVehicle;
    int BoxPowerID;
    float SkillDowntime = 0f;

    private void Start()
    {
        delay = new WaitForEndOfFrame();
        SoundHelper = GetComponent<CombatAudioHelper>();
        //    WheelColliderSync = FindWheelCollider();
        myController = GetComponent<VehicleController>();
        Ngojek = GetComponent<NgojekHandler>();
        MyAnim = GetComponent<Animator>();
        myController.InitVehicleController(MaxSteerAngle, MaxTorque, M_VehicleUser, M_VehicleControl, CenterOfMass, VehicleMesh, SphereWheelie, VehicleNormal, MyAnim, MyDriverSkill);
        //SeparateWheelie();

        PreGameStartPlayerInit();
    }

    private void PreGameStartPlayerInit()
    {
        if (photonView.IsMine)
        {
            var pHandler = GameObject.FindGameObjectWithTag("Game Settings").GetComponent<PlayerHandler>();
            var h = transform.parent.GetChild(1).transform;
            h.gameObject.SetActive(true);
            SetCurrentOwnership(pHandler, photonView);

            var Minicam = GameObject.FindGameObjectWithTag("Minimap Camera");

            Minicam.GetComponent<MinimapHandler>().SetTarget(photonView.transform);
            Minicam.GetComponent<MinimapHandler>().isFollow = true;
            Minicam.GetComponent<MinimapHandler>().ActivateMinimap();
        }
    }

    private void SetCurrentOwnership(IPlayerHandler pHandler, PhotonView photonView)
    {
        pHandler.SetPlayerVehicle(photonView);
    }

    public void SetBoxPowerID(int i)
    {
        BoxPowerID = i;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.NetworkingClient.EventReceived += RaiseEvents;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.NetworkingClient.EventReceived -= RaiseEvents;
    }


    private void SeparateWheelie()
    {
        SphereWheelie.transform.parent = null;
    }

    private List<WheelColliderTransformSync> FindWheelCollider()
    {
        List<WheelColliderTransformSync> Result = new List<WheelColliderTransformSync>();
        var find_ = GetComponentsInChildren<WheelColliderTransformSync>();
        for (int i = 0; i < find_.Length; i++)
        {
            Result.Add(find_[i]);
        }
        return Result;
    }

    private void Update()
    {
        if (!States.ValidateState(myController) && !photonView.Owner.IsLocal) return;

        CP.DoJump(M_VehicleControl, myController);

        LetUpdateLocalPlayerVehicle.Invoke();

        if (CP.OnClickPowerButton(myController))
        {
            object[] data = new object[] { photonView.ViewID, UniversalHandler.CLIENT_ABILITES };
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(UniversalHandler.USE_ABILITIES, data, eventOptions, SendOptions.SendReliable);
        }

        if (CP.OnClickBoxPowerButton(myController))
        {
            object[] data = new object[] { photonView.ViewID, UniversalHandler.BOX_ABILITIES };
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(UniversalHandler.USE_ABILITIES, data, eventOptions, SendOptions.SendReliable);
        }

        if (CP.OnClickDriverPowerButton(myController))
        {
            if (photonView.IsMine)
            {
                if (MyDriverSkill.CurrentActiveSkill != null && CooldownCoroutine == null)
                {
                    SoundHelper.PlayAudio(SoundHelper.Voices.m_UseSkill, 1);
                    MyDriverSkill.SetUsage();
                    MyDriverSkill.CurrentActiveSkill.Fire();
                    if (MyDriverSkill.GetCurrentUsage() >= MyDriverSkill.GetMaxUsage())
                    {
                        InitCooldownState();
                    }
                    return;
                }
                object[] data = new object[] { photonView.ViewID, UniversalHandler.DRIVER_ABILITIES };
                RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
                PhotonNetwork.RaiseEvent(UniversalHandler.USE_ABILITIES, data, eventOptions, SendOptions.SendReliable);
            }
        }
    }

    void FixedUpdate()
    {
        if (!photonView.IsMine)
        {
            CP.WheelieRotation(myController);
            return;
        }

        if (M_VehicleControl.Equals(VehicleControlType.Radical))
        {
            CP.SyncMesh(myController);
        }

        if (States.ValidateState(myController))
        {
            CP.WheelieRotation(myController);
            CP.DoSteering(M_VehicleControl, myController, WheelColliderSync, RearWheelNotSteering);
            SyncWheels();
        }

        if (M_VehicleControl.Equals(VehicleControlType.Radical))
        {
            CP.MeshFollow(myController, VehicleOffset);
        }
    }

    private void SyncWheels()
    {
        for (int i = 0; i < WheelColliderSync.Count; i++)
        {
            WheelColliderSync[i].SyncTransform();
        }
    }

    internal int GetBoxPowerID()
    {
        return BoxPowerID;
    }

    internal PowerUpManager GetDriverSkill()
    {
        return MyDriverSkill;
    }

    private IEnumerator myCooldown()
    {
        SkillDowntime = MyDriverSkill.GetDowntime();
        yield return new WaitForSeconds(SkillDowntime);
        CooldownCoroutine = null;
        Debug.Log("CD refreshed.");
        MyDriverSkill.ResetUsage();
        yield break;
    }

    internal void InitCooldownState()
    {
        if (CooldownCoroutine == null)
        {
            CooldownCoroutine = StartCoroutine(myCooldown());
        }
    }


    #region RPC & Raise Event

    private void RaiseEvents(EventData obj)
    {
        StateProcessor states = new StateProcessor();

        switch (obj.Code)
        {
            case UniversalHandler.USE_ABILITIES:
                object[] data = (object[])obj.CustomData;
                if (photonView.ViewID != (int)data[0])
                    return;

                bool AlreadyInstanced = MyDriverSkill.CurrentActiveSkill;
                if ((int)data[1] == UniversalHandler.DRIVER_ABILITIES)
                {
                    SoundHelper.PlayAudio(SoundHelper.Voices.m_UseSkill, 1);
                    if (AlreadyInstanced)
                    {
                        PowerCoroutine = null;
                        break;
                    }
                }

                if (PowerCoroutine == null)
                {
                    PowerCoroutine = StartCoroutine(CP.UsePower(myController, Ngojek, transform, (int)data[1]));
                }
                break;

            case UniversalHandler.ABILITIES_END:
                object[] data1 = (object[])obj.CustomData;
                if (photonView.ViewID != (int)data1[0])
                    return;

                PowerCoroutine = null;
                break;

            case UniversalHandler.ABILITIES_COOLDOWN:
                object[] data3 = (object[])obj.CustomData;
                if (photonView.ViewID != (int)data3[0])
                    return;

                InitCooldownState();
                PowerCoroutine = null;
                break;

            case UniversalHandler.PLAYER_REMOTE_KNOCK:
                object[] data2 = (object[])obj.CustomData;
                if (photonView.ViewID != (int)data2[0])
                    return;


                StartCoroutine(CP.KnockMe(myController, (float)data2[1], (int)data2[2], (float)data2[3], transform.position));
                states.SerFrozenEffect(myController);
                GetComponent<NgojekHandler>().ReleasePedestrian();
                break;
        }
    }

    [PunRPC]
    private void OnActivateAbility(int AbilityId) //visual effect helper.
    {
        switch (AbilityId)
        {
            case UniversalHandler.MBAH_ABILITY_1:
                IVehicleController con = myController;
                con.ShakeCamera();
                UniversalHandler.Instance.VFX.CallParticles("Skill_MbahShockwave", transform.position);
                break;

            case UniversalHandler.PREMAN_ABILITY_1:
                break;
        }
    }

    [PunRPC]
    private void OnReceiveHit(int AbilityId)
    {
        switch (AbilityId)
        {
            case UniversalHandler.MBAH_ABILITY_1:
                if (photonView.IsMine)
                {
                    IVehicleController con = myController;
                    con.ShakeCamera();
                    UniversalHandler.Instance.VFX.CallParticles("Impact_General", transform.position);

                    States.SetSlowedEffect(myController);
                }
                break;

            case UniversalHandler.PREMAN_ABILITY_1:
                break;
        }
    }

    [PunRPC]
    private void PlaySoundKnocked()
    {
        if (photonView.IsMine)
        {
            SoundHelper.PlayAudio(SoundHelper.Voices.m_Knocked, 1);
        }
    }

    [PunRPC]
    private void DoneAbilties()
    {
        object[] data = new object[] { photonView.ViewID };
        RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(UniversalHandler.ABILITIES_END, data, eventOptions, SendOptions.SendReliable);
    }

    [PunRPC]
    private void CooldownAbilities()
    {
        object[] data = new object[] { photonView.ViewID };
        RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(UniversalHandler.ABILITIES_COOLDOWN, data, eventOptions, SendOptions.SendReliable);
    }

    void IPunOwnershipCallbacks.OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        targetView.TransferOwnership(requestingPlayer);
    }

    void IPunOwnershipCallbacks.OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {

    }

    #endregion
}

public class StateProcessor
{

    public IEnumerator SetSlowedEffect(IVehicleController controller)
    {
        SetVehicleState(controller, VehicleStates.Mbah_Slowdown);
        yield return new WaitForSeconds(1f);
        SetVehicleState(controller, VehicleStates.Normal);
        yield break;
    }

    public IEnumerator FallDownStunEffect(IVehicleController controller)
    {
        SetVehicleState(controller, VehicleStates.Freeze);
        yield return new WaitForSeconds(1f);
        SetVehicleState(controller, VehicleStates.Normal);
        yield break;
    }

    public IEnumerator FallDownStunEffect(IVehicleController controller, IEnumerator followUp)
    {

        SetVehicleState(controller, VehicleStates.Freeze);
        Debug.Log("freeezee");
        yield return new WaitForSeconds(1f);
        SetVehicleState(controller, VehicleStates.Normal);
        Debug.Log("normal");
        yield return followUp;
        yield break;
    }

    public IEnumerator SerFrozenEffect(IVehicleController controller)
    {
        SetVehicleState(controller, VehicleStates.Freeze);
        Debug.Log("freeezee");
        yield return new WaitForSeconds(2f);
        SetVehicleState(controller, VehicleStates.Normal);
        Debug.Log("normal");
        yield break;
    }

    public IEnumerator SetFrozenEffect(IVehicleController controller, IEnumerator followUp)
    {
        SetVehicleState(controller, VehicleStates.Freeze);
        Debug.Log("freeezee");
        yield return new WaitForSeconds(2f);
        SetVehicleState(controller, VehicleStates.Normal);
        Debug.Log("normal");
        yield return followUp;
        yield break;
    }

    public bool ValidateState(IVehicleController controller)
    {
        switch (controller.GetState())
        {
            case VehicleStates.Freeze:
                //freeze//
                return false;

            case VehicleStates.KO:
                //dead//
                return false;

            case VehicleStates.Mbah_Slowdown:
                return true;

            case VehicleStates.Invulnerable:
                return true;

            default:
                return true;
        }
    }

    public void SetVehicleState(IVehicleController controller, VehicleStates states)
    {
        controller.SetState(states);
    }


    public VehicleStates GetStates(IVehicleController handler)
    {
        return handler.GetState();
    }

}


public class ControllerProcessor
{
    public void DoSteering(IVehicleController myController, List<WheelColliderTransformSync> Wheels, bool BackWheelSteering)
    {
        myController.DoSteering();

        for (int i = 0; i < Wheels.Count; i++)
        {
            myController.Accel(Wheels[i].Wheel);

            if (BackWheelSteering)
            {
                if (!Wheels[i].isFrontWheels)
                    continue;
            }

            myController.ChangeWheelAngle(Wheels[i].Wheel);
        }
    }

    public void DoSteering(VehicleControlType types, IVehicleController myController, List<WheelColliderTransformSync> Wheels, bool BackWheelSteering)
    {
        if (types == VehicleControlType.Radical)
        {
            myController.DoSteering();

            myController.Accel();

        }
    }

    public void DoJump(VehicleControlType types, IVehicleController vehicleController)
    {
        if (types == VehicleControlType.Radical)
            vehicleController.DoJump();
    }

    public IEnumerator UsePower(IVehicleController controller, INgojeck user, Transform owner, int identifier) //TESTING SIR//
    {
        switch (identifier)
        {
            case UniversalHandler.CLIENT_ABILITES:
                if (user.GetPowerFromClient() == null)
                {
                    owner.GetComponent<PhotonView>().RPC("DoneAbilties", RpcTarget.All);
                    yield break;
                }

                yield return PowerFactory.GetPower(user.GetPowerFromClient().GetPower()).CastPowerCoroutine(owner, user.GetPowerFromClient().GetCurrentUsage(), user.GetPowerFromClient().GetMaxUsage(), user.GetPowerFromClient().GetDowntime(), user.GetPowerFromClient().GetDuration(), false);
                user.GetPowerFromClient().SetUsage();

                yield break;

            case UniversalHandler.BOX_ABILITIES:
                var vHandler = owner.GetComponent<VehicleHandler>().GetBoxPowerID();
                if (user.GetPowerFromBox() == null)
                {
                    owner.GetComponent<PhotonView>().RPC("DoneAbilties", RpcTarget.All);
                    yield break;
                }

                Debug.Log("ability used");
                yield return PowerFactory.GetPower((PowersEnum)vHandler).CastPowerCoroutine(owner, user.GetPowerFromBox().GetCurrentUsage(), user.GetPowerFromBox().GetMaxUsage(), user.GetPowerFromBox().GetDowntime(), user.GetPowerFromBox().GetDuration(), false);
                user.GetPowerFromBox().SetUsage();

                yield break;

            case UniversalHandler.DRIVER_ABILITIES:
                var PowHandler = owner.GetComponent<VehicleHandler>().GetDriverSkill();
                if (PowHandler.GetCurrentUsage() >= PowHandler.GetMaxUsage())
                {
                    yield break;
                }

                yield return PowerFactory.GetPower((PowersEnum)PowHandler.GetPowerInt()).CastPowerCoroutine(owner, PowHandler.GetCurrentUsage(), PowHandler.GetMaxUsage(), PowHandler.GetDelayActivation(), PowHandler.GetDuration(), PowHandler.CurrentActiveSkill != null);
                PowHandler.SetUsage();
                yield break;
        }

        yield break;
    }

    //public IEnumerator UsePower(IVehicleController controller, INgojeck user, Transform owner) //TESTING SIR//
    //{
    //    if (user.GetPowerFromClient() == null)
    //    {
    //        owner.GetComponent<PhotonView>().RPC("DoneAbilties", RpcTarget.All);
    //        yield break;
    //    }

    //    yield return PowerFactory.GetPower(user.GetPowerFromClient().GetPower()).CastPowerCoroutine(owner, user.GetPowerFromClient().GetCurrentUsage(), user.GetPowerFromClient().GetMaxUsage());
    //    user.GetPowerFromClient().SetUsage();

    //    yield break;
    //}

    public bool OnClickPowerButton(IVehicleController controller)
    {
        return controller.GetSpecialButton();
    }

    public bool OnClickBoxPowerButton(IVehicleController controller)
    {
        return controller.GetBoxSpecialButton();
    }

    public bool OnClickDriverPowerButton(IVehicleController controller)
    {
        return controller.GetDriverSpecialButton();
    }

    public void BalanceVehicle(IVehicleController myController, bool LockZAxis)
    {
        myController.BalanceVehicle(LockZAxis);
    }

    public void WheelieRotation(IVehicleController myController)
    {
        myController.SetWheelieGravity();
    }

    public void MeshFollow(IVehicleController controller, Vector3 offset)
    {
        controller.FollowCollider(offset);
    }

    public void SyncMesh(IVehicleController myController)
    {
        myController.SyncMesh();
    }

    internal IEnumerator KnockMe(IVehicleController controller, float duration, int direction, float force, Vector3 position)
    {

        UniversalHandler.Instance.VFX.CallParticles("Impact_General", position);
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        var dur = duration;
        while (dur > 0f)
        {
            yield return delay;
            controller.Push(direction, force);
            dur -= 0.015f;
        }

        yield break;
    }
}

public enum VehicleUser
{
    AI = 0,
    Player = 1
}

public enum VehicleControlType
{
    Classic = 0,
    Radical = 1
}
