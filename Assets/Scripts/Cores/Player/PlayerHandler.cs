﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class PlayerHandler : MonoBehaviourPunCallbacks, IPlayerHandler
{
    PhotonView CurrentPlayerVehicle;
    Coroutine OnDisconnected;
    Coroutine OnReconnected;

    Player CurrentOwner;
    int OwnerActorNr;
    string CurrentRoom;
    WaitForSeconds delay = new WaitForSeconds(0.5f);
    WaitForEndOfFrame framedelay = new WaitForEndOfFrame();

    void IPlayerHandler.SetCurrentVehicleOwnership()
    {
        if (OnDisconnected == null)
        {
            OnDisconnected = StartCoroutine(OnDisconnectCoroutine());
        }
    }

    public PhotonView GetCurrentPlayer()
    {
        return CurrentPlayerVehicle;
    }

    private IEnumerator OnDisconnectCoroutine()
    {
        PhotonNetwork.IsMessageQueueRunning = false;
        if (CurrentPlayerVehicle.OwnerActorNr == 0)
        {
            OnDisconnected = null;
            yield break;
        }
        CurrentOwner = CurrentPlayerVehicle.Owner;
        OwnerActorNr = CurrentPlayerVehicle.OwnerActorNr;
        CurrentRoom = PhotonNetwork.CurrentRoom.Name;
        while (CurrentPlayerVehicle.Owner == CurrentOwner)
        {
            yield return framedelay;
        }

        PhotonNetwork.IsMessageQueueRunning = true;
        yield return delay;
        OnDisconnected = null;

        yield break;
    }


    void IPlayerHandler.SetCurrentVehicleOwnership(Player player)
    {
        CurrentPlayerVehicle.TransferOwnership(player);
    }

    void IPlayerHandler.SetCurrentVehicleOwnership(int id)
    {
        CurrentPlayerVehicle.TransferOwnership(id);
    }

    void IPlayerHandler.SetPlayerVehicle(PhotonView view)
    {
        CurrentPlayerVehicle = view;

        CurrentRoom = PhotonNetwork.CurrentRoom.Name;
    }


    public void DoReconnect()
    {
        if (!PhotonNetwork.ReconnectAndRejoin())
        {
            if (!PhotonNetwork.Reconnect())
            {
                if (!PhotonNetwork.ConnectUsingSettings())
                {
                    return;
                }
            }
        }


        //if (OnReconnected == null)
        //{
        //    OnReconnected = StartCoroutine(OnReconnectCoroutine());
        //}
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
    }

    public override void OnJoinedRoom()
    {
        if (OnReconnected == null) return;

        base.OnJoinedRoom();

        CurrentOwner = PhotonNetwork.LocalPlayer;
    }

    private IEnumerator OnReconnectCoroutine()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.IsMessageQueueRunning = true;
            while (!PhotonNetwork.ConnectUsingSettings())
            {
                PhotonNetwork.ConnectUsingSettings();
                Debug.Log("connecting...");
                yield return delay;
            }
        }
        PhotonNetwork.SendAllOutgoingCommands();

        yield return new WaitUntil(() => PhotonNetwork.IsConnectedAndReady);

        yield return delay;
        OnReconnected = null;

        yield break;

    }

    void IPlayerHandler.ResetDisconnectCoroutine()
    {
        OnDisconnected = null;
    }


}
