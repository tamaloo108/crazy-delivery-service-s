﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using System;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Threading.Tasks;
using Photon.Realtime;

public class NgojekHandler : MonoBehaviourPunCallbacks, INgojeck, IPunObservable
{
    [SerializeField] Transform KursiPenumpang;
    [SerializeField] float AutoPickUpTime = .45f;

    Vector3 StartPosition;
    VehicleController myController;
    PedestrianProcessor Processor;
    Vector3 currentDestination;
    Transform CurrentTargetDestinationTransform;
    ControllerHUDHandler myHUDController;
    GameObject Arrow;
    PedestrianDestinationHandler CurrentDestinationHandler;
    PedestrianHandler CurrentPedestrian;
    ArrowBehavior _ArrowBehavior;
    PowerUpManager BoxPower;
    StateProcessor StateProc;
    CombatAudioHelper SoundHelper;
    bool CanPickup = false;
    bool OnDriverTravel = false;
    bool CanDrop = false;
    bool OnStealPenalty = false;
    float TargetDist = 0f;
    float m_AutoPickUpLeft = 0f;
    float PenaltyTime = 0.322f;

    bool CanUpdateArrow = false;
    WaitForSeconds delay = new WaitForSeconds(0.2f);
    WaitForSeconds PenaltyTime_;
    WaitForSeconds CommonSoundsDelay;

    Coroutine Invincibilities;
    Coroutine ArrowCoroutine;
    Coroutine PenaltyTimeCoroutine;
    Coroutine CommonSoundResponCoroutine;


    private void Start()
    {
        CommonSoundsDelay = new WaitForSeconds(15f);
        StateProc = new StateProcessor();
        myController = GetComponent<VehicleController>();
        Processor = new PedestrianProcessor();
        StartPosition = transform.position;
        PenaltyTime_ = new WaitForSeconds(PenaltyTime);
        SoundHelper = GetComponent<CombatAudioHelper>();
    }

    public Transform GetKursiPenumpang()
    {
        return KursiPenumpang;
    }

    private void Update()
    {
        if (CanUpdateArrow && KursiPenumpang.childCount > 0)
        {
            UpdateArrowDistance();
            UpdateArrowRotation();
        }

        myController.GetControllerHUD().SetDriverSpecialButtonState(true, myController.GetMySkill().GetPower(), myController.GetMySkill().GetCurrentUsage(), myController.GetMySkill().GetMaxUsage());

        if (CanPickup && !OnDriverTravel)
        {
            if (CurrentDestinationHandler == null || CurrentDestinationHandler.IsOnTravel() || CurrentPedestrian.isDisableOrder() || OnStealPenalty) return;

            if (UniversalHandler.Instance.GameSettings.IsAutoPickup())
            {
                if (AutoPickUpAfterSeconds(true))
                {
                    if (photonView.IsMine)
                    {
                        if (CommonSoundResponCoroutine == null)
                        {
                            CommonSoundResponCoroutine = StartCoroutine(PlayVoiceCoroutine());
                        }
                    }

                    CurrentPedestrian.SendRPC(true, "Sit");
                    CurrentDestinationHandler.SendRPC(true, true, photonView.ViewID);

                    if (photonView.IsMine)
                        SoundHelper.PlayAudio("SFX_GetClient_1", 0);

                    myController.GetControllerHUD().SetPickButtonState(false, false);
                    myController.GetControllerHUD().SetClientSpecialButtonState(!Processor.OnMaxUsage(this), CurrentPedestrian.GetMyPower(), Processor.GetCurrentUsage(this), Processor.GetMaxUsage(this));
                    SendRPC(Processor.GetCurrentDestinationTransform(CurrentDestinationHandler), true);
                    SetTargetDistValue();
                    m_AutoPickUpLeft = 0f;
                    myController.GetControllerHUD().ResetAutoPickUpValue(0f);
                }

            }
            else
            {
                if (Processor.OnPickUpButtonClick(myController))
                {
                    CurrentPedestrian.SendRPC(true, "Sit");
                    CurrentDestinationHandler.SendRPC(true, true, photonView.ViewID);
                    myController.GetControllerHUD().SetPickButtonState(false, false);
                    myController.GetControllerHUD().SetClientSpecialButtonState(!Processor.OnMaxUsage(this), CurrentPedestrian.GetMyPower(), Processor.GetCurrentUsage(this), Processor.GetMaxUsage(this));
                    SendRPC(Processor.GetCurrentDestinationTransform(CurrentDestinationHandler), true);
                    SetTargetDistValue();
                }
            }
        }
        else if (CanDrop && OnDriverTravel)
        {
            if (UniversalHandler.Instance.GameSettings.IsAutoPickup())
            {
                if (AutoPickUpAfterSeconds(false))
                {
                    _ArrowBehavior.SetState(ArrowState.Secured);
                    myController.GetControllerHUD().SetPickButtonState(true, false);
                    if (photonView.IsMine)
                    {
                        SoundHelper.PlayAudio(SoundHelper.Voices.m_Respond, 1);
                    }
                    myController.GetControllerHUD().SetClientSpecialButtonState(false, PowersEnum.None, 0, 0);
                    _ArrowBehavior.SetState(ArrowState.Hidden);
                    CanUpdateArrow = false;
                    CurrentPedestrian.SendRPC(false, "Idle");
                    CurrentDestinationHandler.SendRPC(false, false, photonView.ViewID);
                    SendRPC(Vector3.zero, false);
                    CurrentPedestrian.DoneTravelling();
                    CurrentPedestrian = null;
                    CurrentDestinationHandler = null;
                    photonView.Owner.AddScore(20);
                    m_AutoPickUpLeft = 0f;
                    myController.GetControllerHUD().ResetAutoPickUpValue(0f);
                }
            }
            else
            {
                _ArrowBehavior.SetState(ArrowState.Secured);
                myController.GetControllerHUD().SetPickButtonState(true, false);
                if (Processor.OnPickUpButtonClick(myController))
                {
                    //do drop thing.
                    myController.GetControllerHUD().SetClientSpecialButtonState(false, PowersEnum.None, 0, 0);
                    _ArrowBehavior.SetState(ArrowState.Hidden);
                    if (photonView.IsMine)
                    {
                        SoundHelper.PlayAudio(SoundHelper.Voices.m_Respond, 1);
                    }
                    CanUpdateArrow = false;
                    CurrentPedestrian.SendRPC(false, "Idle");
                    CurrentDestinationHandler.SendRPC(false, false, photonView.ViewID);
                    SendRPC(Vector3.zero, false);
                    CurrentPedestrian.DoneTravelling();
                    CurrentPedestrian = null;
                    CurrentDestinationHandler = null;
                    photonView.Owner.AddScore(20);

                }
            }
        }

        if (OnDriverTravel)
        {
            if (photonView.Owner.IsLocal)
            {
                myController.GetControllerHUD().SetClientSpecialButtonState(!Processor.OnMaxUsage(this), CurrentPedestrian.GetMyPower(), Processor.GetCurrentUsage(this), Processor.GetMaxUsage(this));
            }
        }

        if (BoxPower != null)
        {
            if (photonView.Owner.IsLocal)
            {
                myController.GetControllerHUD().setBoxPowerButtonState(!(BoxPower.GetCurrentUsage() >= BoxPower.GetMaxUsage()), BoxPower.GetPower(), BoxPower.GetCurrentUsage(), BoxPower.GetMaxUsage());
                if (BoxPower.GetCurrentUsage() >= BoxPower.GetMaxUsage())
                {
                    photonView.RPC("RPC_ReleasePowerBox", RpcTarget.All, true);
                }
            }
        }
    }

    private IEnumerator PlayVoiceCoroutine()
    {
        yield return CommonSoundsDelay;

        SoundHelper.PlayAudio(SoundHelper.Voices.m_Idle, 1);
        yield return CommonSoundsDelay;
        CommonSoundResponCoroutine = null;
        yield break;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        myController.GetControllerHUD().SetClientSpecialButtonState(false, PowersEnum.None, 0, 0);
        myController.GetControllerHUD().setBoxPowerButtonState(false, PowersEnum.None, 0, 0);
        photonView.RPC("RPC_ReleasePowerBox", RpcTarget.All, true);
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        myController.GetControllerHUD().SetClientSpecialButtonState(false, PowersEnum.None, 0, 0);
        myController.GetControllerHUD().setBoxPowerButtonState(false, PowersEnum.None, 0, 0);
        photonView.RPC("RPC_ReleasePowerBox", RpcTarget.All, true);
    }

    private bool AutoPickUpAfterSeconds(bool isOnPickUp)
    {
        if (photonView.Owner.IsLocal)
        {
            m_AutoPickUpLeft += Time.deltaTime;
            float values = ConvertRange(m_AutoPickUpLeft, 0f, 0f, 1f, AutoPickUpTime);
            myController.GetControllerHUD().OnAutoPickDropState(isOnPickUp, values);
            if (m_AutoPickUpLeft >= AutoPickUpTime)
            {
                return true;
            }
        }

        return false;
    }

    private void SetTargetDistValue()
    {
        TargetDist = (currentDestination - transform.position).sqrMagnitude;
    }

    private void UpdateArrowRotation()
    {
        Vector3 diff = currentDestination - _ArrowBehavior.transform.position;
        float rotateY = Mathf.Atan2(diff.x, diff.z) * Mathf.Rad2Deg;
        _ArrowBehavior.transform.rotation = Quaternion.Euler(-30f, rotateY, 0f);
    }

    private void UpdateArrowDistance()
    {
        if (CanDrop) return;
        var curDist = (currentDestination - transform.position).sqrMagnitude;
        var val = ConvertRange(TargetDist, 0f, 0f, 1f, curDist);
        _ArrowBehavior.SetAnimSpeed(val);
    }

    float ConvertRange(float Val, float OldMin, float newMin, float newMax, float oldMax)
    {
        return (((Val - OldMin) * newMax) / oldMax) + newMin;
    }

    private IEnumerator SendKnock(Collider other)
    {
        var pView = other.GetComponent<PhotonView>();
        photonView.Owner.AddScore(2);
        yield return delay;
        object[] content = new object[]
        {
            pView.ViewID,
            .2f,
            UniversalHandler.KNOCK_UP,
            1.05f
        };

        RaiseEventOptions _RaiseEventOpt = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(UniversalHandler.PLAYER_REMOTE_KNOCK, content, _RaiseEventOpt, SendOptions.SendReliable);
        Invincibilities = null;
        yield break;
    }

    internal void SetHUD(ControllerHUDHandler hUDController)
    {
        hUDController = myHUDController;
    }

    #region RPCs

    private void SendRPC(Transform target, bool travelState)
    {
        var position = target.position;
        CurrentTargetDestinationTransform = target;

        photonView.RPC("RPC_SetDestination", RpcTarget.All, position);
        photonView.RPC("RPC_SetOnTravelState", RpcTarget.All, travelState);
        if (travelState)
        {
            photonView.RPC("RPC_ActivateArrow", RpcTarget.All);
        }
        else if (!travelState)
        {
            photonView.RPC("RPC_DeactivateArrow", RpcTarget.All);
        }
    }

    private void SendRPC(Vector3 position, bool travelState)
    {
        photonView.RPC("RPC_SetDestination", RpcTarget.All, position);
        photonView.RPC("RPC_SetOnTravelState", RpcTarget.All, travelState);
        if (travelState)
        {
            photonView.RPC("RPC_ActivateArrow", RpcTarget.All);
        }
        else if (!travelState)
        {
            photonView.RPC("RPC_DeactivateArrow", RpcTarget.All);
        }
    }

    [PunRPC]
    void RPC_SetOnState(byte values)
    {
        IVehicleController handler = myController;
        switch (values)
        {
            case UniversalHandler.PLAYER_CHARGE_ON:
                handler.SetState(VehicleStates.Charge);
                break;

            case UniversalHandler.PLAYER_CHARGE_OFF:
                handler.SetState(VehicleStates.Normal);
                break;

            case UniversalHandler.PLAYER_INVULNERABLE_ON:
                handler.SetState(VehicleStates.Invulnerable);
                break;

            case UniversalHandler.PLAYER_INVULNERABLE_OFF:
                handler.SetState(VehicleStates.Normal);
                break;
        }
    }

    [PunRPC]
    void RPC_PlayerFall()
    {
        Processor.ResetPosition(myController, StartPosition);
    }

    [PunRPC]
    void RPC_GetPowerBox(int pow)
    {
        //ReleasePowerBox();
        AddPowerBox(pow);
    }

    [PunRPC]
    void RPC_ReleasePowerBox(bool val)
    {
        try
        {
            Debug.Log("Called?");
            var pBoxView = BoxPower.gameObject.GetComponent<PhotonView>();
            pBoxView.RPC("RPC_HideBox", RpcTarget.All, val);
            ReleasePowerBox();
        }
        catch (Exception)
        {

        }

    }

    [PunRPC]
    void RPC_CatchLandmine(int id, PhotonMessageInfo info)
    {
        PhotonView boxView = PhotonView.Find(id);
        Debug.Log(id + " " + info.Sender);
        Debug.Log(photonView.ViewID);
        Destroy(boxView.gameObject);
    }

    private void AddPowerBox(int ID)
    {
        var vHandler = myController.GetComponent<VehicleHandler>();
        vHandler.SetBoxPowerID(ID);
    }

    private void ReleasePowerBox()
    {
        var vHandler = myController.GetComponent<VehicleHandler>();
        vHandler.SetBoxPowerID(0);
        BoxPower.ResetUsage();
        BoxPower = null;
    }

    [PunRPC]
    void RPC_SetDestination(Vector3 position)
    {
        currentDestination = position;
    }

    [PunRPC]
    void RPC_SetOnTravelState(bool TravelState)
    {
        OnDriverTravel = TravelState;
    }

    [PunRPC]
    async void RPC_ActivateArrow()
    {
        if (Arrow == null)
        {
            var _SpawnArrow = SpawnArrow();
            await _SpawnArrow;
            Arrow.transform.SetParent(transform);
            Arrow.transform.position = Vector3.zero;
            _ArrowBehavior = Arrow.AddComponent<ArrowBehavior>();
            Arrow.transform.localPosition = Vector3.up * 2f;
        }

        _ArrowBehavior.SetState(ArrowState.Point);
        CanUpdateArrow = true;
    }

    [PunRPC]
    void RPC_DeactivateArrow()
    {
        _ArrowBehavior.SetState(ArrowState.Hidden);
        CanUpdateArrow = false;
    }

    private void ArrowDoneLoad(AsyncOperationHandle<GameObject> obj)
    {
        Arrow = obj.Result;
    }

    async Task<GameObject> SpawnArrow()
    {
        WaitUntil wait = new WaitUntil(() => Arrow != null);
        Addressables.InstantiateAsync("Arrow").Completed += ArrowDoneLoad;
        await wait;
        return Arrow;
    }

    #endregion

    #region Collision
    private void OnTriggerStay(Collider other)
    {
        if (StateProc.GetStates(myController) == VehicleStates.Charge)
        {
            if (other.CompareTag("Player"))
            {
                var OtherState = StateProc.GetStates(other.GetComponent<VehicleController>()) == VehicleStates.Invulnerable;

                if (OtherState) return;

                if (Invincibilities == null)
                {
                    if (photonView.IsMine)
                    {
                        SoundHelper.PlayAudio("SFX_Hit_1", 0);
                        photonView.RPC("PlaySoundKnocked", RpcTarget.All);
                    }
                    Invincibilities = StartCoroutine(SendKnock(other));
                }
            }
        }

        if (other.tag.Equals("Pedestrian"))
        {
            CurrentPedestrian = other.gameObject.GetComponent<PedestrianHandler>();
            CurrentDestinationHandler = other.gameObject.GetComponent<PedestrianDestinationHandler>();

            if (OnDriverTravel) return;
            CanPickup = true;

            if (photonView.Owner.IsLocal)
            {
                myController.GetControllerHUD().SetPickButtonState(true, true);
            }
        }
        else if (other.tag.Equals("Destination"))
        {
            if (CanPickup) return;
            if (CurrentTargetDestinationTransform == null) return;
            CanDrop = CurrentTargetDestinationTransform == other.transform;

            if (photonView.Owner.IsLocal)
            {
                myController.GetControllerHUD().SetPickButtonState(true, false);
            }
        }
        else if (other.tag.Equals("Power Up Box"))
        {
            if (BoxPower != null) return;

            var pBox = other.transform.GetComponent<PowerUpManager>();
            var pBoxView = pBox.GetComponent<PhotonView>();
            //var AudioHelper = pBox.GetComponent<CombatAudioHelper>();


            if (photonView.IsMine)
            {
                SoundHelper.PlayAudio("SFX_GetBox_1", 0);
            }

            if (pBox.IsItBox())
            {
                if (photonView.Owner.IsLocal)
                {
                    UniversalHandler.Instance.VFX.DrawVFX(other.transform.position, "BoxImpact_3");
                }

                photonView.RPC("RPC_GetPowerBox", RpcTarget.All, pBox.GetPowerInt());
                pBoxView.RPC("RPC_HideBox", RpcTarget.All, false);
                pBoxView.RPC("RPC_SetBoxPow", RpcTarget.All, photonView.ViewID);
            }

            BoxPower = pBox;
            BoxPower.VisibilityState(false);

            if (photonView.Owner.IsLocal)
            {
                myController.GetControllerHUD().setBoxPowerButtonState(true);
            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Pedestrian"))
        {
            if (!OnDriverTravel)
            {
                CurrentPedestrian = null;
                CurrentDestinationHandler = null;
            }

            CanPickup = false;
        }
        else if (other.tag.Equals("Destination"))
        {
            CanDrop = false;
            if (_ArrowBehavior != null && KursiPenumpang.childCount > 0)
            {
                _ArrowBehavior.SetState(ArrowState.Point);
            }
        }

        if (photonView.IsMine)
        {
            m_AutoPickUpLeft = 0f;
            myController.GetControllerHUD().ResetAutoPickUpValue(0f);
            myController.GetControllerHUD().SetPickButtonState(false, false);
        }
    }

    #endregion

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentDestination);
            stream.SendNext(CanPickup);
            stream.SendNext(CanDrop);
            stream.SendNext(OnDriverTravel);
            stream.SendNext(m_AutoPickUpLeft);
        }
        else
        {
            currentDestination = (Vector3)stream.ReceiveNext();
            CanPickup = (bool)stream.ReceiveNext();
            CanDrop = (bool)stream.ReceiveNext();
            OnDriverTravel = (bool)stream.ReceiveNext();
            m_AutoPickUpLeft = (float)stream.ReceiveNext();
        }
    }

    void INgojeck.SetDestination(Vector3 dest)
    {
        currentDestination = dest;
    }

    void INgojeck.SetOnTravelState(bool state)
    {
        OnDriverTravel = state;
    }

    bool INgojeck.GetOnTravelState()
    {
        return OnDriverTravel;
    }

    void INgojeck.ReleasePedestrian()
    {
        if (OnDriverTravel)
        {
            if (photonView.IsMine)
                SoundHelper.PlayAudio(SoundHelper.Voices.m_Knocked, 1);

            OnStealPenalty = true;
            myController.GetControllerHUD().SetPickButtonState(false, false);
            myController.GetControllerHUD().SetClientSpecialButtonState(false);
            _ArrowBehavior.SetState(ArrowState.Hidden);
            CanUpdateArrow = false;
            CurrentPedestrian.SendRPC(false, "Idle");
            CurrentDestinationHandler.SendRPC(false, false, photonView.ViewID);
            SendRPC(Vector3.zero, false);
            //CurrentPedestrian.DoneTravelling();
            CurrentPedestrian.OnReadySteal();
            CurrentPedestrian = null;
            CurrentDestinationHandler = null;
            if (PenaltyTimeCoroutine == null)
            {
                PenaltyTimeCoroutine = StartCoroutine(PenaltyTimeCounter());
            }
        }
    }

    private IEnumerator PenaltyTimeCounter()
    {
        yield return PenaltyTime_;
        OnStealPenalty = false;
        PenaltyTimeCoroutine = null;
        yield break;
    }

    PowerUpManager INgojeck.GetPowerFromClient()
    {
        if (KursiPenumpang.transform.childCount == 0) return null;

        if (KursiPenumpang.transform.childCount > 0)
        {
            return KursiPenumpang.transform.GetChild(0).GetComponent<PowerUpManager>();
        }
        return null;
    }

    void INgojeck.Fall()
    {
        photonView.RPC("RPC_PlayerFall", RpcTarget.All);
    }

    PowerUpManager INgojeck.GetPowerFromBox()
    {
        if (BoxPower == null) return null;

        return BoxPower;
    }

    void INgojeck.SetPowerFromBox(PowerUpManager box)
    {
        BoxPower = box;
    }
}
