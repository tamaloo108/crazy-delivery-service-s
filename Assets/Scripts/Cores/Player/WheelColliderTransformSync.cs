﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelColliderTransformSync : MonoBehaviour
{
    [SerializeField] Transform WheelParent;
    [SerializeField] bool M_isFrontWheel = true;

    WheelCollider myWheel;

    private void Start()
    {
        myWheel = GetComponent<WheelCollider>();
    }

    public bool isFrontWheels
    {
        get => M_isFrontWheel;
    }

    public WheelCollider Wheel
    {
        get => myWheel;
    }

    public void SyncTransform(Transform target)
    {
        if (target == null) return;
        Vector3 _pos = target.position;
        Quaternion _rot = target.rotation;
        myWheel.GetWorldPose(out _pos, out _rot);

        WheelParent.position = _pos;
        WheelParent.rotation = _rot;
    }

    public void SyncTransform()
    {
        if (WheelParent == null) return;
        Vector3 _pos = WheelParent.position;
        Quaternion _rot = WheelParent.rotation;
        myWheel.GetWorldPose(out _pos, out _rot);

        WheelParent.position = _pos;
        WheelParent.rotation = _rot;
    }

}
