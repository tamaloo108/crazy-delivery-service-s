﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using Photon.Pun;

public class VehicleController : MonoBehaviourPunCallbacks, IVehicleController
{
    [SerializeField] private float rotationSpeed = 5f;
    [SerializeField] private float VehicleGravity = 26f;
    [SerializeField] private float ThrustSpeed = .8f;
    [SerializeField] private float steerSpeed = 25f;
    [SerializeField] private float JumpPower = 25f;
    [SerializeField] private Vector3 GroundOffset;
    [SerializeField] LayerMask GroundLayer;
    [SerializeField] Transform FrontWheel;
    [SerializeField] Transform BackWheel;
    [SerializeField] Animator PlayerCharacterAnimator;
    [SerializeField] bool PseudoEightDirectionalMovement;

    Collider[] GroundCollider;
    Rigidbody myRb;
    Vector3 CenterOfMass;
    VehicleUser myUser;
    VehicleStates myStates;
    VehicleControlType ControlType;
    float MaxSteeringAngle;
    float MaxTorque;
    float CurrentRotate;
    float CurSteeringAngle;
    float CurTorque;
    float RotateVal = 0f;
    bool OnBrake = false;

    Vector3 _inputs;
    WaitForSeconds delay = new WaitForSeconds(2f);
    WaitForSeconds BrakeDelay = new WaitForSeconds(.5f);
    Coroutine BrakeCounter;

    ControllerHUDHandler HUDHandler;
    Transform VehicleNormal;
    GameObject VehicleMesh;
    Collider SphereWheelie;
    Rigidbody SphereRb;
    Animator myAnim;
    Camera mainCam;

    PowerUpManager mySkills;

    RaycastHit hitGround;
    private Vector3 velo;


    public void InitVehicleController(float _MaxSteeringAngle, float _MaxSpeed, VehicleUser Driver, VehicleControlType Controller, Vector3 CenterOfMass, GameObject VehicleMesh, Collider SphereWheelie, Transform VehicleNormal, Animator MyAnim, PowerUpManager mySkills)
    {
        mainCam = Camera.main;
        myAnim = MyAnim;
        PhotonNetwork.ConnectUsingSettings();
        MaxSteeringAngle = _MaxSteeringAngle;
        MaxTorque = _MaxSpeed;
        myUser = Driver;
        ControlType = Controller;
        myRb = GetComponent<Rigidbody>();
        this.mySkills = mySkills;
        if (!ControlType.Equals(VehicleControlType.Radical))
        {
            this.CenterOfMass = CenterOfMass;
            myRb.centerOfMass = this.CenterOfMass;
        }
        else
        {
            this.VehicleNormal = VehicleNormal;
            SphereRb = SphereWheelie.GetComponent<Rigidbody>();
            this.VehicleMesh = VehicleMesh;
            this.SphereWheelie = SphereWheelie;
        }

        myStates = VehicleStates.Normal;
        HUDHandler = GameObject.FindGameObjectWithTag("ControllerHUD").GetComponent<ControllerHUDHandler>();
    }

    public PowerUpManager GetMySkill()
    {
        return mySkills;
    }

    public ControllerHUDHandler GetControllerHUD()
    {
        return HUDHandler;
    }

    private IEnumerator ResetVelocityLerp()
    {
        float duration = 3f;
        while (duration > 0f)
        {
            var velocityDown = new Vector3(SphereRb.velocity.x, -2f, SphereRb.velocity.z);
            var k = Vector3.Lerp(SphereRb.velocity, velocityDown, Time.deltaTime);
            SphereRb.velocity = k;
            duration -= Time.deltaTime;
        }
        yield break;
    }

    //Vector3 SnapTo(Vector3 v3, float snapAngle)
    //{
    //    float angle = Vector3.Angle(v3, Vector3.up);
    //    if (angle < snapAngle / 2.0f)          // Cannot do cross product 
    //        return Vector3.up * v3.magnitude;  //   with angles 0 & 180
    //    if (angle > 180.0f - snapAngle / 2.0f)
    //        return Vector3.down * v3.magnitude;

    //    float t = Mathf.Round(angle / snapAngle);
    //    float deltaAngle = (t * snapAngle) - angle;

    //    Vector3 axis = Vector3.Cross(Vector3.up, v3);
    //    Quaternion q = Quaternion.AngleAxis(deltaAngle, axis);
    //    return q * v3;
    //}

    void IVehicleController.Accel(WheelCollider collider)
    {
        if (GetVertical() < 0f && !OnBrake)
        {
            if (BrakeCounter == null)
            {
                StartCoroutine(DoBraking(collider));
            }
        }
        collider.motorTorque = GetVertical() > 0f ? CurTorque : CurTorque / 3f;

        if (ControlType.Equals(VehicleControlType.Radical))
        {
            SphereRb.AddForce(VehicleMesh.transform.forward * CurTorque, ForceMode.Acceleration);
        }
    }

    void IVehicleController.Accel()
    {
        if (_inputs.x != Mathf.Abs(0) && _inputs.z != Mathf.Abs(0))
        {
            SphereRb.freezeRotation = false;
            SphereRb.AddForce(VehicleMesh.transform.forward * CurTorque, ForceMode.Acceleration);
        }
        else
        {
            RotateVal = 0f;
            CurTorque = 0f;
            SphereRb.freezeRotation = true;
        }
    }

    private IEnumerator DoBraking(WheelCollider collider)
    {
        OnBrake = true;
        collider.brakeTorque = MaxTorque * 2f;
        yield return BrakeDelay;
        collider.brakeTorque = 0f;
        yield return delay;
        OnBrake = false;
        BrakeCounter = null;
        yield break;
    }

    void IVehicleController.BalanceVehicle(bool active)
    {
        if (active)
        {
            var pos = myRb.transform.position;
            pos.z = 0f;
            myRb.transform.position = pos;
        }
    }

    void IVehicleController.ChangeWheelAngle(WheelCollider collider)
    {
        collider.steerAngle = CurSteeringAngle;
    }

    float ConvertRange(float Val, float OldMin, float newMin, float newMax, float oldMax)
    {
        return (((Val - OldMin) * newMax) / oldMax) + newMin;
    }

    void IVehicleController.DoSteering()
    {
        if (PseudoEightDirectionalMovement)
        {
            GetInput();
            //   if (Mathf.Abs(_inputs.x) > 1 || Mathf.Abs(_inputs.z) > 1)
            //   {

            if (myStates == VehicleStates.Mbah_Slowdown)
            {
                CurTorque = Mathf.Lerp(CurTorque, GetVertical() > 0f ? (MaxTorque * GetVertical()) : (MaxTorque / 5f * GetVertical()), Time.deltaTime * ThrustSpeed);
                CurTorque = CurTorque / 2f;
            }
            else
            {
                CurTorque = Mathf.Lerp(CurTorque, GetVertical() > 0f ? (MaxTorque * GetVertical()) : (MaxTorque / 5f * GetVertical()), Time.deltaTime * ThrustSpeed);
            }

            myAnim.SetFloat("Torque", Mathf.Lerp(myAnim.GetFloat("Torque"), ConvertRange(CurTorque, 0f, 0f, 1f, MaxTorque), Time.deltaTime));
        }
        else
        {

            if (Mathf.Abs(CurTorque) > 7f)
            {
                CurSteeringAngle = Mathf.Lerp(CurSteeringAngle, MaxSteeringAngle * GetHorizontal(), Time.fixedTime * steerSpeed);
            }
            CurTorque = Mathf.Lerp(CurTorque, GetVertical() > 0f ? (MaxTorque * GetVertical()) : (MaxTorque / 5f * GetVertical()), Time.deltaTime * ThrustSpeed);
            CurrentRotate = Mathf.Lerp(CurrentRotate, CurSteeringAngle, Time.fixedTime * rotationSpeed); CurSteeringAngle = 0f;
        }


        if (Mathf.Abs(CurTorque) > 7f)
        {
            myAnim.SetBool("Do Rotate", Mathf.Abs(GetHorizontal()) > 0.54f);
            myAnim.SetFloat("Magnitude", Mathf.Lerp(myAnim.GetFloat("Magnitude"), 1f, Time.deltaTime));
            PlayerCharacterAnimator.SetBool("DoRotate", myAnim.GetBool("Do Rotate"));
            myAnim.SetFloat("Rotate", Mathf.MoveTowards(myAnim.GetFloat("Rotate"), GetHorizontal(), Time.fixedTime * 14f));
            PlayerCharacterAnimator.SetFloat("Rotate", myAnim.GetFloat("Rotate"));
        }
        else
        {
            myAnim.SetFloat("Magnitude", Mathf.Lerp(myAnim.GetFloat("Magnitude"), 0f, Time.deltaTime));
            myAnim.SetBool("Do Rotate", false);
            PlayerCharacterAnimator.SetBool("DoRotate", false);
            PlayerCharacterAnimator.SetFloat("Rotate", 0f);
        }

        //Wheel Anim
        RotateVal = GetVertical() > 0f ? -(SphereRb.velocity.magnitude / 0.25f) : (SphereRb.velocity.magnitude / 0.25f);
        FrontWheel.localEulerAngles += new Vector3(0f, RotateVal, 0f);
        BackWheel.localEulerAngles += new Vector3(0f, RotateVal, 0f);

    }

    private void GetInput()
    {
        _inputs.x = GetHorizontal();
        _inputs.z = GetVertical();
    }

    public void SetVelocity(Vector3 v)
    {
        SphereRb.velocity = v;
    }

    private float GetHorizontal()
    {
        switch (myUser)
        {
            case VehicleUser.AI:
                return 0f;

            default:
                return OnGround() && photonView.IsMine && GetPositiveStateSelf(myStates) ? !PseudoEightDirectionalMovement ? CrossPlatformInputManager.GetAxis("Horizontal") : CrossPlatformInputManager.GetAxisRaw("Horizontal") : 0f;
        }
    }

    private bool OnJumpButtonDown()
    {
        return photonView.IsMine && CrossPlatformInputManager.GetButtonDown("Jump Button") && GetPositiveStateSelf(myStates);
    }

    private float GetVertical()
    {
        switch (myUser)
        {
            case VehicleUser.AI:
                return 0f;

            default:
                return OnGround() && photonView.IsMine ? !PseudoEightDirectionalMovement ? CrossPlatformInputManager.GetAxis("Vertical") : CrossPlatformInputManager.GetAxisRaw("Vertical") : 0f;
                //return OnGround() && photonView.IsMine && CrossPlatformInputManager.GetButton("Gas Button") ? 1f : OnGround() && photonView.IsMine && CrossPlatformInputManager.GetButton("Back Button") ? -1f : 0f ;
        }
    }

    private void DoCameraShake()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        perlin.SetCameraShake(20f, 20f, 0.03f);
    }

    private void InitCamOffset()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        var c_offsets = new Vector3(UnityEngine.Random.Range(-10f, 10f), UnityEngine.Random.Range(-10f, 10f), UnityEngine.Random.Range(-10f, 10f));
        perlin.InitNoiseValue(c_offsets);
    }

    void IVehicleController.SetWheelieGravity()
    {
        SphereRb.AddForce(Vector3.down * VehicleGravity, ForceMode.Acceleration);
    }

    void IVehicleController.SyncMesh()
    {
        if (PseudoEightDirectionalMovement)
        {

            //CurSteeringAngle = Mathf.Atan2(_inputs.x, _inputs.z < 1 ? Mathf.Abs(_inputs.z) : _inputs.z);
            //CurSteeringAngle = Mathf.Rad2Deg * CurSteeringAngle;
            //CurSteeringAngle += mainCam.transform.eulerAngles.y;
            //var targetRot = Quaternion.LookRotation(_inputs).eulerAngles;
            //CurrentRotate = Mathf.Round(targetRot.y / 45) * 45;
            //Debug.Log(CurrentRotate);
            //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(targetRot.x, CurrentRotate, targetRot.z), Time.deltaTime * 2.25f);

            //if (Mathf.Abs(CurTorque) < 7f) return;
            //CurSteeringAngle = Mathf.Atan2(_inputs.x, _inputs.z < 1 ? Mathf.Abs(_inputs.z) : _inputs.z);
            //CurSteeringAngle = Mathf.Round((Mathf.Rad2Deg * CurSteeringAngle) / 45f) * 45f;
            //CurSteeringAngle += transform.eulerAngles.y;
            //CurrentRotate = CurSteeringAngle;
            //Debug.Log(CurrentRotate);
            //transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(0f, CurrentRotate, 0f), Time.deltaTime * 2.25f);

            if (Mathf.Abs(CurTorque) < 7f) return;
            CurSteeringAngle = Mathf.Atan2(_inputs.x, _inputs.z < 1 ? Mathf.Abs(_inputs.z) : _inputs.z);
            CurSteeringAngle = (Mathf.Rad2Deg * CurSteeringAngle);
            CurSteeringAngle += mainCam.transform.eulerAngles.y;
            CurrentRotate = Mathf.Round(CurSteeringAngle / 45f) * 45f;
            Debug.Log(CurrentRotate);
            var targetRot = Quaternion.Euler(0f, CurrentRotate, 0f);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * 4f);

        }
        else
        {
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(0f, transform.eulerAngles.y + CurrentRotate, 0f), Time.deltaTime * 4f);
        }

        RaycastHit hitOn;
        Physics.Raycast(transform.position, Vector3.down, out hitOn, 2f);

        VehicleNormal.up = Vector3.Lerp(VehicleNormal.up, hitOn.normal, Time.deltaTime * 8f);
        VehicleNormal.Rotate(0, transform.eulerAngles.y, 0);
    }


    public void OnDrawGizmos()
    {
        if (VehicleMesh != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + GroundOffset, transform.localScale.x / 2f);
        }
    }

    void IVehicleController.DoJump()
    {
        if (OnGround())
        {
            if (OnJumpButtonDown())
            {
                SphereRb.AddForce(Vector3.up * JumpPower, ForceMode.VelocityChange);
            }
        }
    }

    private bool OnGround()
    {
        GroundCollider = Physics.OverlapSphere(transform.position + GroundOffset, transform.localScale.x / 2f, GroundLayer);
        if (GroundCollider.Length > 0f)
        {
            return true;
        }

        return false;
    }

    bool GetPositiveStateSelf(VehicleStates state)
    {
        return state == VehicleStates.Charge || state == VehicleStates.Normal || state == VehicleStates.Invulnerable;
    }

    bool IVehicleController.GetPickup()
    {
        return photonView.IsMine && CrossPlatformInputManager.GetButtonDown("Pick Button") && GetPositiveStateSelf(myStates);
    }

    bool IVehicleController.GetSpecialButton()
    {
        return photonView.IsMine && CrossPlatformInputManager.GetButtonDown("Special Button") && GetPositiveStateSelf(myStates);
    }

    void IVehicleController.FollowCollider(Vector3 offset)
    {
        transform.position = SphereWheelie.transform.position - offset;
    }

    void IVehicleController.SetState(VehicleStates state)
    {
        myStates = state;
    }

    VehicleStates IVehicleController.GetState()
    {
        return myStates;
    }

    void IVehicleController.ForceSetPosition(Vector3 position)
    {
        SphereRb.MovePosition(position);
    }

    void IVehicleController.ActivateBoostAnim(bool state)
    {
        PlayerCharacterAnimator.SetBool("DoBoost", state);
    }

    bool IVehicleController.GetBoxSpecialButton()
    {
        return photonView.IsMine && CrossPlatformInputManager.GetButtonDown("Box Special Button") && GetPositiveStateSelf(myStates);
    }

    bool IVehicleController.GetDriverSpecialButton()
    {
        return photonView.IsMine && CrossPlatformInputManager.GetButtonDown("Driver Special Button") && GetPositiveStateSelf(myStates);
    }

    void IVehicleController.ShakeCamera()
    {
        if (photonView.IsMine)
        {
            InitCamOffset();
            DoCameraShake();
        }
    }

    void IVehicleController.Push(int direction, float force)
    {
        var Dir = Vector3.zero;

        if (photonView.IsMine)
        {
            InitCamOffset();
            DoCameraShake();
        }

        switch (direction)
        {
            case UniversalHandler.KNOCK_FORWARD:
                Dir = transform.forward;
                break;

            case UniversalHandler.KNOCK_BACK:
                Dir = -transform.forward;
                break;

            case UniversalHandler.KNOCK_UP:
                Dir = transform.up;
                break;

            case UniversalHandler.KNOCK_DOWN:
                Dir = -transform.up;
                break;
        }


        SphereRb.AddForce(Dir * force, ForceMode.VelocityChange);
    }

    bool IVehicleController.OnMovementInputDetected()
    {
        return _inputs != Vector3.zero;
    }

}

public enum VehicleStates
{
    Normal = 0,
    Freeze = 1,
    KO = 2,
    Charge = 3,
    Invulnerable = 4,
    Mbah_Slowdown = 5
}