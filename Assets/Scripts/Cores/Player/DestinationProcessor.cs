﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationProcessor : MonoBehaviour
{
    bool CanDrop;
    PedestrianProcessor PProcessor;
    NgojekHandler Player;
    VehicleController Controller;
    private void Start()
    {
        PProcessor = new PedestrianProcessor();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            Player = other.GetComponent<NgojekHandler>();
            Controller = other.GetComponent<VehicleController>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            CanDrop = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Player = null;
        Controller = null;
        CanDrop = false;
    }


}
