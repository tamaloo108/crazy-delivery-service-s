﻿public interface IPlayerHandler
{
    void SetPlayerVehicle(Photon.Pun.PhotonView view);
    void SetCurrentVehicleOwnership();
    void SetCurrentVehicleOwnership(Photon.Realtime.Player player);
    void SetCurrentVehicleOwnership(int id);
    void ResetDisconnectCoroutine();
}