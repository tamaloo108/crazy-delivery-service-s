﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UniversalSoundHandler : Singleton<UniversalSoundHandler>
{
    public List<AudioFiles> Sounds;
    bool Initialized = false;

    public UnityEvent<string> MenuPlayBGM, MenuPlaySFX1, MenuPlaySFX2, MenuPlayVA;
    public UnityEvent<float> BGMChangePitch;

    public void InitSounds(AudioData d)
    {
        Sounds = new List<AudioFiles>();
        foreach (AudioFiles files in d.SoundList)
        {
            Sounds.Add(files);
        }

        Initialized = true;
    }


    public void AssignEvent(UnityAction<string> BGM, UnityAction<string> SFX1, UnityAction<string> SFX2, UnityAction<string> VA, UnityAction<float> PitchBGM)
    {
        MenuPlayBGM = new UnityEvent<string>();
        MenuPlaySFX1 = new UnityEvent<string>();
        MenuPlaySFX2 = new UnityEvent<string>();
        MenuPlayVA = new UnityEvent<string>();
        BGMChangePitch = new UnityEvent<float>();

        MenuPlayBGM.AddListener(BGM);
        MenuPlaySFX1.AddListener(SFX1);
        MenuPlaySFX2.AddListener(SFX2);
        MenuPlayVA.AddListener(VA);
        BGMChangePitch.AddListener(PitchBGM);
    }

    public void RemoveEvent()
    {
        MenuPlayBGM.RemoveAllListeners();
        MenuPlaySFX1.RemoveAllListeners();
        MenuPlaySFX2.RemoveAllListeners();
        MenuPlayVA.RemoveAllListeners();
        BGMChangePitch.RemoveAllListeners();
    }

    public void PlaySounds(string name, AudioSource sources)
    {
        var g = Sounds.Find(x => x.Name.Equals(name));
        sources.clip = g.Clip;
        sources.pitch = g.Pitch;
        sources.loop = g.Loop;
        sources.priority = g.Priority;
        sources.spatialBlend = g.Blend;
        sources.volume = g.Volume;

        sources.Play();
    }


    public void StopSounds(AudioSource sources)
    {
        sources.Stop();
    }

    public void PauseSounds(AudioSource sources)
    {
        sources.Pause();
    }

    public void ResumeSounds(AudioSource sources)
    {
        sources.UnPause();
    }
}

[System.Serializable]
public class AudioFiles
{
    public string Name;
    public AudioClip Clip;
    public bool PlayOnAwake;
    public bool Loop;
    [Range(0f, 1f)] public float Volume = 0.5f;
    [Range(0f, 2f)] public float Pitch = 1f;
    [Range(0, 255)] public int Priority = 128;
    [Range(0f, 1f)] public float Blend = 0f;
}
