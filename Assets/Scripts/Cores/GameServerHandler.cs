﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine.AddressableAssets;
using ExitGames.Client.Photon;

public class GameServerHandler : MonoBehaviourPunCallbacks
{
    [SerializeField] private bool DisconnectConsent = false;
    [SerializeField] private AssetReference DisconnectUI;
    bool IsDisconnected;

    float timeLeft = 0.0018f;
    private GameObject DisconnectInstance;
    PlayerHandler pHandler;
    LevelManager Manager;
    WaitUntil InRoom;
    WaitUntil UntilOnline;
    WaitForEndOfFrame delay;
    Coroutine SocketTimeStampCoroutine;

    private void Awake()
    {
        delay = new WaitForEndOfFrame();
        UntilOnline = new WaitUntil(() => PhotonNetwork.NetworkClientState == ClientState.Disconnecting);
        InRoom = new WaitUntil(() => PhotonNetwork.IsConnected && PhotonNetwork.InRoom);
        Manager = GetComponent<LevelManager>();
    }

    private void Start()
    {
        pHandler = GetComponent<PlayerHandler>();
        this.enabled = false;
    }

    void PlayerSetOwnership(IPlayerHandler handler)
    {
        handler.SetCurrentVehicleOwnership();
    }

    private void OnDoneSpawn(GameObject obj)
    {
        DisconnectInstance = obj;
        DisconnectInstance.GetComponent<DisconnectUIHelper>().GetRetryButton().onClick.AddListener(() => DoReconnect());
        DisconnectInstance.GetComponent<DisconnectUIHelper>().GetQuitButton().onClick.AddListener(() => QuitGame());
    }

    private void QuitGame()
    {
        Application.Quit();
    }

    private void DoReconnect()
    {
        //  pHandler.DoReconnect();
        RecoverConnection();
    }

    void ResetOnDisconnect(IPlayerHandler handler)
    {
        handler.ResetDisconnectCoroutine();
    }

    private void RecoverConnection()
    {
        if (!PhotonNetwork.ReconnectAndRejoin())
        {
            Debug.Log("fail to reconnect and rejoin");
            if (!PhotonNetwork.NetworkingClient.ReconnectToMaster())
            {
                Debug.Log("fail to reconnect to master");
                if (!PhotonNetwork.ConnectUsingSettings())
                {
                    Debug.Log("fail to reconnect");
                }
            }
        }


    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();


        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.SendAllOutgoingCommands();

            if (DisconnectInstance != null)
            {
                DisconnectInstance.SetActive(false);
            }
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        IsDisconnected = true;
        if (CanRecoverFromDisconnect(cause))
        {
            if (!DisconnectConsent)
            {
                if (DisconnectInstance == null)
                {
                    UniversalAddresableLoader.Instance.Spawn(DisconnectUI, Vector3.zero, null, OnDoneSpawn);
                }
                else
                {
                    DisconnectInstance.SetActive(true);
                }
            }
        }
    }

    private bool CanRecoverFromDisconnect(DisconnectCause cause)
    {
        switch (cause)
        {
            case DisconnectCause.Exception:
            case DisconnectCause.ServerTimeout:
            case DisconnectCause.ClientTimeout:
            case DisconnectCause.DisconnectByClientLogic:
            case DisconnectCause.DisconnectByServerReasonUnknown:
                return true;
        }
        return false;
    }

}


