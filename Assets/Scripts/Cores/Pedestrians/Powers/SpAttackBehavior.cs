﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CDS.Skill;
using Photon.Pun;

public class SpAttackBehavior : PersistentSkillBehavior
{
    [SerializeField] ParticleSystem Particles;
    [SerializeField] Animator EffectAnim;


    bool DetectTarget;
    float Duration;
    float initDuration_;
    Coroutine PowerDuration;
    WaitUntil initDuration;
    WaitForFixedUpdate PhysicsDelay;
    StateProcessor SP;
    PhotonView pView;
    VehicleController myController;

    private void Start()
    {
        SP = new StateProcessor();
        PhysicsDelay = new WaitForFixedUpdate();
        initDuration = new WaitUntil(() => Duration > 0f);
        if (PowerDuration == null)
        {
            PowerDuration = StartCoroutine(DoPowerCounter());
        }
        Particles.Simulate(0f, true, true);
    }

    public override void SetDuration(float v)
    {
        initDuration_ = v;
        Duration = v;
    }

    public override void Fire()
    {
        Duration = initDuration_;
        if (PowerDuration == null)
        {
            PowerDuration = StartCoroutine(DoPowerCounter());
        }
    }

    public override void SetView(PhotonView view)
    {
        pView = view;
        myController = pView.GetComponent<VehicleController>();
    }

    public void Slowdown(GameObject g)
    {
        if (pView.IsMine)
        {
            //InitShakeCamera(myController);
            var controller = g.GetComponent<VehicleController>();
            var tView = controller.GetComponent<PhotonView>();

            UniversalHandler.Instance.VFX.CallParticles("Impact_General", g.transform.position);

            if (SP.GetStates(controller) == VehicleStates.Invulnerable) return;

            //StartCoroutine(SP.SetSlowedEffect(controller));
            pView.RPC("OnActivateAbility", RpcTarget.All, UniversalHandler.MBAH_ABILITY_1);
            tView.RPC("PlaySoundKnocked", RpcTarget.All);
            tView.RPC("OnReceiveHit", RpcTarget.All, UniversalHandler.MBAH_ABILITY_1);
        }
    }

    void InitShakeCamera(IVehicleController controller)
    {
        controller.ShakeCamera();
    }

    private IEnumerator DoPowerCounter()
    {
        yield return initDuration;
        EffectAnim.SetBool("Startwave", true);
        while (Duration > 0f)
        {
            Particles.Play();
            if (DetectTarget)
            {
                Debug.Log("Demeg");
                PowerDuration = null;
                break;
            }
            yield return PhysicsDelay;
            Duration -= Time.fixedDeltaTime;
        }
        EffectAnim.SetBool("Startwave", false);
        Debug.Log("pog.");
        PowerDuration = null;
        yield break;
    }

}
