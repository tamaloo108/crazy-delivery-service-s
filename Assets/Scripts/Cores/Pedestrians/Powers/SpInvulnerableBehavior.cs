﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CDS.Skill;
using Photon.Pun;
using System;

public class SpInvulnerableBehavior : PersistentSkillBehavior
{
    [SerializeField] ParticleSystem Particles;

    float Duration;
    float initDuration_;
    Coroutine PowerDuration;
    WaitUntil initDuration;
    WaitForFixedUpdate PhysicsDelay;
    StateProcessor SP;
    PhotonView pView;
    VehicleController myController;

    public override void Fire()
    {
        Duration = initDuration_;
        if (PowerDuration == null)
        {
            PowerDuration = StartCoroutine(PowerCounter());
        }
    }

    private void Start()
    {
        SP = new StateProcessor();
        PhysicsDelay = new WaitForFixedUpdate();
        initDuration = new WaitUntil(() => Duration > 0f);
        Particles.Simulate(0f, true, true);
        if (PowerDuration == null)
        {
            PowerDuration = StartCoroutine(PowerCounter());
        }
    }

    private IEnumerator PowerCounter()
    {
        yield return initDuration;
        yield return new WaitUntil(() => pView != null);
        pView.RPC("RPC_SetOnState", RpcTarget.All, UniversalHandler.PLAYER_INVULNERABLE_ON);
        Particles.Play();
        while (Duration > 0f)
        {
            yield return PhysicsDelay;
            Duration -= Time.fixedDeltaTime;
        }
        yield return PhysicsDelay;
        pView.RPC("RPC_SetOnState", RpcTarget.All, UniversalHandler.PLAYER_INVULNERABLE_OFF);
        Particles.Stop();
        PowerDuration = null;
        yield break;
    }

    public override void SetDuration(float v)
    {
        Duration = v;
        initDuration_ = v;
    }

    public override void SetView(PhotonView view)
    {
        pView = view;
    }
}
