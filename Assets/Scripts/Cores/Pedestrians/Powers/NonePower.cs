﻿using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon;

namespace CDS.Power
{
    public class NonePower : PowerBase
    {
        public override PowersEnum MyPower => PowersEnum.None;

        public override IEnumerator CastPowerCoroutine(Transform owner, int usage, int maxUsage)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn)
        {
            throw new System.NotImplementedException();
        }

        public override void RaisePowerEvent(EventData photonEvent)
        {
            throw new System.NotImplementedException();
        }
    }
}

