﻿using UnityEngine;
using System;
using System.Collections;
using ExitGames.Client.Photon;

namespace CDS.Power
{
    public abstract class PowerBase
    {
        public abstract PowersEnum MyPower { get; }
        public abstract void RaisePowerEvent(EventData photonEvent);
        public abstract IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage);
        public abstract IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown);
        public abstract IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn);
        public abstract IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn);
    }
}

