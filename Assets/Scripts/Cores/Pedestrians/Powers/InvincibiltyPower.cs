﻿using UnityEngine;
using Photon.Pun;
using System.Collections;
using UnityEngine.AddressableAssets;
using System;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System.Threading.Tasks;

namespace CDS.Power
{
    public class InvincibiltyPower : PowerBase
    {
        public override PowersEnum MyPower => PowersEnum.Invincibilty;
        bool onCast = false;
        bool isDone = false;
        bool AbilitiesDepleted = false;
        bool isInstantiating = false;
        int MaxUsage = 0;
        int CurUsage = 0;
        private float SkillDuration;
        float Cooldown = 0f;
        GameObject objDisplay;

        WaitForEndOfFrame RunDelay = new WaitForEndOfFrame();

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn)
        {
            this.MaxUsage = MaxUsage;
            this.CurUsage = CurUsage;
            Cooldown = SkillCooldown;

            AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

            if (AbilitiesDepleted || onCast)
            {
                PhotonView _owner = owner.GetComponent<PhotonView>();
                _owner.RPC("DoneAbilties", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown)
        {
            this.MaxUsage = MaxUsage;
            this.CurUsage = CurUsage;
            Cooldown = SkillCooldown;

            AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

            if (AbilitiesDepleted || onCast)
            {
                PhotonView _owner = owner.GetComponent<PhotonView>();
                _owner.RPC("DoneAbilties", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn)
        {
            this.MaxUsage = MaxUsage;
            this.CurUsage = CurUsage;
            this.SkillDuration = SkillDuration;
            Cooldown = SkillCooldown;

            AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

            if (AbilitiesDepleted || onCast)
            {
                PhotonView _owner = owner.GetComponent<PhotonView>();
                _owner.RPC("DoneAbilties", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage)
        {
            this.MaxUsage = MaxUsage;
            this.CurUsage = CurUsage;

            AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

            if (AbilitiesDepleted || onCast)
            {
                PhotonView _owner = owner.GetComponent<PhotonView>();
                _owner.RPC("DoneAbilties", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }


        IEnumerator EventCaller(Transform owner)
        {
            PhotonView view = owner.GetComponent<PhotonView>();
            PhotonNetwork.NetworkingClient.EventReceived += RaisePowerEvent;
            yield return RaiseEventSetup(view);
            yield break;
        }

        IEnumerator RaiseEventSetup(PhotonView view)
        {
            object[] content = new object[] { view.ViewID, new Vector3(0f, 0f, 4.3f), true, CurUsage, MaxUsage };
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(UniversalHandler.SPAWN_ABILITY_MESH, content, eventOptions, SendOptions.SendReliable);
            yield break;
        }

        async Task<Boolean> PowerDuration(float dur)
        {
            WaitForSeconds wait = new WaitForSeconds(dur);

            await wait;

            return true;
        }

        async Task PowerSpawner(Transform owner, object[] data)
        {
            WaitForEndOfFrame wait = new WaitForEndOfFrame();

            if (objDisplay == null && !isInstantiating)
            {
                isInstantiating = true;
                Addressables.InstantiateAsync("InvincibilityBall").Completed += InvincibiltyPower_Completed;
            }

            while (true)
            {
                if (objDisplay != null)
                {
                    objDisplay.SetActive(true);
                    objDisplay.GetComponent<Invincibilty>().SetUser(owner.parent.GetChild(0));
                    objDisplay.GetComponent<Invincibilty>().SetChargeDuration(SkillDuration);
                    objDisplay.transform.SetParent(owner.parent.GetChild(0));
                    objDisplay.transform.localPosition = (Vector3)data[1];
                    //objDisplay.transform.localPosition = Vector3.zero;
                    objDisplay.transform.localRotation = Quaternion.LookRotation(Vector3.zero);
                    break;
                }
                await wait;
            }

            isInstantiating = false;
            PhotonNetwork.NetworkingClient.EventReceived -= RaisePowerEvent;
            return;
        }

        private void InvincibiltyPower_Completed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
        {
            objDisplay = obj.Result;
        }

        public override async void RaisePowerEvent(EventData photonEvent)
        {
            if (photonEvent.Code == UniversalHandler.SPAWN_ABILITY_MESH)
            {

                object[] data = (object[])photonEvent.CustomData;

                var _owner = PhotonView.Find((int)data[0]);
                var owner = _owner.transform;
                onCast = (bool)data[2];
                owner.GetComponent<VehicleController>().ActivateBoostAnim(true);
                var isDoneTask = PowerDuration(Cooldown);
                var EffectSpawnTask = PowerSpawner(owner, data);
                
                await Task.WhenAll(isDoneTask, EffectSpawnTask);
                await EffectSpawnTask;
                isDone = await isDoneTask;
                if (isDone)
                {
                    isDone = false;
                    onCast = false;

                    owner.GetComponent<VehicleController>().ActivateBoostAnim(false);
                    //UnityEngine.Object.Destroy(objDisplay);
                    _owner.RPC("DoneAbilties", RpcTarget.All);
                }
            }
        }

    }
}

