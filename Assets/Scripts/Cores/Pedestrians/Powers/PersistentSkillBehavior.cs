﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CDS.Skill
{
    public abstract class PersistentSkillBehavior : MonoBehaviour
    {
        public abstract void SetDuration(float v);
        public abstract void Fire();
        public abstract void SetView(Photon.Pun.PhotonView view);
    }
}