﻿using Cinemachine;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Photon.Pun;
using CDS.Skill;

public class SpBoostBehavior : PersistentSkillBehavior
{
    [SerializeField] AssetReference ZoomEffect;
    [SerializeField] Vector3 ZoomDistance = new Vector3(0f, 0f, 4f);
    [SerializeField] ParticleSystem Particles;

    Transform User;
    PhotonView UserPView;
    float ChargeDuration;
    float initDuration_;
    GameObject Zoomies;
    Coroutine Charges = null;
    private CinemachineStateDrivenCamera CinemachineStateCam;
    private CinemachineVirtualCamera PlayerCam;
    private WaitUntil ChargeDurationAdded;
    private WaitUntil UserAvailable;
    private Vector3 DesiredCameraDistance;
    Vector3 CurrentCameraDistance;
    Vector3 UserForward;

    WaitForFixedUpdate delay;
    PhotonView pView;
    CombatAudioHelper SoundHelper;

    void Awake()
    {
        Particles.Simulate(0f, true, true);
        //Particles.Play();
        delay = new WaitForFixedUpdate();
        CinemachineStateCam = GameObject.FindGameObjectWithTag("CinemachineStateDriven").GetComponent<Cinemachine.CinemachineStateDrivenCamera>();
        PlayerCam = CinemachineStateCam.ChildCameras[0].GetComponent<CinemachineVirtualCamera>();
        ChargeDurationAdded = new WaitUntil(() => ChargeDuration > 0f);
        UserAvailable = new WaitUntil(() => UserAvailable != null);
        DesiredCameraDistance = new Vector3(0f, 0f, -3f);
    }

    private void Start()
    {
        SoundHelper = GetComponent<CombatAudioHelper>();

        if (Charges == null)
        {
            Charges = StartCoroutine(DoCharge());
        }
    }

    private void Push(GameObject gameObject, Vector3 direction, float force)
    {
        var rb = gameObject.GetComponent<Rigidbody>();
        if (rb == null) return;
        rb.isKinematic = false;
        // rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rb.AddForce(direction * force, ForceMode.VelocityChange);
    }

    public override void SetView(PhotonView view)
    {
        pView = view;
    }

    private Vector3 GetUserForward()
    {
        return User.parent.GetChild(0).transform.forward;
    }

    private IEnumerator DoCharge()
    {
        Particles.Play(true);
        yield return ChargeDurationAdded;
        yield return UserAvailable;
        CurrentCameraDistance = UniversalHandler.Instance.DefaultCameraHandler;
        var rb = User.parent.GetChild(1).gameObject.GetComponent<Rigidbody>();

        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        SoundHelper.PlayAudio("SFX_NitroStart_1");
        if (User.GetComponent<PhotonView>().IsMine)
        {
            UniversalAddresableLoader.Instance.Spawn(ZoomEffect, Vector3.zero, null, OnDoneSpawnZoomEffect);
            if (CurrentCameraDistance != DesiredCameraDistance)
                PlayerCam.GetComponent<CinemachineCameraOffset>().m_Offset += ZoomDistance;
            UserPView.RPC("RPC_SetOnState", RpcTarget.All, UniversalHandler.PLAYER_CHARGE_ON);
        }
        SoundHelper.PlayAudio("SFX_Nitro_1");
        while (ChargeDuration > 0f)
        {
            yield return UserAvailable;
            UserForward = GetUserForward();

            Push(User.parent.GetChild(1).gameObject, UserForward, 4.15f);
            //Push(User.parent.GetChild(1).gameObject, UserForward, 0f);
            yield return delay;
            ChargeDuration -= Time.fixedDeltaTime;
            Debug.Log("charge dur" + ChargeDuration);
        }

        SoundHelper.StopAudio();
        Particles.Stop();
        Charges = null;
        rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
        UserPView.RPC("RPC_SetOnState", RpcTarget.All, UniversalHandler.PLAYER_CHARGE_OFF);
        Destroy(Zoomies);
        ResetCamera();
        Zoomies = null;
        Destroy(gameObject);
        yield break;
    }

    void ResetCamera()
    {
        Charges = null;

        if (User.GetComponent<PhotonView>().IsMine)
        {
            PlayerCam.GetComponent<CinemachineCameraOffset>().m_Offset = CurrentCameraDistance;
        }

        if (Zoomies == null) return;
        Destroy(Zoomies);
    }

    private void OnDoneSpawnZoomEffect(GameObject obj)
    {
        Zoomies = obj;
    }

    public void SetUser(Transform user)
    {
        User = user;
        UserPView = user.GetComponent<PhotonView>();
    }

    public void SetChargeDuration(float v)
    {
        ChargeDuration = v;
    }

    public override void SetDuration(float v)
    {
        initDuration_ = v;
        ChargeDuration = v;
    }

    public override void Fire()
    {
        ChargeDuration = initDuration_;
        if (Charges == null)
        {
            Charges = StartCoroutine(DoCharge());
        }
    }

}
