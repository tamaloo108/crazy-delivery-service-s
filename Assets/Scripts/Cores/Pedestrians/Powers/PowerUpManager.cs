﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using Photon.Pun;
using CDS.Skill;

public class PowerUpManager : MonoBehaviour
{
    [SerializeField] bool isBox;
    [SerializeField] bool PlayerSpecial;
    [SerializeField] bool Randomize;
    [SerializeField] PowersEnum MyPower = PowersEnum.None;
    [SerializeField] int MaxUsage = 1;
    [SerializeField] float Downtime = 1f;
    [SerializeField] float Duration = 1f;

    int CurUsage = 0;
    float DelayEachActivate = .3f;

    public PersistentSkillBehavior CurrentActiveSkill { get; private set; }

    private void Start()
    {
        if (Randomize)
        {
            RandomizePower();
        }
    }

    public void VisibilityState(bool Visible)
    {
        var hi = GetComponentsInChildren<Renderer>();
        var col = GetComponentsInChildren<Collider>();

        for (int i = 0; i < hi.Length; i++)
        {
            hi[i].enabled = Visible;
        }

        for (int i = 0; i < col.Length; i++)
        {
            col[i].enabled = Visible;
        }
    }

    public void SetCurrentActiveSkill(PersistentSkillBehavior skill)
    {
        CurrentActiveSkill = skill;
    }

    public float GetDowntime()
    {
        return Downtime;
    }

    public float GetDuration()
    {
        return Duration;
    }

    public bool IsItBox()
    {
        return isBox;
    }

    public bool IsItPlayer()
    {
        return PlayerSpecial;
    }

    private void RandomizePower()
    {
        var num = UnityEngine.Random.Range(1, 4);
        MyPower = (PowersEnum)num;
    }

    public PowersEnum GetPower()
    {
        return MyPower;
    }

    public int GetPowerInt()
    {
        return (int)MyPower;
    }

    public int GetMaxUsage()
    {
        return MaxUsage;
    }

    public float GetDelayActivation()
    {
        return DelayEachActivate;
    }

    public void SetUsage(int i)
    {
        CurUsage = i;
    }

    public void SetUsage()
    {
        CurUsage++;
    }

    public int GetCurrentUsage()
    {
        return CurUsage;
    }

    public void ResetUsage()
    {
        CurUsage = 0;
    }

    void SetBoxToUser(INgojeck owner, PowerUpManager box)
    {
        owner.SetPowerFromBox(box);
    }

    #region RPC

    [PunRPC]
    void RPC_HideBox(bool state)
    {
        VisibilityState(state);
    }

    [PunRPC]
    void RPC_SetBoxPow(int id)
    {
        PhotonView owner = PhotonNetwork.GetPhotonView(id);
        var OwnerOjeck = owner.GetComponent<NgojekHandler>();
        SetBoxToUser(OwnerOjeck, this);
    }

    #endregion
}

public enum PowersEnum
{
    None = 0,
    Invincibilty = 1, //charge forward trample anything it comes contact with.
    FreezeBall = 2, //shoot ball-like bullet straight.
    LandMine = 3,
    Boom = 4,
    PowerSp_Attack = 5,
    PowerSp_Protection = 6, //negates any harm.
    PowerSp_Boost = 7, //boost like invincibility.
}

