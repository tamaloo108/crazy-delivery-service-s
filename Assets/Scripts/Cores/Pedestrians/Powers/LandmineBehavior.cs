﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class LandmineBehavior : MonoBehaviourPunCallbacks
{
    bool Exploded = false;
    bool BombActive = false;
    float LifeDuration;
    Transform User = null;
    Coroutine LifeDurCoroutine;
    Coroutine ExplosionCoroutine;
    WaitForSeconds SecondDelay;
    WaitForSeconds BombDelay;
    WaitUntil LifeDurationAdded;

    CombatAudioHelper SoundHelper;
    Coroutine FrozenProjectile;
    StateProcessor States;
    private Vector3 WorldPosition;
    private Vector3 LocalPosition;
    private LayerMask hittableMask;

    private void Start()
    {
        SoundHelper = GetComponent<CombatAudioHelper>();
        hittableMask = LayerMask.NameToLayer("Player");
        BombDelay = new WaitForSeconds(0.65f);
        LifeDurationAdded = new WaitUntil(() => LifeDuration > 0f);
        States = new StateProcessor();
        SecondDelay = new WaitForSeconds(1f);
        if (LifeDurCoroutine == null)
        {
            LifeDurCoroutine = StartCoroutine(LifeCountdown());
            StartCoroutine(BombCounter());
        }
    }

    private void DoCameraShake()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        perlin.SetCameraShake(20f, 20f, 0.3f);
    }

    private void InitCamOffset()
    {
        var perlin = UniversalHandler.Instance.GetDrivenCamera().GetLiveCamera().GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        var c_offsets = new Vector3(UnityEngine.Random.Range(-15f, 15f), UnityEngine.Random.Range(-15f, 15f), UnityEngine.Random.Range(-15f, 15f));
        perlin.InitNoiseValue(c_offsets);
    }

    private IEnumerator BombCounter()
    {
        BombActive = false;
        yield return BombDelay;
        BombActive = true;
        yield break;
    }

    private IEnumerator LifeCountdown()
    {
        yield return LifeDurationAdded;
        SoundHelper.PlayAudio("SFX_TrapDrop_1");
        yield return SecondDelay;

        while (LifeDuration > 0f)
        {
            yield return SecondDelay;
            LifeDuration -= 1f;
        }
        yield return SecondDelay;
        LifeDurCoroutine = null;
        Destroy(gameObject);
        yield break;
    }

    internal void SetUser(Transform transform)
    {
        this.User = transform;
    }

    internal void SetLifeDuration(float v)
    {
        LifeDuration = v;
    }

    private IEnumerator Explosion(Transform _transform)
    {
        UniversalHandler.Instance.VFX.CallParticles("Explosion", transform.position);
        var Forward = _transform.forward;
        var Up = _transform.up;
        var spheres = _transform.parent.GetChild(1).transform;
        var pView = _transform.GetComponent<PhotonView>();
        if (pView.IsMine)
        {
            pView.RPC("PlaySoundKnocked", RpcTarget.All);
            InitCamOffset();
            DoCameraShake();
        }

        spheres.GetComponent<Rigidbody>().AddForce(-Forward * 50f, ForceMode.VelocityChange);
        spheres.GetComponent<Rigidbody>().AddForce(Up * 50f, ForceMode.VelocityChange);
        Debug.Log("explosion!");
        yield return SecondDelay;
        var Handler = _transform.parent.transform.GetChild(0).gameObject.GetComponent<VehicleController>();
        _transform.GetComponent<PhotonView>().RPC("RPC_CatchLandmine", RpcTarget.All, photonView.ViewID);
        ExplosionCoroutine = null;
        yield break;
    }

    private void OnCollisionStay(Collision collision)
    {
        //Debug.Log(collision.transform.name);
        //try
        //{
        //    if (!BombActive) return;

        //    if (collision.transform.parent.GetChild(0).GetComponent<PhotonView>() != null)
        //    {

        //        collision.transform.parent.GetChild(0).GetComponent<PhotonView>().RPC("RPC_CatchLandmine", RpcTarget.All, photonView.ViewID);

        //    }
        //    //if (parent.transform.GetChild(0) == null) return;


        //    if (!collision.transform.tag.Equals("Player Collider")) return;
        //    // if (parent.transform.GetChild(0) != User && collision.transform != User)
        //    //  {

        //    //gameObject.GetComponent<MeshRenderer>().enabled = false;
        //    //UniversalHandler.Instance.GetDeleter().AssignObject(this.gameObject);
        //    //UniversalHandler.Instance.GetDeleter().GetComponent<PhotonView>().RPC("RPC_Deleter", RpcTarget.All, 2f);
        //    //photonView.RPC("RPC_Destroy", RpcTarget.All, 2f);
        //    Debug.Log("destroyed");

        //    //if (ExplosionCoroutine == null && !Exploded)
        //    //{
        //    //    Exploded = true;
        //    //    var Handler = parent.transform.GetChild(0).gameObject.GetComponent<VehicleController>();
        //    //    parent.transform.GetChild(0).GetComponent<NgojekHandler>().ReleasePedestrian();
        //    //    User.GetComponent<Photon.Pun.PhotonView>().Owner.AddScore(4);
        //    //    ExplosionCoroutine = StartCoroutine(Explosion(parent.GetChild(1)));
        //    //}
        //    // }
        //}
        //catch (Exception)
        //{


        //}
    }

    void GetPoints(Transform target)
    {
        Ray raycast = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(raycast, out hit, 1000f, hittableMask))
        {
            //if pointing at the object you want to get collision point for
            if (hit.transform.gameObject == target.gameObject)
            {
                //have locPos = the local position of the hit point
                WorldPosition = hit.point;
                LocalPosition = transform.InverseTransformPoint(hit.point);

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            GetPoints(other.transform);
            UniversalHandler.Instance.VFX.DrawVFX(other.transform.position, "Impact_General");
        }
    }

    private void OnTriggerStay(Collider other)
    {

        if (!BombActive) return;

        try
        {
            if (other.transform.GetComponent<PhotonView>() == null) return;

            var parent = other.transform.parent;

            if (parent.transform.GetChild(0) == null) return;

            gameObject.GetComponent<MeshRenderer>().enabled = false;

            if (ExplosionCoroutine == null && !Exploded)
            {
                SoundHelper.PlayAudio("SFX_LandmineExplode_1");
                Exploded = true;
                var Handler = parent.transform.GetChild(0).gameObject.GetComponent<VehicleController>();

                if (States.GetStates(Handler) == VehicleStates.Invulnerable)
                {
                    UniversalHandler.Instance.VFX.CallParticles("Explosion", transform.position);                    
                    Handler.GetComponent<PhotonView>().RPC("RPC_CatchLandmine", RpcTarget.All, photonView.ViewID);
                    return;
                }
                parent.transform.GetChild(0).GetComponent<NgojekHandler>().ReleasePedestrian();

                if (parent.transform.GetChild(0) != User && other.transform != User)
                {
                    User.GetComponent<Photon.Pun.PhotonView>().Owner.AddScore(4);
                }

                ExplosionCoroutine = StartCoroutine(Explosion(other.transform));
            }

        }
        catch (Exception)
        {


        }

    }

    private void OnCollisionExit(Collision collision)
    {
        //if (!BombActive) return;
        //if (Exploded)
        //    Destroy(gameObject, .3f);
    }

    private void OnTriggerExit(Collider other)
    {
        //if (!BombActive) return;
        //if (Exploded)
        //    Destroy(gameObject, .3f);
    }

    private IEnumerator OnDoneExecute()
    {
        Destroy(gameObject, .3f);
        yield break;
    }

    [PunRPC]
    void RPC_Destroy(float v)
    {
        Debug.Log("destroy la");
        PhotonNetwork.Destroy(gameObject);
    }


}
