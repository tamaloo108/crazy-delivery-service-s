﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace CDS.Power
{
    public class SpInvulnerablePower : PowerBase
    {
        public override PowersEnum MyPower => PowersEnum.PowerSp_Protection;
        bool onCast = false;
        bool isDone = false;
        bool Assigned = false;
        bool AbilitiesGoesCooldown = false;
        int MaxUsage = 0;
        int CurUsage = 0;
        float SkillCooldown = 0f;
        float Duration = 0f;
        WaitForEndOfFrame RunDelay = new WaitForEndOfFrame();
        PhotonView m_Owner;
        Transform T_Owner;
        GameObject objDisplay;

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn)
        {
            this.CurUsage = CurUsage;
            this.MaxUsage = MaxUsage;
            this.SkillCooldown = SkillCooldown;
            Duration = SkillDuration;
            T_Owner = owner;
            m_Owner = owner.GetComponent<PhotonView>();

            AbilitiesGoesCooldown = this.CurUsage >= this.MaxUsage;

            if (AbilitiesGoesCooldown || onCast || AlreadySpawn)
            {
                m_Owner.RPC("CooldownAbilities", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        private IEnumerator EventCaller(Transform owner)
        {
            PhotonView view = owner.GetComponent<PhotonView>();
            PhotonNetwork.NetworkingClient.EventReceived += RaisePowerEvent;
            yield return RaiseEventSetup(view);
            yield break;
        }

        private IEnumerator RaiseEventSetup(PhotonView view)
        {
            object[] content = new object[] { view.ViewID, Vector3.zero, true, CurUsage, MaxUsage };
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(UniversalHandler.SPAWN_ABILITY_MESH, content, eventOptions, SendOptions.SendReliable);
            yield break;
        }

        async Task<Boolean> PowerDuration(float skillCooldown)
        {
            WaitForSeconds wait = new WaitForSeconds(skillCooldown);

            await wait;

            return true;
        }

        async Task PowerSpawner(Transform owner, object[] data)
        {
            WaitForSeconds wait = new WaitForSeconds(0.01f);
            WaitUntil Objspawn = new WaitUntil(() => objDisplay != null);

            if (objDisplay == null)
            {
                Addressables.InstantiateAsync("SpInvulnerableEffect").Completed += SpInvulnerableEffectCompleted;
            }

            await Objspawn;

            if (m_Owner.IsMine)
            {
                while (true)
                {
                    if (objDisplay != null)
                    {
                        var fb = objDisplay.GetComponent<SpInvulnerableBehavior>();
                        fb.SetDuration(Duration);
                        fb.SetView(m_Owner);
                        owner.GetComponent<VehicleController>().GetMySkill().SetCurrentActiveSkill(fb);
                        objDisplay.transform.SetParent(m_Owner.transform.parent.GetChild(0));
                        objDisplay.transform.localPosition = (Vector3)data[1];
                        await wait;
                        //objDisplay.transform.SetParent(null);
                        break;
                    }

                    await wait;
                }

                m_Owner.RPC("CooldownAbilities", RpcTarget.All);
                PhotonNetwork.NetworkingClient.EventReceived -= RaisePowerEvent;
            }
        }

        private void SpInvulnerableEffectCompleted(AsyncOperationHandle<GameObject> obj)
        {
            objDisplay = obj.Result;
        }

        public override void RaisePowerEvent(EventData photonEvent)
        {
            if (photonEvent.Code == UniversalHandler.SPAWN_ABILITY_MESH)
            {
                object[] data = (object[])photonEvent.CustomData;

                var _owner = PhotonView.Find((int)data[0]);
                var owner = _owner.transform;
                onCast = (bool)data[2];

                var isDoneTask = PowerDuration(SkillCooldown);
                var EffectSpawnTask = PowerSpawner(owner, data);
            }
        }
    }
}