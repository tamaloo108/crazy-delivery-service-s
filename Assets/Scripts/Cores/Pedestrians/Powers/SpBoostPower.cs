﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace CDS.Power
{
    public class SpBoostPower : PowerBase
    {
        public override PowersEnum MyPower => PowersEnum.PowerSp_Boost;

        bool onCast = false;
        bool isDone = false;
        bool AbilitiesDepleted = false;
        bool isInstantiating = false;
        int MaxUsage = 0;
        int CurUsage = 0;
        float SkillCooldown = 0f;
        float Duration = 0f;
        private Transform T_Owner;
        private PhotonView m_Owner;
        private bool AbilitiesGoesCooldown;
        GameObject objDisplay;

        WaitForEndOfFrame RunDelay = new WaitForEndOfFrame();

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown)
        {
            this.MaxUsage = MaxUsage;
            this.CurUsage = CurUsage;

            AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

            if (AbilitiesDepleted || onCast)
            {
                PhotonView _owner = owner.GetComponent<PhotonView>();
                _owner.RPC("DoneAbilties", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn)
        {
            this.CurUsage = CurUsage;
            this.MaxUsage = MaxUsage;
            this.SkillCooldown = SkillCooldown;
            Duration = SkillDuration;
            T_Owner = owner;
            m_Owner = owner.GetComponent<PhotonView>();

            AbilitiesGoesCooldown = this.CurUsage >= this.MaxUsage;

            if (AbilitiesGoesCooldown || onCast || AlreadySpawn)
            {
                m_Owner.RPC("CooldownAbilities", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        private IEnumerator EventCaller(Transform owner)
        {
            PhotonView view = owner.GetComponent<PhotonView>();
            PhotonNetwork.NetworkingClient.EventReceived += RaisePowerEvent;
            yield return RaiseEventSetup(view);
            yield break;
        }

        private IEnumerator RaiseEventSetup(PhotonView view)
        {
            object[] content = new object[] { view.ViewID, new Vector3(0f, 0f, 4.3f), true, CurUsage, MaxUsage };
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(UniversalHandler.SPAWN_ABILITY_MESH, content, eventOptions, SendOptions.SendReliable);
            yield break;
        }

        async Task<Boolean> PowerDuration(float dur)
        {
            WaitForSeconds wait = new WaitForSeconds(dur);

            await wait;

            return true;
        }

        async Task PowerSpawner(Transform owner, object[] data)
        {
            WaitForEndOfFrame wait = new WaitForEndOfFrame();

            if (objDisplay == null && !isInstantiating)
            {
                isInstantiating = true;
                Addressables.InstantiateAsync("SpBoostEffect").Completed += SpBoostPower_completed;
            }

            while (true)
            {
                if (objDisplay != null)
                {
                    objDisplay.SetActive(true);
                    objDisplay.GetComponent<SpBoostBehavior>().SetUser(owner.parent.GetChild(0));
                    objDisplay.GetComponent<SpBoostBehavior>().SetDuration(Duration);
                    objDisplay.GetComponent<SpBoostBehavior>().SetView(m_Owner);
                    objDisplay.transform.SetParent(owner.parent.GetChild(0));
                    objDisplay.transform.localPosition = (Vector3)data[1];
                    //objDisplay.transform.localPosition = Vector3.zero;
                    objDisplay.transform.localRotation = Quaternion.LookRotation(Vector3.zero);
                    break;
                }
                await wait;
            }

            isInstantiating = false;
            PhotonNetwork.NetworkingClient.EventReceived -= RaisePowerEvent;
            return;
        }

        private void SpBoostPower_completed(AsyncOperationHandle<GameObject> obj)
        {
            objDisplay = obj.Result;
        }

        public override async void RaisePowerEvent(EventData photonEvent)
        {
            if (photonEvent.Code == UniversalHandler.SPAWN_ABILITY_MESH)
            {

                object[] data = (object[])photonEvent.CustomData;

                var _owner = PhotonView.Find((int)data[0]);
                var owner = _owner.transform;
                onCast = (bool)data[2];
                owner.GetComponent<VehicleController>().ActivateBoostAnim(true);
                var isDoneTask = PowerDuration(1f);
                var EffectSpawnTask = PowerSpawner(owner, data);

                await Task.WhenAll(isDoneTask, EffectSpawnTask);
                await EffectSpawnTask;
                isDone = await isDoneTask;
                if (isDone)
                {
                    isDone = false;
                    onCast = false;

                    owner.GetComponent<VehicleController>().ActivateBoostAnim(false);
                    //UnityEngine.Object.Destroy(objDisplay);
                    //_owner.RPC("DoneAbilties", RpcTarget.All);
                    m_Owner.RPC("CooldownAbilities", RpcTarget.All);
                }
            }
        }

    }
}