﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CDS.Power;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Threading.Tasks;
using System;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class FreezeBallPower : PowerBase
{
    public override PowersEnum MyPower => PowersEnum.FreezeBall;
    bool onCast = false;
    bool isDone = false;
    bool Executed = false;
    bool AbilitiesDepleted = false;
    int MaxUsage = 0;
    int CurUsage = 0;
    private float SkillDuration;
    private float SkillCooldown;
    float Cooldown = 0f;
    GameObject objDisplay;

    WaitForEndOfFrame RunDelay = new WaitForEndOfFrame();
    PhotonView m_Owner;

    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;
        Cooldown = SkillCooldown;

        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            PhotonView _owner = owner.GetComponent<PhotonView>();
            m_Owner = _owner;
            _owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }


    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;
        Cooldown = SkillCooldown;

        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            PhotonView _owner = owner.GetComponent<PhotonView>();
            m_Owner = _owner;
            _owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }

    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;

        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            PhotonView _owner = owner.GetComponent<PhotonView>();
            m_Owner = _owner;
            _owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }

    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;
        this.SkillDuration = SkillDuration;
        this.SkillCooldown = SkillCooldown;

        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            PhotonView _owner = owner.GetComponent<PhotonView>();
            m_Owner = _owner;
            _owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }

    IEnumerator EventCaller(Transform owner)
    {
        PhotonView view = owner.GetComponent<PhotonView>();
        PhotonNetwork.NetworkingClient.EventReceived += RaisePowerEvent;
        yield return RaiseEventSetup(view);
        yield break;
    }

    IEnumerator RaiseEventSetup(PhotonView view)
    {
        object[] content = new object[] { view.ViewID, new Vector3(0f, 0.65f, 2f), true, CurUsage, MaxUsage };
        RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(UniversalHandler.SPAWN_ABILITY_MESH, content, eventOptions, SendOptions.SendReliable);
        yield break;
    }

    async Task PowerSpawner(Transform owner, object[] data)
    {
        WaitForEndOfFrame wait = new WaitForEndOfFrame();

        if (objDisplay == null && !Executed)
        {
            Executed = true;
            Addressables.InstantiateAsync("FreezeProjectile").Completed += FreezeCompleted;
        }

        while (true)
        {
            if (objDisplay != null)
            {
                var fb = objDisplay.GetComponent<FreezeProjectileBehavior>();
                objDisplay.SetActive(true);
                fb.SetSpeed(9f);
                fb.SetLifeDuration(SkillDuration);
                fb.SetUser(owner.parent.GetChild(0));
                objDisplay.transform.SetParent(owner.parent.GetChild(0));
                objDisplay.transform.localPosition = (Vector3)data[1];
                await wait;
                objDisplay.transform.SetParent(null);
                break;
            }
            await wait;
        }

        PhotonNetwork.NetworkingClient.EventReceived -= RaisePowerEvent;
        return;
    }

    private void FreezeCompleted(AsyncOperationHandle<GameObject> obj)
    {
        objDisplay = obj.Result;
        var objView = obj.Result.GetComponent<PhotonView>();

        if (PhotonNetwork.AllocateViewID(objView))
        {
            object[] d = new object[]
            {
                objDisplay.transform.position, objDisplay.transform.rotation, objView.ViewID
            };

            RaiseEventOptions reO = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.Others,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions SendOps = new SendOptions
            {
                Reliability = true
            };

            PhotonNetwork.RaiseEvent(UniversalHandler.ADD_VIEW_ID, d, reO, SendOps);
        }
    }

    async Task<Boolean> PowerDuration(float dur) //for delay/cooldown.
    {
        WaitForSeconds wait = new WaitForSeconds(dur);

        await wait;

        return true;
    }

    public override async void RaisePowerEvent(EventData photonEvent)
    {
        if (photonEvent.Code == UniversalHandler.SPAWN_ABILITY_MESH)
        {

            object[] data = (object[])photonEvent.CustomData;

            var _owner = PhotonView.Find((int)data[0]);
            var owner = _owner.transform;
            onCast = (bool)data[2];

            var isDoneTask = PowerDuration(SkillCooldown);
            var EffectSpawnTask = PowerSpawner(owner, data);
            await Task.WhenAll(isDoneTask, EffectSpawnTask);
            await EffectSpawnTask;
            isDone = await isDoneTask;

            if (isDone)
            {
                isDone = false;
                onCast = false;

                //UnityEngine.Object.Destroy(objDisplay);
                _owner.RPC("DoneAbilties", RpcTarget.All);
            }
        }

        if (photonEvent.Code == UniversalHandler.ADD_VIEW_ID)
        {
            object[] data = (object[])photonEvent.CustomData;

            objDisplay.GetComponent<PhotonView>().ViewID = (int)data[2];
        }
    }

}
