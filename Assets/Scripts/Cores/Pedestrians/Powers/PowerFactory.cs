﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace CDS.Power
{
    public static class PowerFactory
    {
        static Dictionary<PowersEnum, Type> PowerDictionary;

        static bool initialized => PowerDictionary != null;

        static void Initialize()
        {
            PowerDictionary = new Dictionary<PowersEnum, Type>();

            var powers = Assembly.GetAssembly(typeof(PowerBase)).GetTypes().Where(x => x.IsClass && x.IsSubclassOf(typeof(PowerBase)));

            foreach (var item in powers)
            {

                var temp = Activator.CreateInstance(item) as PowerBase;
                PowerDictionary.Add(temp.MyPower, item);
            }
        }

        public static PowerBase GetPower(PowersEnum key)
        {
            if (!initialized)
            {
                Initialize();
            }

            if (PowerDictionary.ContainsKey(key))
            {
                Type p = PowerDictionary[key];
                var power = Activator.CreateInstance(p) as PowerBase;
                return power;
            }

            return null;
        }
    }
}