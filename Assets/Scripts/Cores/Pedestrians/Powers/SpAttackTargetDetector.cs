﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpAttackTargetDetector : MonoBehaviour
{
    [SerializeField] SpAttackBehavior MainSystem;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            MainSystem.Slowdown(other.gameObject);
        }
    }

}
