﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ExitGames.Client.Photon;
using Photon.Pun;
using System;
using Photon.Realtime;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace CDS.Power
{
    public class SpAttackPower : PowerBase
    {
        public override PowersEnum MyPower => PowersEnum.PowerSp_Attack;
        bool onCast = false;
        bool isDone = false;
        bool Assigned = false;
        bool AbilitiesGoesCooldown = false;
        int MaxUsage = 0;
        int CurUsage = 0;
        float SkillCooldown = 0f;
        float Duration = 0f;
        WaitForEndOfFrame RunDelay = new WaitForEndOfFrame();
        PhotonView m_Owner;
        Transform T_Owner;
        GameObject objDisplay;

        private bool Executed = false;

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn)
        {
            this.CurUsage = CurUsage;
            this.MaxUsage = MaxUsage;
            this.SkillCooldown = SkillCooldown;
            Duration = SkillDuration;
            T_Owner = owner;
            m_Owner = owner.GetComponent<PhotonView>();

            AbilitiesGoesCooldown = this.CurUsage >= this.MaxUsage;

            if (AbilitiesGoesCooldown || onCast || AlreadySpawn)
            {
                m_Owner.RPC("CooldownAbilities", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn)
        {
            this.CurUsage = CurUsage;
            this.MaxUsage = MaxUsage;
            this.SkillCooldown = SkillCooldown;

            T_Owner = owner;
            m_Owner = owner.GetComponent<PhotonView>();

            AbilitiesGoesCooldown = this.CurUsage >= this.MaxUsage;

            if (AbilitiesGoesCooldown || onCast || AlreadySpawn)
            {
                m_Owner.RPC("CooldownAbilities", RpcTarget.All);
                yield break;
            }

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }


        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown)
        {
            this.CurUsage = CurUsage;
            this.MaxUsage = MaxUsage;
            this.SkillCooldown = SkillCooldown;

            T_Owner = owner;
            m_Owner = owner.GetComponent<PhotonView>();

            //AbilitiesGoesCooldown = this.CurUsage >= this.MaxUsage;

            //if (AbilitiesGoesCooldown || onCast)
            //{
            //    m_Owner.RPC("CooldownAbilities", RpcTarget.All);
            //    yield break;
            //}

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage)
        {
            this.CurUsage = CurUsage;
            this.MaxUsage = MaxUsage;

            T_Owner = owner;
            m_Owner = owner.GetComponent<PhotonView>();

            AbilitiesGoesCooldown = this.CurUsage >= this.MaxUsage;

            //if (AbilitiesGoesCooldown || onCast)
            //{
            //    m_Owner.RPC("CooldownAbilities", RpcTarget.All);
            //    yield break;
            //}

            onCast = true;
            yield return EventCaller(owner);

            yield return RunDelay;
            yield break;
        }

        private IEnumerator EventCaller(Transform owner)
        {
            PhotonView view = owner.GetComponent<PhotonView>();
            PhotonNetwork.NetworkingClient.EventReceived += RaisePowerEvent;
            yield return RaiseEventSetup(view);
            yield break;
        }

        private IEnumerator RaiseEventSetup(PhotonView view)
        {
            object[] content = new object[] { view.ViewID, Vector3.zero, true, CurUsage, MaxUsage };
            RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(UniversalHandler.SPAWN_ABILITY_MESH, content, eventOptions, SendOptions.SendReliable);
            yield break;
        }

        private void SpAttackEffectCompleted(AsyncOperationHandle<GameObject> obj)
        {
            objDisplay = obj.Result;
            Assigned = true;
        }

        async Task<Boolean> PowerDuration(float skillCooldown)
        {
            WaitForSeconds wait = new WaitForSeconds(skillCooldown);

            await wait;

            return true;
        }

        async Task PowerSpawner(Transform owner, object[] data)
        {
            WaitForSeconds wait = new WaitForSeconds(0.01f);
            WaitUntil Objspawn = new WaitUntil(() => objDisplay != null && Assigned);

            if (objDisplay == null && !Executed)
            {
                Executed = true;
                Addressables.InstantiateAsync("SpAttackEffect").Completed += SpAttackEffectCompleted;
            }

            await Objspawn;

            if (m_Owner.IsMine)
            {
                while (true)
                {
                    if (objDisplay != null)
                    {
                        var fb = objDisplay.GetComponent<SpAttackBehavior>();
                        owner.GetComponent<VehicleController>().GetMySkill().SetCurrentActiveSkill(fb);
                        objDisplay.transform.SetParent(m_Owner.transform.parent.GetChild(0));
                        objDisplay.transform.localPosition = (Vector3)data[1];
                        fb.SetDuration(SkillCooldown);
                        fb.SetView(m_Owner);
                        await wait;
                        //objDisplay.transform.SetParent(null);
                        break;
                    }

                    await wait;
                }

                m_Owner.RPC("CooldownAbilities", RpcTarget.All);
                PhotonNetwork.NetworkingClient.EventReceived -= RaisePowerEvent;
            }
        }


        public override void RaisePowerEvent(EventData photonEvent)
        {
            if (photonEvent.Code == UniversalHandler.SPAWN_ABILITY_MESH)
            {
                object[] data = (object[])photonEvent.CustomData;

                var _owner = PhotonView.Find((int)data[0]);
                var owner = _owner.transform;
                onCast = (bool)data[2];

                var isDoneTask = PowerDuration(SkillCooldown);
                var EffectSpawnTask = PowerSpawner(owner, data);
            }
        }

    }
}
