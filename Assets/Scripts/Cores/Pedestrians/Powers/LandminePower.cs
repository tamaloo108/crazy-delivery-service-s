﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CDS.Power;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Threading.Tasks;
using System;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class LandminePower : PowerBase
{
    public override PowersEnum MyPower => PowersEnum.LandMine;
    bool onCast = false;
    bool isDone = false;
    bool Assigned = false;
    bool AbilitiesDepleted = false;
    int MaxUsage = 0;
    int CurUsage = 0;
    private float SkillCooldown;
    private float SkillDuration;
    float Cooldown = 0f;
    GameObject objDisplay;
    GameObject Mine;

    WaitForEndOfFrame RunDelay = new WaitForEndOfFrame();
    PhotonView m_Owner;
    Transform T_Owner;
    private bool Executed = false;

    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, bool AlreadySpawn)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;
        Cooldown = SkillCooldown;

        T_Owner = owner;
        m_Owner = owner.GetComponent<PhotonView>();


        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            m_Owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }


    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;
        Cooldown = SkillCooldown;

        T_Owner = owner;
        m_Owner = owner.GetComponent<PhotonView>();


        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            m_Owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }

    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;

        T_Owner = owner;
        m_Owner = owner.GetComponent<PhotonView>();


        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            m_Owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }

    public override IEnumerator CastPowerCoroutine(Transform owner, int CurUsage, int MaxUsage, float SkillCooldown, float SkillDuration, bool AlreadySpawn)
    {
        this.MaxUsage = MaxUsage;
        this.CurUsage = CurUsage;
        this.SkillCooldown = SkillCooldown;
        this.SkillDuration = SkillDuration;

        T_Owner = owner;
        m_Owner = owner.GetComponent<PhotonView>();


        AbilitiesDepleted = this.CurUsage >= this.MaxUsage;

        if (AbilitiesDepleted || onCast)
        {
            m_Owner.RPC("DoneAbilties", RpcTarget.All);
            yield break;
        }

        onCast = true;
        yield return EventCaller(owner);

        yield return RunDelay;
        yield break;
    }

    IEnumerator EventCaller(Transform owner)
    {
        PhotonView view = owner.GetComponent<PhotonView>();
        PhotonNetwork.NetworkingClient.EventReceived += RaisePowerEvent;
        yield return RaiseEventSetup(view);
        yield break;
    }

    IEnumerator RaiseEventSetup(PhotonView view)
    {
        object[] content = new object[] { view.ViewID, new Vector3(0f, -0.2f, -3.5f), true, CurUsage, MaxUsage };
        RaiseEventOptions eventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(UniversalHandler.SPAWN_ABILITY_MESH, content, eventOptions, SendOptions.SendReliable);
        yield break;
    }

    async Task<Boolean> PowerDuration(float dur)
    {
        WaitForSeconds wait = new WaitForSeconds(dur);

        await wait;

        return true;
    }

    async Task PowerSpawner(Transform owner, object[] data)
    {
        WaitForSeconds wait = new WaitForSeconds(0.01f);
        WaitUntil Objspawn = new WaitUntil(() => objDisplay != null && Assigned);
        PhotonView pView = owner.GetComponent<PhotonView>();
        if (objDisplay == null && !Executed)
        {
            Executed = true;
            Addressables.InstantiateAsync("LandMine").Completed += LandMineCompleted;
        }

        await Objspawn;

        if (m_Owner.IsMine)
        {
            var objView = objDisplay.GetComponent<PhotonView>();

            if (PhotonNetwork.AllocateViewID(objView))
            {
                object[] d = new object[]
                {
                new Vector3(0f, -0.2f, -3.5f), objDisplay.transform.rotation, objView.ViewID, true , m_Owner.ViewID
                };

                RaiseEventOptions reO = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.Others,
                    CachingOption = EventCaching.AddToRoomCache
                };

                SendOptions SendOps = new SendOptions
                {
                    Reliability = true
                };
                PhotonNetwork.RaiseEvent(UniversalHandler.ADD_VIEW_ID, d, reO, SendOps);

            }

            while (true)
            {
                if (objDisplay != null)
                {
                    var fb = objDisplay.GetComponent<LandmineBehavior>();
                    objDisplay.SetActive(true);
                    fb.SetUser(m_Owner.transform.parent.GetChild(0));
                    fb.SetLifeDuration(SkillDuration);
                    objDisplay.transform.SetParent(m_Owner.transform.parent.GetChild(0));
                    objDisplay.transform.localPosition = (Vector3)data[1];
                    await wait;
                    objDisplay.transform.SetParent(null);
                    objDisplay.GetComponent<Rigidbody>().isKinematic = false;
                    objDisplay.GetComponent<Collider>().isTrigger = false;
                    break;
                }

                await wait;
            }

            PhotonNetwork.NetworkingClient.EventReceived -= RaisePowerEvent;
        }


        Debug.Log("lm " + objDisplay.GetComponent<PhotonView>().ViewID);
        return;
    }

    private void LandMineCompleted(AsyncOperationHandle<GameObject> obj)
    {
        objDisplay = obj.Result;
        Assigned = true;
    }

    public override async void RaisePowerEvent(EventData photonEvent)
    {
        if (photonEvent.Code == UniversalHandler.SPAWN_ABILITY_MESH)
        {

            object[] data = (object[])photonEvent.CustomData;

            var _owner = PhotonView.Find((int)data[0]);
            var owner = _owner.transform;
            onCast = (bool)data[2];

            var isDoneTask = PowerDuration(SkillCooldown);
            var EffectSpawnTask = PowerSpawner(owner, data);
            await Task.WhenAll(isDoneTask, EffectSpawnTask);
            await EffectSpawnTask;
            isDone = await isDoneTask;

            if (isDone)
            {
                isDone = false;
                onCast = false;

                _owner.RPC("DoneAbilties", RpcTarget.All);
            }
        }

        if (photonEvent.Code == UniversalHandler.ADD_VIEW_ID)
        {
            Debug.Log("must be");
            WaitForEndOfFrame wait = new WaitForEndOfFrame();
            WaitUntil Objspawn = new WaitUntil(() => objDisplay != null && Assigned);
            object[] data = (object[])photonEvent.CustomData;
            Assigned = true;
            Assigned = (bool)data[3];

            m_Owner = PhotonView.Find((int)data[4]);

            Debug.Log(m_Owner.ViewID);

            objDisplay.GetComponent<PhotonView>().ViewID = (int)data[2];

            await Objspawn;

            while (true)
            {
                if (objDisplay != null)
                {
                    var fb = objDisplay.GetComponent<LandmineBehavior>();
                    objDisplay.SetActive(true);
                    fb.SetUser(m_Owner.transform.parent.GetChild(0));
                    fb.SetLifeDuration(SkillDuration);
                    objDisplay.transform.SetParent(m_Owner.transform.parent.GetChild(0));
                    objDisplay.transform.localPosition = (Vector3)data[0];
                    await wait;
                    objDisplay.transform.SetParent(null);
                    break;
                }
                await wait;
            }
            PhotonNetwork.NetworkingClient.EventReceived -= RaisePowerEvent;
        }
    }

    private void ClientCompletedLandmine(AsyncOperationHandle<GameObject> obj)
    {
        objDisplay = obj.Result;
    }

}
