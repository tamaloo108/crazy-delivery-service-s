﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.AddressableAssets;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Invincibilty : MonoBehaviourPun
{
    [SerializeField] AssetReference TwinkleStar;
    [SerializeField] AssetReference ZoomEffect;
    [SerializeField] Vector3 ZoomDistance = new Vector3(0f, 0f, 4f);
    CinemachineStateDrivenCamera CinemachineStateCam;
    CinemachineVirtualCamera PlayerCam;
    float ChargeDuration;
    Vector3 CurrentCameraDistance;
    Vector3 DesiredCameraDistance;
    Transform User;
    Coroutine Charges = null;
    Coroutine Invincibilities = null;
    WaitForFixedUpdate delay;
    WaitForSeconds waitDelay = new WaitForSeconds(0.1f);
    WaitUntil ChargeDurationAdded;
    WaitUntil UserAvailable;
    Vector3 UserForward;
    GameObject Victim;
    GameObject Zoomies;
    private Coroutine FrozenProjectile;
    StateProcessor States;
    CombatAudioHelper SoundHelper;


    bool Send = false;
    PhotonView UserPView;

    private void Awake()
    {
        delay = new WaitForFixedUpdate();
        CinemachineStateCam = GameObject.FindGameObjectWithTag("CinemachineStateDriven").GetComponent<Cinemachine.CinemachineStateDrivenCamera>();
        PlayerCam = CinemachineStateCam.ChildCameras[0].GetComponent<CinemachineVirtualCamera>();
        ChargeDurationAdded = new WaitUntil(() => ChargeDuration > 0f);
        UserAvailable = new WaitUntil(() => UserAvailable != null);
        DesiredCameraDistance = new Vector3(0f, 0f, -3f);
    }

    private void Start()
    {
        SoundHelper = GetComponent<CombatAudioHelper>();

        if (Charges == null)
        {
            Charges = StartCoroutine(DoCharge());
        }
    }

    private void OnDoneSpawnZoomEffect(GameObject obj)
    {
        Zoomies = obj;
    }

    public void SetChargeDuration(float v)
    {
        ChargeDuration = v;
    }

    private IEnumerator DoCharge()
    {
        yield return ChargeDurationAdded;
        yield return UserAvailable;
        CurrentCameraDistance = UniversalHandler.Instance.DefaultCameraHandler;
        var rb = User.parent.GetChild(1).gameObject.GetComponent<Rigidbody>();
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        SoundHelper.PlayAudio("SFX_NitroStart_1");

        if (User.GetComponent<PhotonView>().IsMine)
        {
            UniversalAddresableLoader.Instance.Spawn(ZoomEffect, Vector3.zero, null, OnDoneSpawnZoomEffect);
            if (CurrentCameraDistance != DesiredCameraDistance)
                PlayerCam.GetComponent<CinemachineCameraOffset>().m_Offset += ZoomDistance;
            UserPView.RPC("RPC_SetOnState", RpcTarget.All, UniversalHandler.PLAYER_CHARGE_ON);
        }

        SoundHelper.PlayAudio("SFX_Nitro_1");

        while (ChargeDuration > 0f)
        {
            yield return UserAvailable;
            UserForward = GetUserForward();

            Push(User.parent.GetChild(1).gameObject, UserForward, 5.15f);
            //Push(User.parent.GetChild(1).gameObject, UserForward, 0f);
            yield return delay;
            ChargeDuration -= Time.fixedDeltaTime;
        }

        Charges = null;
        rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
        UserPView.RPC("RPC_SetOnState", RpcTarget.All, UniversalHandler.PLAYER_CHARGE_OFF);
        Destroy(Zoomies);
        Destroy(gameObject);

        yield break;
    }

    private Vector3 GetUserForward()
    {
        return User.parent.GetChild(0).transform.forward;
    }

    public void SetUser(Transform user)
    {
        User = user;
        UserPView = user.GetComponent<PhotonView>();
    }

    private void OnDestroy()
    {
        Charges = null;

        if (User.GetComponent<PhotonView>().IsMine)
        {
            PlayerCam.GetComponent<CinemachineCameraOffset>().m_Offset = CurrentCameraDistance;
        }

        if (Zoomies == null) return;
        Destroy(Zoomies);
    }

    private void Push(GameObject gameObject, Vector3 direction, float force)
    {
        var rb = gameObject.GetComponent<Rigidbody>();
        if (rb == null) return;
        rb.isKinematic = false;
        // rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rb.AddForce(direction * force, ForceMode.VelocityChange);
    }
}
