﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Photon.Pun;

public class FreezeProjectileBehavior : MonoBehaviourPunCallbacks
{
    [SerializeField] AssetReference StarOnHead;

    float Speed;
    float LifeDuration;
    Transform User = null;
    Rigidbody myRB;
    Vector3 UserForward = Vector3.zero;
    Coroutine ProjectileMovement;
    Coroutine FrozenProjectile;
    WaitForFixedUpdate delay = new WaitForFixedUpdate();
    WaitUntil UserAdd;
    WaitUntil DurationAdd;
    CombatAudioHelper soundHelper;
    StateProcessor States;
    bool forward = false;
    bool Moving = true;
    private Vector3 velocity;

    private void Awake()
    {
        DurationAdd = new WaitUntil(() => LifeDuration > 0f);
        UserAdd = new WaitUntil(() => User != null);
        transform.GetChild(0).GetComponent<Renderer>().enabled = false;


    }

    public void SetLifeDuration(float v)
    {
        LifeDuration = v;
    }

    public void SetSpeed(float spd)
    {
        Speed = spd;
    }

    public void SetUser(Transform User)
    {
        this.User = User;
    }

    private void Start()
    {
        myRB = GetComponent<Rigidbody>();
        States = new StateProcessor();
        soundHelper = GetComponent<CombatAudioHelper>();

        if (ProjectileMovement == null)
        {
            ProjectileMovement = StartCoroutine(MoveBullet());
        }
    }

    private Vector3 GetUserForward()
    {
        return User.parent.GetChild(0).transform.forward;
    }

    private IEnumerator MoveBullet()
    {
        yield return DurationAdd;
        yield return UserAdd;
        if (!forward)
        {
            UserForward = GetUserForward();
            forward = true;
        }
        float currentVol = 0f;
        soundHelper.GetMixer().GetFloat("SFXVol", out currentVol);
        SetMove(UserForward);
        soundHelper.PlayAudio("SFX_RocketLaunch_1");
        yield return delay;
        transform.GetChild(0).GetComponent<Renderer>().enabled = true;
        soundHelper.PlayAudio("SFX_RocketFly_1");
        while (LifeDuration > 0f)
        {

            if (!Moving) break;

            SetMove(UserForward);
            yield return delay;
            LifeDuration -= 0.01f;
        }

        ProjectileMovement = null;
        photonView.RPC("RPC_Destroy", RpcTarget.All);

        yield break;
    }

    private void SetMove(Vector3 forward)
    {
        //var move = Vector3.Lerp(myRB.position, myRB.position + forward, Speed);
        if (Moving)
        {
            var move = Vector3.SmoothDamp(myRB.position, myRB.position + forward, ref velocity, 0.003f, 300f);
            myRB.MovePosition(move);
            myRB.MoveRotation(Quaternion.LookRotation(forward));
        }
    }

    private void OnTriggerStay(Collider other)
    {
        try
        {
            var parent = other.transform.parent;

            if (parent.transform.GetChild(0) == null) return;

            if (parent.transform.GetChild(0) != User && other.transform != User)
            {
                gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

                var Handler = parent.transform.GetChild(0).gameObject.GetComponent<VehicleController>();

                if (FrozenProjectile == null)
                {
                    Debug.Log(parent.transform.GetChild(0).name);
                    UniversalHandler.Instance.VFX.CallParticles("Explosion", other.transform.position);
                    soundHelper.PlayAudio("SFX_RocketExplode_1");

                    if (States.GetStates(Handler) == VehicleStates.Invulnerable)
                    {
                        FrozenProjectile = StartCoroutine(OnDoneExecute());
                    }
                    else
                    {
                        parent.transform.GetChild(0).GetComponent<NgojekHandler>().ReleasePedestrian();
                        if (Handler.GetComponent<PhotonView>().Owner.IsLocal)
                        {
                            UniversalHandler.Instance.InvokeInitCameraOffset(10f, 10f);
                            UniversalHandler.Instance.InvokeCameraShake(20f, 20f, 0.03f);
                        }
                        KnockEm(parent.transform.GetChild(1));
                        User.GetComponent<PhotonView>().Owner.AddScore(10);
                        UniversalAddresableLoader.Instance.Spawn(StarOnHead, Vector3.up, Handler.gameObject, OnDoneSpawnBintang);
                        FrozenProjectile = StartCoroutine(States.SetFrozenEffect(Handler, OnDoneExecute()));
                    }
                }
            }


        }
        catch (Exception)
        {

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Building"))
        {
            soundHelper.PlayAudio("SFX_RocketExplode_1");
            gameObject.GetComponentInChildren<Renderer>().enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;
            Moving = false;
            UniversalHandler.Instance.VFX.CallParticles("Explosion", transform.localPosition);
            photonView.RPC("RPC_Destroy", RpcTarget.All);
            //Destroy(gameObject);
        }
    }

    private void ParentToTarget()
    {

    }

    private void KnockEm(Transform transform)
    {
        var pView = transform.GetComponent<PhotonView>();
        pView.RPC("PlaySoundKnocked", RpcTarget.All);
        Rigidbody rb = transform.GetComponent<Rigidbody>();
        rb.AddForce(27f * transform.up, ForceMode.VelocityChange);
    }

    private IEnumerator OnDoneExecute()
    {
        photonView.RPC("RPC_Destroy", RpcTarget.All);
        yield break;
    }

    private void OnDoneSpawnBintang(GameObject obj)
    {
        Destroy(obj, 2f);
    }

    [PunRPC]
    void RPC_Destroy()
    {
        enabled = false;
        Destroy(gameObject, 1f);
    }
}
