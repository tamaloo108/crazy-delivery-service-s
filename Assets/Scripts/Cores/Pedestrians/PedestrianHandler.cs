﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Photon.Pun;

public class PedestrianHandler : MonoBehaviourPunCallbacks, IPedestrianHandler, IPunObservable
{
    //need rework someday.

    [SerializeField] List<string> AnimNames;
    [SerializeField] Transform Ragdoll;
    [SerializeField] bool EnableRagdoll = false;
    [SerializeField] Transform MeshesParent;
    [SerializeField] GameObject StealHUD;
    Animator myAnim;
    List<Rigidbody> MyRagdollRb = new List<Rigidbody>();
    List<Collider> MyRagdollCol = new List<Collider>();
    Rigidbody mainRb;
    Collider mainCollider;
    float KnockedTimeLeft = 3f;
    float timeLeft = 4f;
    bool DisableOrder = false;
    bool RagdollTime = false;
    bool OnTravelling = false;
    Quaternion CurrentCollisionDirection;
    Vector3 RagdollPosition;
    Vector3 currentMovePosition;
    AnimatorTransitionInfo TransInfo;
    AnimatorStateInfo StateInfo;
    PowerUpManager MyPower;
    PedestrianDestinationHandler DestinationHandler;
    Transform CurrentBikeSeat;
    CombatAudioHelper SoundHelper;
    Coroutine ResetPosition;
    Coroutine Stolen;

    public event Action<int> OnOrderDone;
    WaitForSeconds delay = new WaitForSeconds(0.001f);
    WaitForSecondsRealtime OrderBuffered = new WaitForSecondsRealtime(1.5f);
    private void Awake()
    {
        DestinationHandler = GetComponent<PedestrianDestinationHandler>();
        myAnim = GetComponent<Animator>();
        mainCollider = GetComponent<Collider>();
        mainRb = GetComponent<Rigidbody>();
        MyPower = GetComponent<PowerUpManager>();
    }

    private void Start()
    {
        SoundHelper = GetComponent<CombatAudioHelper>();

        if (EnableRagdoll)
        {
            InitRagdoll();
            photonView.RPC("ActivateRagdollNetwork", RpcTarget.All);
            photonView.RPC("DeactiveRagdollNetwork", RpcTarget.All);
        }
    }

    private void SetMainRigidbodyState(bool isKinematic)
    {
        mainRb.isKinematic = isKinematic;
    }

    bool IsOnIdle()
    {
        TransInfo = myAnim.GetAnimatorTransitionInfo(0);
        StateInfo = myAnim.GetCurrentAnimatorStateInfo(0);
        return StateInfo.fullPathHash == Animator.StringToHash("Base Layer.Idle");
    }

    private void Update()
    {
        if (EnableRagdoll)
        {
            CountRagdollTime();
        }
    }

    public PowersEnum GetMyPower()
    {
        return MyPower.GetPower();
    }

    private void CountRagdollTime()
    {

        if (timeLeft > 0f && RagdollTime)
        {
            SetParentPositionToMesh();
            timeLeft -= Time.deltaTime;
        }
        else if (timeLeft <= 0f && RagdollTime)
        {
            RagdollTime = false;
            SetRagdollState(true);
            SetMeshesTransform();
            SetAnimationClip();
            timeLeft = 4f;
        }

        if (IsOnIdle() && !DestinationHandler.IsOnTravel())
        {
            SetMainColliderState(false);
            SetMainRigidbodyState(false);
        }
    }

    private void SetAnimationClip()
    {
        myAnim.CrossFade(AnimNames[UnityEngine.Random.Range(0, 1)], 0.01f, 0, UnityEngine.Random.Range(0.0f, 0.03f));
    }

    private int GetStandingAnim()
    {
        var forward = Ragdoll.transform.forward;
        return forward.z >= 1f ? 1 : 0;
    }

    private void SetParentPositionToMesh()
    {
        RagdollPosition = Ragdoll.position;
        mainRb.MovePosition(RagdollPosition);
    }

    private void SetMeshesTransform()
    {
        Ragdoll.SetParent(MeshesParent);
        Ragdoll.localRotation = transform.rotation;
    }

    private void DetectCollision()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, CurrentCollisionDirection, Time.deltaTime * 5f);
    }

    public bool isDisableOrder()
    {
        return DisableOrder;
    }

    private void OnCollisionEnter(Collision collision)
    {
        CalculateCollisionAngle(collision);
    }

    private void CalculateCollisionAngle(Collision collision)
    {
        if (collision.transform.tag.Equals("Player Collider"))
        {
            var normal = collision.contacts[0].normal;
            var collisionAngle = 90 - (Vector3.Angle(collision.relativeVelocity, -normal));

            CurrentCollisionDirection = Quaternion.AngleAxis(collisionAngle, Vector3.up);
            if (collision.relativeVelocity.sqrMagnitude > 9.6f && EnableRagdoll)
            {
                photonView.RPC("ActivateRagdollNetwork", RpcTarget.All);
            }
        }
    }

    private void SetMainColliderState(bool isTrigger)
    {
        mainCollider.isTrigger = isTrigger;
    }

    private void SetRagdollState(bool isKinematic)
    {
        for (int i = 0; i < MyRagdollRb.Count; i++)
        {

            MyRagdollRb[i].isKinematic = isKinematic;
        }

        for (int i = 0; i < MyRagdollCol.Count; i++)
        {
            MyRagdollCol[i].isTrigger = isKinematic;
        }

        myAnim.enabled = isKinematic;
    }

    private void InitRagdoll()
    {
        var _rb = GetComponentsInChildren<Rigidbody>();
        var _bx = GetComponentsInChildren<Collider>();

        for (int i = 0; i < _bx.Length; i++)
        {
            if (mainCollider.GetInstanceID() != _bx[i].GetInstanceID())
            {

                MyRagdollCol.Add(_bx[i]);
            }
        }

        for (int i = 0; i < _rb.Length; i++)
        {
            if (mainRb.GetInstanceID() != _rb[i].GetInstanceID())
            {
                MyRagdollRb.Add(_rb[i]);
            }
        }
    }

    private void OnRagdollTime(PhotonStream stream)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(RagdollPosition);
            stream.SendNext(RagdollTime);
            stream.SendNext(timeLeft);
        }
        else
        {
            RagdollPosition = (Vector3)stream.ReceiveNext();
            RagdollTime = (bool)stream.ReceiveNext();
            timeLeft = (float)stream.ReceiveNext();
        }
    }

    private IEnumerator OnTimerBeforeDie()
    {
        SoundHelper.PlayAudio("SFX_KnockUp_1");
        yield return OrderBuffered;
        StealHUD.SetActive(true);
        DisableOrder = false;

        while (KnockedTimeLeft > 0f)
        {
            yield return delay;
            KnockedTimeLeft -= Time.deltaTime;

            if (OnTravelling)
            {
                DisableOrder = false;
                StealHUD.SetActive(false);
                Stolen = null;
                yield break;
            }
        }

        StealHUD.SetActive(false);
        DoneTravelling();
        Stolen = null;
        yield break;
    }

    #region Interfaces

    bool IPedestrianHandler.isOnRagdoll()
    {
        return RagdollTime;
    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        OnRagdollTime(stream);
    }

    void IPedestrianHandler.PlayAnim(string name)
    {
        myAnim.CrossFade(name, 0.05f, 0, 0.1f);
    }

    void IPedestrianHandler.OnTravelState(bool state)
    {
        SetMainColliderState(state);
        SetMainRigidbodyState(state);
        OnTravelling = state;


    }

    #endregion

    #region RPCs

    public void SendRPC(bool travelState, string animName)
    {
        photonView.RPC("RPC_OnTravelState", RpcTarget.All, travelState);
        photonView.RPC("RPC_PlayAnim", RpcTarget.All, animName);
    }

    public void DoneTravelling()
    {
        photonView.RPC("RPC_DoneTravelling", RpcTarget.All);
    }

    public void OnReadySteal()
    {
        photonView.RPC("RPC_OnStealState", RpcTarget.All);
    }

    [PunRPC]
    void RPC_OnStealState()
    {
        DisableOrder = true;
        MyPower.ResetUsage();
        if (Stolen == null)
        {
            Stolen = StartCoroutine(OnTimerBeforeDie());
        }
    }

    [PunRPC]
    void DeactiveRagdollNetwork()
    {
        if (OnTravelling) return;

        if (!EnableRagdoll) return;
        CountRagdollTime();
    }

    [PunRPC]
    void ActivateRagdollNetwork()
    {
        if (OnTravelling) return;

        if (!EnableRagdoll) return;
        SetRagdollState(false);

        SetMainRigidbodyState(true);
        SetMainColliderState(true);
        Ragdoll.SetParent(null);
        RagdollTime = true;
    }

    [PunRPC]
    void RPC_PlayAnim(string name)
    {
        myAnim.CrossFade(name, 0.05f, 0, 0.1f);
    }

    [PunRPC]
    void RPC_OnTravelState(bool state)
    {
        StealHUD.SetActive(false);
        OnTravelling = state;

        SetMainColliderState(state);
        SetMainRigidbodyState(state);
    }

    [PunRPC]
    void RPC_DoneTravelling()
    {
        SoundHelper.PlayAudio("SFX_Arrived_1");
        MyPower.ResetUsage();
        OnOrderDone?.Invoke(gameObject.GetHashCode());
        DisableOrder = true;
    }

    #endregion
}
