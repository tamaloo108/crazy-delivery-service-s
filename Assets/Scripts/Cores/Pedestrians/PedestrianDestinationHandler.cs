﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedestrianDestinationHandler : MonoBehaviourPun, IDestinationHandler
{
    [SerializeField] List<Transform> Destinations;
    [SerializeField] GameObject IdentifierParticles;
    [SerializeField] Photon.Pun.PhotonTransformView TView;
    Vector3 CurrentDestination = Vector3.zero;
    Animator myAnim;
    Transform CurrentDestinationTransform;
    bool OnTravel = false;

    private void Start()
    {
        InitDestinations();
    }

    private void InitDestinations()
    {
        myAnim = GetComponent<Animator>();
        var _temp = GameObject.FindGameObjectsWithTag("Destination");
        for (int i = 0; i < _temp.Length; i++)
        {
            Destinations.Add(_temp[i].transform);
            Destinations[i].GetChild(0).gameObject.SetActive(false);
        }
    }

    void IDestinationHandler.SetDestination()
    {
        if (!OnTravel)
        {
            CurrentDestination = Destinations[UnityEngine.Random.Range(0, Destinations.Count)].position;
        }
    }

    void IDestinationHandler.SetTravelState(bool state)
    {
        OnTravel = state;
    }

    internal bool IsOnTravel()
    {
        return OnTravel;
    }

    bool IDestinationHandler.GetTravelState()
    {
        return IsOnTravel();
    }

    #region RPC

    internal void SendRPC(bool SeatModeState, bool TravelState, int SenderID)
    {
        photonView.RPC("RPC_SetDestination", RpcTarget.All);
        photonView.RPC("RPC_SeatModeState", RpcTarget.All, SeatModeState, SenderID);
        photonView.RPC("RPC_SetTravelState", RpcTarget.All, TravelState);
    }

    [PunRPC]
    void RPC_SetDestination()
    {
        if (!OnTravel)
        {
            var SelDest = UnityEngine.Random.Range(0, Destinations.Count);
            var SelDestTrans = UnityEngine.Random.Range(0, Destinations.Count);
            CurrentDestination = Destinations[SelDest].position;
            CurrentDestinationTransform = Destinations[SelDest];
        }
    }

    [PunRPC]
    void RPC_SeatModeState(bool state, int SenderID)
    {

        var sender = PhotonView.Find(SenderID).gameObject.GetComponent<NgojekHandler>();
        var seat = sender.GetKursiPenumpang();
        myAnim.SetBool("OnRide", state);

        IdentifierParticles.SetActive(!state);

        if (state)
        {
            TView.enabled = false;
            if (sender.GetComponent<PhotonView>().IsMine)
            {
                CurrentDestinationTransform.GetChild(0).gameObject.SetActive(true);
            }
            transform.SetParent(seat);
            transform.localPosition = new Vector3(0f, -.3f, 0f);
            transform.localRotation = Quaternion.LookRotation(Vector3.zero);
        }
        else
        {
            TView.enabled = true; if (sender.GetComponent<PhotonView>().IsMine)
            {
                CurrentDestinationTransform.GetChild(0).gameObject.SetActive(false);
            }

            transform.SetParent(null);
            transform.position += Vector3.forward * 2f;
            for (int i = 0; i <= 3; i++)
            {
                UniversalHandler.Instance.VFX.CallParticles("CoinFlew", transform.position, true, seat);
            }
        }

    }

    [PunRPC]
    void RPC_SetTravelState(bool state)
    {
        OnTravel = state;
    }

    #endregion

    Vector3 IDestinationHandler.GetCurrentDestination()
    {
        return CurrentDestination;
    }

    void IDestinationHandler.SeatModeState(bool state, Transform seat)
    {
        if (state)
        {
            transform.SetParent(seat);
            transform.localPosition = Vector3.zero;
        }
        else
        {
            transform.SetParent(seat);
            transform.position += Vector3.forward * 1.25f;
        }
    }

    Transform IDestinationHandler.GetCurrentDestinationTransform()
    {
        return CurrentDestinationTransform;
    }
}
