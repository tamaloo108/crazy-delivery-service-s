﻿using System;
using UnityEngine;
using Photon.Pun;

public class PedestrianProcessor
{

    public bool OnMaxUsage(INgojeck user)
    {
        return user.GetPowerFromClient().GetCurrentUsage() >= user.GetPowerFromClient().GetMaxUsage();
    }

    public int GetCurrentUsage(INgojeck user)
    {
        return user.GetPowerFromClient().GetCurrentUsage();
    }

    public int GetMaxUsage(INgojeck user)
    {
        return user.GetPowerFromClient().GetMaxUsage();
    }

    public bool OnPickUpButtonClick(IVehicleController controller)
    {
        return controller.GetPickup();
    }

    public Vector3 GetCurrentDestination(IDestinationHandler handler)
    {
        return handler.GetCurrentDestination();
    }

    public Transform GetCurrentDestinationTransform(IDestinationHandler handler)
    {
        return handler.GetCurrentDestinationTransform();
    }

    public void DoPickup(IDestinationHandler handler, IPedestrianHandler penumpang, INgojeck ojek, Transform seat)
    {
        if (!penumpang.isOnRagdoll())
        {
            penumpang.PlayAnim("Sit");
            penumpang.OnTravelState(true);
            handler.SetDestination();
            ojek.SetDestination(handler.GetCurrentDestination());
            ojek.SetOnTravelState(true);
            handler.SetTravelState(true);
            handler.SeatModeState(true, seat);
        }
    }

    internal void Drop(IDestinationHandler currentDestinationHandler, IPedestrianHandler penumpang, INgojeck ngojekHandler, IVehicleController myController)
    {
        if (myController.GetPickup())
        {
            if (ngojekHandler.GetOnTravelState())
            {
                penumpang.PlayAnim("Warrior Idle");
                ngojekHandler.SetDestination(Vector3.zero);
                ngojekHandler.SetOnTravelState(false);
                currentDestinationHandler.SetTravelState(false);
                currentDestinationHandler.SeatModeState(false, null);
            }
        }
    }

    internal void ResetPosition(IVehicleController myController, Vector3 startPosition)
    {
        myController.ForceSetPosition(startPosition);
    }
}
