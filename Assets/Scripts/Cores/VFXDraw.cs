﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class VFXDraw : MonoBehaviour
{
    [SerializeField] List<VisualEffectList> VFXList;

    Transform m_Target;
    Action<GameObject> m_Callback;
    Action m1_callback;

    private void Start()
    {
        UniversalHandler.Instance.VFX = this;
    }

    public void CallParticles(string VfxName, Vector3 position, Action Callback)
    {
        m1_callback = Callback;
        UniversalAddresableLoader.Instance.Spawn(VFXList.Find(x => x.GetName().Equals(VfxName)).GetRef(), position, null, OnDoneSpawnCallback);
    }

    public void CallParticles(string VfxName, Vector3 position)
    {
        UniversalAddresableLoader.Instance.Spawn(VFXList.Find(x => x.GetName().Equals(VfxName)).GetRef(), position, null, OnDoneSpawn);
    }

    public void CallParticles(string vfxName, Vector3 position, bool FlewToTarget, Transform Target)
    {
        if (FlewToTarget)
        {
            m_Target = Target;
            UniversalAddresableLoader.Instance.Spawn(VFXList.Find(x => x.GetName().Equals(vfxName)).GetRef(), position, null, OnDoneSpawnTarget);
        }
        else
        {
            CallParticles(vfxName, position);
        }
    }

    private void OnDoneSpawnCallback(GameObject obj)
    {
        m1_callback();
        var p = obj.GetComponent<ParticleSystem>();
        p.Simulate(0f, true, true);
        p.Play();
        m1_callback = null;
    }

    private void OnDoneSpawnTarget(GameObject obj)
    {
        var t = obj.GetComponent<CoinPropsHandler>();
        t.PlayCoins(m_Target);
    }

    private void OnDoneSpawn(GameObject obj)
    {
        var p = obj.GetComponent<ParticleSystem>();
        p.Simulate(0f, true, true);
        p.Play();
    }
}

[System.Serializable]
public class VisualEffectList
{
    [SerializeField] AssetReference VisualEffectRef;
    [SerializeField] string VfxName;

    public string GetName()
    {
        return VfxName;
    }

    public AssetReference GetRef()
    {
        return VisualEffectRef;
    }
}
