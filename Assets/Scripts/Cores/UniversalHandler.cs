﻿using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Cinemachine;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using ExitGames.Client.Photon;

public class UniversalHandler : Singleton<UniversalHandler>
{
    #region EVENT COMMAND

    #region RaiseEvents
    public const byte ROOM_FULL = 1;
    public const byte START_LOADING = 2;
    public const byte SPAWN_ABILITY_MESH = 3;
    public const byte USE_ABILITIES = 4;
    public const byte ABILITIES_END = 5;
    public const byte ARROW_HIDDEN = 6;
    public const byte ARROW_POINT = 7;
    public const byte ARROW_SECURED = 8;
    public const byte MATCH_END = 9;
    public const byte SEND_PLAYER_BOARD = 10;
    public const byte ADD_VIEW_ID = 11;
    public const byte HIDE_OBJ_MESH = 12;
    public const byte MANUAL_NETWORK_INSTANTIATE = 13;
    public const byte PLAYER_REMOTE_KNOCK = 14;
    public const byte PLAYER_DELETE_PARENT = 15;
    public const byte ABILITIES_COOLDOWN = 16;
    #endregion

    #region General Events
    public const string PLAYER_LOAD_LEVEL = "LevelLoaded";
    public const string PLAYER_SCORE = "PlayerScore";
    public const string PLAYER_MATCH_RANK = "PlayerMatchRank";
    public const int DRIVER_ABILITIES = 220;
    public const int CLIENT_ABILITES = 221;
    public const int BOX_ABILITIES = 222;
    public const int KNOCK_UP = 300;
    public const int KNOCK_DOWN = 301;
    public const int KNOCK_LEFT = 302;
    public const int KNOCK_RIGHT = 303;
    public const int KNOCK_FORWARD = 304;
    public const int KNOCK_BACK = 305;
    public const byte PLAYER_CHARGE_ON = 101;
    public const byte PLAYER_CHARGE_OFF = 102;
    public const byte PLAYER_INVULNERABLE_ON = 103;
    public const byte PLAYER_INVULNERABLE_OFF = 104;
    #endregion

    #region Character Selection
    public const int SELECT_FREEMAN = 501;
    public const int SELECT_MBAH_KEKOK = 502;
    public const int SELECT_ALIEN = 503;
    public const int SELECT_TAXIMAN = 504;
    public int CHARACTER_CODE;
    public const string freeman = "PhotonResources/MainMenuPodium/Bike 1";
    public const string mbah = "PhotonResources/MainMenuPodium/Bike 2";

    #endregion

    #region Abilities Events
    public const int MBAH_ABILITY_1 = 800;
    public const int PREMAN_ABILITY_1 = 801;
    public const int JAMTAXI_ABILITY_1 = 802;
    #endregion


    #endregion



    GameLauncher Launcher;
    ForceNetworkDeleter FNS;
    public VFXDraw VFX { get; set; }
    private Vector3 _position;
    private Quaternion _rotation;
    private int _viewID;
    CinemachineStateDrivenCamera DrivenCamera;
    public GameSettingHandler GameSettings { get; private set; }
    public Camera GlobalCamera;
    public GameObject BGScreen;

    public delegate void OnInitCameraShake(float amp, float freq, float dur);
    public delegate void OnInitCameraOffset(float minOffset, float MaxOffset);
    public event OnInitCameraShake InitCameraShake;
    public event OnInitCameraOffset InitCameraOffset;
    public Vector3 DefaultCameraHandler;

    public void SetManager(GameLauncher launcher)
    {
        Launcher = launcher;
    }

    public void setcharactercode(int code) {
        CHARACTER_CODE = code;
    }

    public int getcharactercode() 
    {
        return this.CHARACTER_CODE;
    }

    public void InitGameSettings(GameSettingHandler GSH)
    {
        GameSettings = GSH;
    }

    public void SetBGScreen(GameObject screen)
    {
        BGScreen = screen;
    }

    public void SetDrivenCamera(CinemachineStateDrivenCamera cam)
    {
        DrivenCamera = cam;
    }

    public CinemachineStateDrivenCamera GetDrivenCamera()
    {
        return DrivenCamera;
    }

    public GameLauncher GetManager()
    {
        return Launcher;
    }

    public void SetDeleteUtility(ForceNetworkDeleter fns)
    {
        FNS = fns;
    }

    public ForceNetworkDeleter GetDeleter()
    {
        return FNS;
    }

    public void ManualNetworkedInstantiation(GameObject target, Vector3 position, Quaternion rotation, int viewID)
    {


    }

    public void ManualNetworkedInstantiation(AssetReference target, Vector3 position, Quaternion rotation, int viewID)
    {
        _position = position;
        _rotation = rotation;
        _viewID = viewID;
        Addressables.InstantiateAsync(target).Completed += OnDoneInstantiate;
    }

    internal void InvokeInitCameraOffset(float v1, float v2)
    {
        InitCameraOffset.Invoke(v1, v2);
    }

    internal void InvokeCameraShake(float v1, float v2, float v3)
    {
        InitCameraShake.Invoke(v1, v2, v3);
    }

    private void OnDoneInstantiate(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
    {
        PhotonView pView = obj.Result.GetComponent<PhotonView>();

        object[] content = new object[]
        {
            _viewID,
            _position,
            _rotation
        };

        RaiseEventOptions rEvent = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.Others,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOps = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent(UniversalHandler.MANUAL_NETWORK_INSTANTIATE, content, rEvent, sendOps);
    }
}

#region EXTENSION

public static class ScoreExtension
{

    public static void SetScore(this Player player, int score)
    {
        Hashtable _score = new Hashtable();
        _score[UniversalHandler.PLAYER_SCORE] = score;

        player.SetCustomProperties(_score);
    }

    public static int GetScore(this Player player)
    {
        object score;
        if (player.CustomProperties.TryGetValue(UniversalHandler.PLAYER_SCORE, out score))
        {
            return (int)score;
        }

        return 0;
    }

    public static int GetPlayerMatchRank(this Player p)
    {
        object rank;
        if (p.CustomProperties.TryGetValue(UniversalHandler.PLAYER_MATCH_RANK, out rank))
            return (int)rank;

        return 99;
    }

    public static void AddScore(this Player player, int value)
    {
        int current = player.GetScore();
        current += value;

        Hashtable _score = new Hashtable();
        _score[UniversalHandler.PLAYER_SCORE] = current;

        player.SetCustomProperties(_score);

    }
}

public static class PlayerExtension
{
    public static int GetPlayerID(this Player p)
    {
        return p.UserId.GetHashCode();
    }
}

public static class StateDrivenCameraExtension //only works on this mayeebe,
{
    public static void SetAnimState(this CinemachineStateDrivenCamera cam, string stateName, bool val)
    {
        var anim = cam.GetComponent<Animator>();
        if (anim != null)
        {
            anim.SetBool(stateName, val);
        }

    }

    public static CinemachineVirtualCamera GetLiveCamera(this CinemachineStateDrivenCamera cam)
    {
        return cam.LiveChild.VirtualCameraGameObject.GetComponent<CinemachineVirtualCamera>();
    }
}

public static class NgojekHandlerExtension
{
    public static void ReleasePedestrian(this INgojeck ojeck)
    {
        ojeck.ReleasePedestrian();
    }

    public static void FallDown(this INgojeck ojeck)
    {
        ojeck.Fall();
    }
}

public static class VehicleControllerExtension
{
    public static void ActivateBoostAnim(this IVehicleController controller, bool state)
    {
        controller.ActivateBoostAnim(state);
    }
}

public static class ControllerHUDHandlerExpansion
{
    public static void SetPickButtonState(this ControllerHUDHandler handler, bool v, bool OnPick)
    {
        handler.SetPickButtonState(v, OnPick);
    }

    public static void SetClientSpecialButtonState(this ControllerHUDHandler handler, bool v)
    {
        handler.SetClientSpecialButtonState(v);
    }

    public static void SetDriverSpecialButtonState(this ControllerHUDHandler handler, bool v)
    {
        handler.SetDriverSpecialButtonState(v);
    }

    public static void SetBoxPowerButtonState(this ControllerHUDHandler handler, bool v)
    {
        handler.setBoxPowerButtonState(v);
    }

    public static void SetClientSpecialButtonState(this ControllerHUDHandler handler, bool v, PowersEnum PowerType, int CurUsage, int Max)
    {
        handler.SetClientSpecialButtonState(v, PowerType, CurUsage, Max);
    }

    public static void SetDriverSpecialButtonState(this ControllerHUDHandler handler, bool v, PowersEnum PowerType, int CurUsage, int Max)
    {
        handler.SetDriverSpecialButtonState(v, PowerType, CurUsage, Max);
    }

    public static void SetBoxPowerButtonState(this ControllerHUDHandler handler, bool v, PowersEnum PowerType, int CurUsage, int Max)
    {
        handler.setBoxPowerButtonState(v, PowerType, CurUsage, Max);
    }

}

public static class PowerUpManagerExtension
{
    public static void SetObjectHidden(this PowerUpManager pManager, bool state)
    {
        pManager.VisibilityState(state);
    }
}

public static class CameraShakeExtension
{
    public static void InitNoiseValue(this Cinemachine.CinemachineBasicMultiChannelPerlin handler, Vector3 offset)
    {
        handler.m_PivotOffset = offset;
    }

    public static async void SetCameraShake(this Cinemachine.CinemachineBasicMultiChannelPerlin handler, float amplitudo, float frequency, float duration)
    {
        WaitForSeconds delay = new WaitForSeconds(duration);
        handler.m_AmplitudeGain = amplitudo;
        handler.m_FrequencyGain = frequency;
        await delay;
        handler.m_AmplitudeGain = 0f;
        handler.m_FrequencyGain = 0f;
    }
}

public static class VFXDrawExtension
{
    public static void DrawVFX(this VFXDraw handler, Vector3 position, string VfxName)
    {
        handler.CallParticles(VfxName, position);
    }
}

public static class ChatGuiExtension
{
    public static void PlaySFX(this ChatGui handler)
    {
        UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke(handler.OnShowChat ? "SFX_ButtonClick_1" : "SFX_ButtonBack_1");
    }
}

public static class CombatAudioHelperExtension
{
    public static void PlayAudio(this CombatAudioHelper helper, string name)
    {
        helper.PlaySound(name);
    }

    public static void PlayAudio(this CombatAudioHelper helper, string name, int index)
    {
        helper.PlaySound(name, index);
    }

    public static void StopAudio(this CombatAudioHelper helper)
    {
        helper.StopSound();
    }

    public static void StopAudio(this CombatAudioHelper helper, int index)
    {
        helper.StopSound(index);
    }
}

#endregion
