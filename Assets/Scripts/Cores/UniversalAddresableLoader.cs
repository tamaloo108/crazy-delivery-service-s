﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Linq;

public class UniversalAddresableLoader : Singleton<UniversalAddresableLoader>
{
    private List<AssetReference> ObjectReferences = new List<AssetReference>();

    private readonly Dictionary<AssetReference, List<GameObject>> _spawnedObjects = new Dictionary<AssetReference, List<GameObject>>();
    private readonly Dictionary<AssetReference, Queue<AddressableAssetQueue>> _queueSpawnReq = new Dictionary<AssetReference, Queue<AddressableAssetQueue>>();
    private readonly Dictionary<AssetReference, AsyncOperationHandle<GameObject>> _asyncOperationHandle = new Dictionary<AssetReference, AsyncOperationHandle<GameObject>>();


    private List<GameObject> TrackedObjects = new List<GameObject>();

    public void AddToTrackedObject(GameObject g)
    {
        if (TrackedObjects.Contains(g)) return;
        TrackedObjects.Add(g);
    }

    public void RemoveFromTrackedObject(GameObject g)
    {
        if (!TrackedObjects.Contains(g)) return;
        TrackedObjects.Remove(g);
    }

    public GameObject GetTrackedObject(GameObject g)
    {
        return TrackedObjects.Find(x => x.Equals(g));
    }

    public GameObject GetTrackedObject(AssetReference r)
    {
        try
        {
            if (_spawnedObjects.ContainsKey(r))
            {
                return _spawnedObjects[r].FirstOrDefault();
            }
            return null;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public void SetObjectState(GameObject m_Object, bool SetActive)
    {
        m_Object.SetActive(SetActive);
    }

    public void ChangePosition(GameObject m_Object, Vector3 position)
    {
        m_Object.transform.position = position;
    }

    public void ChangePosition(GameObject m_Object, GameObject position)
    {
        m_Object.transform.position = position.transform.position;
    }

    public void Spawn(AssetReference asset, Vector3 pos, GameObject objects, Action<GameObject> callback)
    {
        AssetReference _assetreference = null;
        if (ObjectReferences.Contains(asset))
        {
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }
        else
        {
            ObjectReferences.Add(asset);
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            return;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
                SpawnFromLoaderReference(_assetreference, pos, objects, callback);
            else
                EnqueueSpawnForAfterInitialization(_assetreference, pos, objects);
            return;
        }

        LoadAndSpawn(_assetreference, pos, objects, callback);
    }

    public void Spawn(AssetReference asset, Vector3 pos, GameObject objects)
    {
        AssetReference _assetreference = null;
        if (ObjectReferences.Contains(asset))
        {
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }
        else
        {
            ObjectReferences.Add(asset);
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            return;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
                SpawnFromLoaderReference(_assetreference, pos, objects);
            else
                EnqueueSpawnForAfterInitialization(_assetreference, pos, objects);
            return;
        }

        LoadAndSpawn(_assetreference, pos, objects);
    }

    public IEnumerator SpawnIenumerator(AssetReference asset, Vector3 pos, GameObject objects, Func<GameObject, GameObject> callback)
    {
        AssetReference _assetreference = null;
        if (ObjectReferences.Contains(asset))
        {
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }
        else
        {
            ObjectReferences.Add(asset);
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }


        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            yield break;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
                SpawnFromLoaderReference(_assetreference, pos, objects, callback);
            else
                EnqueueSpawnForAfterInitialization(_assetreference, pos, objects);
            yield break;
        }

        LoadAndSpawn(_assetreference, pos, objects, callback);
    }

    public IEnumerator SpawnIenumerator(AssetReference asset, Vector3 pos, GameObject objects, Action<GameObject> callback)
    {
        AssetReference _assetreference = null;
        if (ObjectReferences.Contains(asset))
        {
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }
        else
        {
            ObjectReferences.Add(asset);
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }


        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            yield break;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
                SpawnFromLoaderReference(_assetreference, pos, objects, callback);
            else
                EnqueueSpawnForAfterInitialization(_assetreference, pos, objects);
            yield break;
        }

        LoadAndSpawn(_assetreference, pos, objects, callback);
    }

    public IEnumerator SpawnIenumerator(AssetReference asset, Vector3 pos, GameObject objects)
    {
        AssetReference _assetreference = null;
        if (ObjectReferences.Contains(asset))
        {
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }
        else
        {
            ObjectReferences.Add(asset);
            _assetreference = ObjectReferences.Find(x => x.Equals(asset));
        }


        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            yield break;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
                SpawnFromLoaderReference(_assetreference, pos, objects);
            else
                EnqueueSpawnForAfterInitialization(_assetreference, pos, objects);
            yield break;
        }

        LoadAndSpawn(_assetreference, pos, objects);
    }

    private void LoadAndSpawn(AssetReference assetreference, Vector3 pos, GameObject objects, Action<GameObject> callback)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnFromLoaderReference(assetreference, pos, objects, callback);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnFromLoaderReference(assetreference, pos, objects, callback);
                }
            }
        };
    }

    private void LoadAndSpawn(AssetReference assetreference, Vector3 pos, GameObject objects, Func<GameObject, GameObject> callback)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnFromLoaderReference(assetreference, pos, objects, callback);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnFromLoaderReference(assetreference, pos, objects, callback);
                }
            }
        };
    }

    private void SpawnFromLoaderReference(AssetReference assetreference, Vector3 pos, GameObject objects, Func<GameObject, GameObject> callback)
    {
        assetreference.InstantiateAsync(pos, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (_spawnedObjects.ContainsKey(assetreference) == false)
            {
                _spawnedObjects[assetreference] = new List<GameObject>();
            }

            _spawnedObjects[assetreference].Add(asyncOperationHandle.Result);

            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
            callback(asyncOperationHandle.Result);
        };
    }

    private void SpawnFromLoaderReference(AssetReference assetreference, Vector3 pos, GameObject objects, Action<GameObject> callback)
    {
        assetreference.InstantiateAsync(pos, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (_spawnedObjects.ContainsKey(assetreference) == false)
            {
                _spawnedObjects[assetreference] = new List<GameObject>();
            }

            _spawnedObjects[assetreference].Add(asyncOperationHandle.Result);

            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
            callback(asyncOperationHandle.Result);
        };
    }

    private void LoadAndSpawn(AssetReference assetreference, Vector3 pos, GameObject objects)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnFromLoaderReference(assetreference, pos, objects);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnFromLoaderReference(assetreference, pos, objects);
                }
            }
        };
    }

    private void EnqueueSpawnForAfterInitialization(AssetReference assetreference, Vector3 pos, GameObject objects)
    {
        if (_queueSpawnReq.ContainsKey(assetreference) == false)
        {
            _queueSpawnReq[assetreference] = new Queue<AddressableAssetQueue>();
        }

        _queueSpawnReq[assetreference].Enqueue(new AddressableAssetQueue(pos, objects));
    }

    private void SpawnFromLoaderReference(AssetReference assetreference, Vector3 pos, GameObject objects)
    {
        assetreference.InstantiateAsync(pos, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (_spawnedObjects.ContainsKey(assetreference) == false)
            {
                _spawnedObjects[assetreference] = new List<GameObject>();
            }

            _spawnedObjects[assetreference].Add(asyncOperationHandle.Result);

            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
        };
    }

    private void Notify_Destroyed(AssetReference assetReference, NotifyOnDestroy obj)
    {
        Addressables.ReleaseInstance(obj.gameObject);

        _spawnedObjects[assetReference].Remove(obj.gameObject);
        if (_spawnedObjects[assetReference].Count == 0)
        {
            if (_asyncOperationHandle[assetReference].IsValid())
            {
                Addressables.Release(_asyncOperationHandle[assetReference]);
            }

            _asyncOperationHandle.Remove(assetReference);
        }
    }
}

public class AddressableAssetQueue
{
    public Vector3 position;
    public GameObject parent;

    public AddressableAssetQueue(Vector3 position, GameObject parent)
    {
        this.position = position;
        this.parent = parent;
    }
}
