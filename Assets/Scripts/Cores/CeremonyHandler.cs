﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class CeremonyHandler : MonoBehaviour
{
    [SerializeField] GameObject Podium;
    [SerializeField] AssetReference LoadingHUD;
    [SerializeField] AssetReference InvidualScoreBoardHUD;
    [SerializeField] Transform ScoreboardContentTransform;

    Cinemachine.CinemachineVirtualCamera Camera;
    Cinemachine.CinemachineStateDrivenCamera StateMain;
    WaitForSeconds LoadingDelay = new WaitForSeconds(3f);
    WaitForSeconds MediumDelay = new WaitForSeconds(.17f);
    WaitUntil IsPodiumSceneLoaded;
    WaitUntil IsPodiumSceneReady;
    WaitUntil TryFindCamera;
    Coroutine Gala;
    Coroutine LoadingLoad;
    private WaitForEndOfFrame Delay = new WaitForEndOfFrame();
    private WaitUntil DoneSpawnBoard;
    private GameObject CurrentBoard;

    GameObject MinimapCam;

    private void Awake()
    {
        DoneSpawnBoard = new WaitUntil(() => CurrentBoard != null);
        IsPodiumSceneLoaded = new WaitUntil(() => UniversalSceneLoader.Instance.IsSceneLoaded("Podium"));
        IsPodiumSceneReady = new WaitUntil(() => UniversalSceneLoader.Instance.IsSceneActive("Podium"));
        MinimapCam = GameObject.FindGameObjectWithTag("Camera For Minimap").gameObject;
    }

    void Start()
    {
        TryFindCamera = new WaitUntil(() => Camera != null);

        if (Gala == null)
        {
            Gala = StartCoroutine(ShowStart());
        }
    }

    public void ReturnToLauncher()
    {
        if (LoadingLoad == null)
        {
            LoadingLoad = StartCoroutine(LoadingCour());
        }

    }

    private IEnumerator LoadingCour()
    {
        MinimapCam.SetActive(true);
        UniversalAddresableLoader.Instance.Spawn(LoadingHUD, Vector3.zero, null, OnDoneLoading);
        UniversalHandler.Instance.GetManager().LeaveRoom();
        yield return LoadingDelay;
        //UniversalHandler.Instance.BGScreen.SetActive(true);
        UniversalSceneLoader.Instance.UnloadScene("Podium", OnDoneUnloadScene);
    }

    private void OnDoneLoading(GameObject obj)
    {
        
        StateMain.SetAnimState("Podium", false);
        UniversalSceneLoader.Instance.SetActiveScene("GameLauncher");

        UniversalHandler.Instance.GetManager().SetMatchRoomActive(true);
        StartCoroutine(UniversalHandler.Instance.GetManager().RefreshRoomList());
        //PhotonNetwork.CurrentRoom.IsOpen = true;
        //PhotonNetwork.CurrentRoom.RemovedFromList = false;
        //UniversalHandler.Instance.GetManager().enabled = true;

        // UniversalHandler.Instance.GetManager().SetMatchRoomActive();
        //UniversalHandler.Instance.GetManager().LeaveRoom();

        // UniversalHandler.Instance.GetManager().SetGameStartState(false);
    }

    private void OnDoneUnloadScene()
    {
        UniversalHandler.Instance.GetManager().SetGameStartState(false);
        
    }

    private IEnumerator ShowStart()
    {
        var CurrentScene = UniversalSceneLoader.Instance.GetActiveScene();
        while (!Camera)
        {
            var g = GameObject.FindGameObjectWithTag("Ceremony Camera").GetComponent<Cinemachine.CinemachineVirtualCamera>();
            yield return Delay;
            if (g != null)
            {
                Camera = g;
            }
        }
        yield return TryFindCamera;
        Camera.m_LookAt = Podium.transform;
        Camera.m_Follow = Podium.transform;
        StateMain = Camera.transform.parent.GetComponent<Cinemachine.CinemachineStateDrivenCamera>();
        StateMain.SetAnimState("Podium", true);
        yield return LoadingDelay;
        yield return SpawnPlayerScoreboard();
        UniversalSceneLoader.Instance.UnloadScene(CurrentScene.name, OnDoneUnloadLevelScene);
        yield return IsPodiumSceneReady;
        MinimapCam.SetActive(false);
        yield return new WaitForSeconds(1f);

        Gala = null;
        yield break;
    }

    private IEnumerator SpawnPlayerScoreboard()
    {
        var pList = PhotonNetwork.PlayerList;
        var SortedpList = pList.OrderByDescending(x => x.GetScore()).ToArray();
        UniversalHandler.Instance.GetManager().enabled = true;
        UniversalHandler.Instance.GetManager().SetMatchRoomActive(false);
        for (int i = 0; i < SortedpList.Length; i++)
        {
            yield return MediumDelay;
            UniversalAddresableLoader.Instance.Spawn(InvidualScoreBoardHUD, Vector3.zero, null, DoneScoreBoard);
            yield return DoneSpawnBoard;
            CurrentBoard.transform.SetParent(ScoreboardContentTransform.transform);
            CurrentBoard.transform.localScale = Vector3.one;
            var myName = SortedpList[i].NickName;
            var myScore = SortedpList[i].GetScore();
            var myRank = i + 1;
            CurrentBoard.GetComponent<IndividualScoreboardHandler>().SetScoreboard(myName, myScore.ToString(), myRank.ToString());
            CurrentBoard = null;
        }

        yield break;
    }

    private void DoneScoreBoard(GameObject arg)
    {
        CurrentBoard = arg;
    }

    private void OnDoneUnloadLevelScene()
    {
        UniversalSceneLoader.Instance.SetActiveScene("Podium");
    }
}
