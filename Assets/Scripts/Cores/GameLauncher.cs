﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using CDS.Power;
using System.Linq;
using System.Text.RegularExpressions;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Random = UnityEngine.Random;

public class GameLauncher : MonoBehaviourPunCallbacks, IOnEventCallback, IMatchmakingCallbacks
{
    [SerializeField] AssetReference ConnectingUI;
    [SerializeField] AssetReference MenuHeadersUI;
    [SerializeField] AssetReference MainMenuUI;
    [SerializeField] AssetReference SettingUI;
    [SerializeField] AssetReference QuestUI;
    [SerializeField] AssetReference ShopUI;
    [SerializeField] AssetReference LobbyUI;
    [SerializeField] AssetReference LoadingUI;
    [SerializeField] AssetReference ControllerHUD;
    [SerializeField] AssetReference CreateRoomUI;
    [SerializeField] AssetReference MatchRoomUI;
    [SerializeField] AssetReference LobbyRoomBoardUI;
    [SerializeField] AssetReference RandomMatchMaking;
    [SerializeField] AssetReference QuickMatchOptionUI;
    [SerializeField] AssetReference ConnectionlostUI;
    [SerializeField] AssetReference NameWarningUI;
    [SerializeField] AssetReference ValidatingLoadingUI;
    [SerializeField] AssetReference Characterselection;
    [SerializeField] AssetReference Characterdetail;
    [SerializeField] AssetReference BGpackUI;
    [SerializeField] AssetReference Mainmenupodium;
    [SerializeField] GameObject GameLauncherUI;
    [SerializeField] Camera UICamera;

    GameObject RoomUIObject;
    GameObject CreateRoomObject;
    GameObject MatchRoomObject;

    Coroutine ConnectingCoroutine;
    Coroutine SpawnPlayerBoardCoroutine;
    Coroutine RemoveLauncher;
    Coroutine SpawnRoomListCoroutine;
    Coroutine LoadingCoroutine;
    Coroutine randommatchcourotine;
    Coroutine validatingcourotine;
    Coroutine validatingleave;
    Coroutine waitjoinprocess;
    WaitUntil WaitForleaveroom;
    WaitUntil SceneIsActive;
    WaitUntil IsConnected;
    WaitUntil WaitForLobbyItem;
    WaitUntil WaitForRoomloaded;
    WaitUntil WaitForRoomItem;
    WaitUntil WaitForLoadingInstance;
    WaitUntil WaitForDoneLoadLevel;
    WaitUntil WaitForReconnecting;
    WaitUntil WaitForPlayerInRoom;
    WaitForSeconds LoadingDelay;
    WaitForEndOfFrame FastDelay;
    GameObject ConnectingInstance;
    GameObject LoadingInstance;
    RoomOptions UserRoomOptions;
    TMP_Text _MaxPlayerHold;
    TMP_Text _userID;
    LobbyHandler _LobbyHandler;
    PlayerLobbyItem _CurLobbyItem;
    RoomListHandler _CurRoomListHandler;
    LevelManager _LevelManager;
    ChatGui _chatgui;
    NamePickGui _NamePickGui;
    Transform LobbyListParent;


    Dictionary<int, GameObject> SpawnedPlayerBoard = new Dictionary<int, GameObject>();
    Dictionary<int, GameObject> SpawnedRoomBoard = new Dictionary<int, GameObject>();

    bool RoomFull = false;
    bool DoneLoadLevel;
    bool initcourier = false;
    bool RefreshLobby = false;
    bool reconnectsucces = false;
    bool reconnectproccess = false;
    bool GameStart = false;
    bool randommatch = false;
    bool practicemode = false;
    bool asd = false;
    bool selectcour = false;
    string playername;
    string decisioncourier;
    string CurrentNickName;
    string CurrentRoomName;
    string mapselected;
    string mapload;
    string interneterrormessage;
    string usernameRegisterField;
    string emailRegisterField;
    string passwordRegisterField;
    string passwordRegisterVerifyField;
    string s = "Room Match311";
    string[] lobbyoption;
    string emailLoginField;
    string passwordLoginField;
    string loginwarningtemp;
    const string MAP_KEY = "C1";
    int CurrentMaxPlayer;
    int maxrandomplayer;
    int currentrandomroom = 0;
    int firstwarningloop = 0;
    int first = 0;
    int firstroomoption = 0;
    int mapindex;
    int randommapindex;
    int test;
    int fixPlayerID;
    List<string> mapoption = new List<string>();

    Canvas hideroom;
    GameObject SpawnedCreateRoom;
    GameObject courierpreview;
    GameObject podiumonmenu;
    GameObject driveronmenu;
    GameObject driveronlobby;
    GameObject RandomMatchUI;
    GameObject SpawnedCreateRoomOption;
    GameObject QuickMatchOption;
    GameObject SpawnedMatchRoom;
    GameObject LoadingInstances;
    GameObject Spawnedheadersmenu;
    GameObject SpawnedGarage;
    GameObject SpawnedShop;
    GameObject Spawnedcharacterselection;
    GameObject Spawnedcharacterdetail;
    GameObject SpawnedQuest;
    GameObject SpawnedSetting;
    GameObject GarageHeaders;
    GameObject reconnectingpanel;
    GameObject disconectpanel;
    GameObject LobbyHeaders;
    GameObject RoomHeaders;
    GameObject connectionlostpanel;
    GameObject namewarningPanel;
    GameObject BGpack;
    GameObject validatingUI;
    GameObject loginUI;
    GameObject registerUI;
    GameObject BGmainmenuUI;
    GameObject BGLobbyUI;
    GameObject BGcharselectionUI;
    GameObject courierselectioninmenuheaders;
    GameObject courierselectioniroomheaders;
    GameObject courierdetailheaders;
    Transform podiummenu;
    Transform podiumlobby;
    private int PlayerCount;
    TMP_Text waitingstatus;
    TMP_Text playerdetail;
    TMP_Text Disconectcause;
    TMP_Text findmatchcountdown;
    TMP_Text warningLoginText;
    TMP_Text loginwarningcause;
    TMP_Text confirmLoginText;
    TMP_Text warningRegisterText;
    TMP_Dropdown mapselection;
    Animator animname;

    // database
    string IDplayer;
    string Nicknameplayer;
    int characterselected;
    int level;
    float exp;
    float score;
    int numberofmatch;
    int custommatchplayed;
    int quickmatchplayed;
    int teammatchplayed;
    int deatchmatchplayed;
    int numberofmvp;
    int numberofwins;
    int numberoflosing;
    int premangameplayed;
    int mbahgameplayed;
    int aliengameplayed;
    int[] IDplayertemp = new int[1];

    public List<RoomInfo> CachedRoomList { get; private set; }
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser User;
    public DatabaseReference DBreference;

    #region Unity Methods

    private void Update()
    {
        Debug.Log("in room ? : " + PhotonNetwork.InRoom);
        Debug.Log("found ? : " + asd);
    }

    private void Awake()
    {
        //tesambilnama();
        FastDelay = new WaitForEndOfFrame();
        LoadingDelay = new WaitForSeconds(2f);
        IsConnected = new WaitUntil(() => PhotonNetwork.IsConnected);
        WaitForLobbyItem = new WaitUntil(() => _CurLobbyItem != null);
        WaitForRoomloaded = new WaitUntil(() => _LobbyHandler != null);
        WaitForRoomItem = new WaitUntil(() => _CurRoomListHandler != null);
        WaitForLoadingInstance = new WaitUntil(() => LoadingInstances != null);
        WaitForDoneLoadLevel = new WaitUntil(() => DoneLoadLevel);
        WaitForPlayerInRoom = new WaitUntil(() => PhotonNetwork.InRoom);
        WaitForleaveroom = new WaitUntil(() => !PhotonNetwork.InRoom);
        SceneIsActive = new WaitUntil(() => UniversalSceneLoader.Instance.IsSceneActive(PhotonNetwork.PlayerList[0].CustomProperties[MAP_KEY].ToString())); // we'll rework this if multiple stage implemented.
        WaitForReconnecting = new WaitUntil(() => reconnectsucces);
        PowerFactory.GetPower(PowersEnum.None);

        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //If they are avalible Initialize Firebase
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void Start()
    {
        UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
        loginpagemanager();
        //StartCoroutine(PlayerIDgenerator());
        UniversalHandler.Instance.SetManager(this);
        _NamePickGui = gameObject.GetComponent<NamePickGui>();
        // PhotonNetwork.AutomaticallySyncScene = true;
        _chatgui = FindObjectOfType<ChatGui>();
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
    }

    private string GetSceneNameFromBuildIndex(int index)
    {
        string scenePath = SceneUtility.GetScenePathByBuildIndex(index);
        string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePath);

        return sceneName;
    }

    public void PlayChatSFX()
    {
        _chatgui.PlaySFX();
    }

    public void tesambilnama()
    {
        int b = SceneManager.sceneCountInBuildSettings;
        Debug.Log("jumlah scene : " + b);
        for (int a = 4; a <= b - 1; a++)
        {
            Debug.Log("scene level : " + GetSceneNameFromBuildIndex(a));
        }
        //Debug.Log("selesai load scene");

    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        reconnectproccess = true;
        connectionlostpanel.SetActive(true);
        disconectpanel.SetActive(true);
        //connectionlostpanel.SetActive(false);
        Debug.Log("connection lost cause : " + cause);
        Disconectcause.text = cause.ToString();
    }

    private void disconectdialog()
    {
        if (!PhotonNetwork.IsConnected && !PhotonNetwork.InRoom && !interneterrormessage.Equals("No internet Access"))
        {
            PhotonNetwork.Reconnect();
            StartCoroutine(reconnectloading());
            disconectpanel.SetActive(false);
        }
        else
        {
            disconectpanel.SetActive(false);
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                disconectpanel.SetActive(true);
            }
            else
            {
                disconectpanel.SetActive(false);
                connectionlostpanel.SetActive(false);
            }
        }
    }

    IEnumerator reconnectloading()
    {
        reconnectingpanel.SetActive(true);
        Debug.Log("memulai loading reconnect");
        yield return WaitForReconnecting;
        reconnectingpanel.SetActive(false);
        connectionlostpanel.SetActive(false);
        Debug.Log("done loading reconnecting");
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        //_LobbyHandler.UnsubscribeGame(OnStartTheGame);
    }

    public void SetLobbyActive()
    {
        if (SpawnedMatchRoom)
        {
            BGLobbyUI.SetActive(true);
            hideroom.renderMode = RenderMode.ScreenSpaceOverlay;
            activatepodiumlobby();
            SpawnedCreateRoom.SetActive(false);
            SpawnedMatchRoom.gameObject.SetActive(true);
        }
    }


    public void DestroyLobbyList()
    {

        foreach (var item in SpawnedRoomBoard)
        {
            Destroy(item.Value);
        }

        SpawnedRoomBoard = new Dictionary<int, GameObject>();
    }

    public IEnumerator RefreshRoomList()
    {
        DestroyLobbyList();
        RefreshLobby = true;
        while (RefreshLobby)
        {
            yield return FastDelay;
            yield return RefreshRoomLists(CachedRoomList);
        }
        yield break;
    }

    public void SetMatchRoomActive(bool state)
    {
        if (SpawnedCreateRoom)
        {
            Debug.Log("state : " + state);
            BGLobbyUI.SetActive(state);
            SpawnedCreateRoom.SetActive(state);
            if (state)
            {
                Spawnedheadersmenu.SetActive(true);
                LobbyHeaders.SetActive(true);
                RoomHeaders.SetActive(false);
                GarageHeaders.SetActive(false);
            }
        }
    }

    public void LoginToGame()
    {
        UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1");

        if (!PhotonNetwork.IsConnected && (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork
            || Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork))
        {
            if (ConnectingCoroutine == null && CurrentNickName.Length <= 14)
            {
                PhotonNetwork.NetworkingClient.LoadBalancingPeer.MaximumTransferUnit = 500;
                PhotonNetwork.NetworkingClient.LoadBalancingPeer.SentCountAllowance = 7;
                PhotonNetwork.NetworkingClient.LoadBalancingPeer.QuickResendAttempts = 3;
                PhotonNetwork.NetworkingClient.LoadBalancingPeer.CrcEnabled = true;
                PhotonNetwork.NetworkingClient.LoadBalancingPeer.DisconnectTimeout = 10000;
                AuthenticationValues authval = new AuthenticationValues(IDplayer);
                PhotonNetwork.AuthValues = authval;
                PhotonNetwork.ConnectUsingSettings();
                Debug.Log("photon netwrok name length " + PhotonNetwork.NickName.Length);
                //PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.NickName = Nicknameplayer;
                _NamePickGui.StartChat();
                ConnectingCoroutine = StartCoroutine(StartConnectingUI());
            }
            else
            {
                UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
            }
        }
        else if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            interneterrormessage = "No internet Access";
            UniversalAddresableLoader.Instance.Spawn(ConnectionlostUI, Vector3.zero, null, Ondonespawnpanellost);
            Debug.Log("Error. Check internet connection!");
        }
    }

    public override void OnFriendListUpdate(List<FriendInfo> friendList)
    {
        base.OnFriendListUpdate(friendList);

        foreach (FriendInfo info in friendList)
        {
            Debug.Log("friend info received " + info.UserId + "is online? " + info.IsOnline);
        }
    }

    private void namewarning(GameObject obj)
    {
        if (namewarningPanel == null)
        {
            namewarningPanel = obj;
        }

        var Texts = obj.GetComponentsInChildren<TMP_Text>();

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "loginpagecondition":
                    loginwarningcause = Texts[i];
                    continue;
            }
        }

        if (firstwarningloop != 1)
        {
            firstwarningloop += 1;
            namewarningPanel.SetActive(false);
            Destroy(namewarningPanel);
        }

        else
        {
            namewarningPanel.SetActive(true);
            loginwarningcause.text = loginwarningtemp;
            Destroy(namewarningPanel, 3f);
        }
    }

    public void QuickStart()
    {
        UniversalAddresableLoader.Instance.Spawn(QuickMatchOptionUI, Vector3.zero, null, quickmatchoption);

    }

    public void quickmatchoption(GameObject obj)
    {
        QuickMatchOption = obj;
        // SpawnedCreateRoom.SetActive(false);
        var button = obj.GetComponentsInChildren<Button>();

        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "Practice":
                    button[i].onClick.AddListener(() => createpractice());
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "2 Driver":
                    button[i].onClick.AddListener(() => ondonerandomsetting(2));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "6 Driver":
                    button[i].onClick.AddListener(() => ondonerandomsetting(6));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "12 Driver":
                    button[i].onClick.AddListener(() => ondonerandomsetting(12));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "Leave Room":
                    button[i].onClick.AddListener(() => cancelmakequickroom(obj));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
                    continue;
            }
        }
    }

    public void cancelmakequickroom(GameObject obj)
    {
        BGLobbyUI.SetActive(true);
        UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1");

        SpawnedCreateRoom.SetActive(true);
        Destroy(obj);

    }

    public void ondonerandomsetting(int a)
    {
        maxrandomplayer = a;
        randommode(a);
        UniversalAddresableLoader.Instance.Spawn(RandomMatchMaking, Vector3.zero, null, OnDoneSpawnRandomMatchUI);
    }


    private void OnMatchMakingProcess(GameObject obj)
    {
        BGLobbyUI.SetActive(false);
        SpawnedCreateRoom.SetActive(false);
        randommatch = true;
        PhotonNetwork.JoinRandomRoom();
    }

    public void SetCurrentRoomName(string name)
    {
        CurrentRoomName = name;
    }

    public void SetCurrentMaxPlayer(float value)
    {
        _MaxPlayerHold.text = ((int)value).ToString();
        CurrentMaxPlayer = (int)value;
    }

    public void SetCurrentInputName(string name)
    {
        CurrentNickName = name;
    }

    public void SetCustomRoomStart()
    {

        // Debug.Log("room option sudah ada, proses aktivasi");
        SpawnedCreateRoomOption.SetActive(true);

    }

    private void OnDoneSpawnCreateRoomUI(GameObject obj)
    {
        CreateRoomObject = obj;
        //SpawnedCreateRoom.SetActive(false);
        OnDoneSpawnRoomUI(obj);
    }

    private IEnumerator StartConnectingUI()
    {
        GameLauncherUI.SetActive(false);
        UniversalAddresableLoader.Instance.Spawn(ConnectingUI, Vector3.zero, null, OnDoneSpawnConnectingUI);
        UniversalAddresableLoader.Instance.Spawn(BGpackUI, Vector3.zero, null, OndonespawnBGpack);
        UniversalAddresableLoader.Instance.Spawn(Mainmenupodium, Vector3.zero, null, onspawnedpodium);
        yield return IsConnected;
        UniversalAddresableLoader.Instance.Spawn(MenuHeadersUI, Vector3.zero, null, Ondonespawnheadersmenu);
        UniversalAddresableLoader.Instance.Spawn(MainMenuUI, Vector3.zero, null, ondonespawngarage);
        UniversalAddresableLoader.Instance.Spawn(ShopUI, Vector3.zero, null, ondonespawnshop);
        UniversalAddresableLoader.Instance.Spawn(QuestUI, Vector3.zero, null, ondonespawnquest);
        UniversalAddresableLoader.Instance.Spawn(LobbyUI, Vector3.zero, null, OnDoneSpawnRoomUI);
        UniversalAddresableLoader.Instance.Spawn(CreateRoomUI, Vector3.zero, null, OnDoneSpawnCreateOption);
        UniversalAddresableLoader.Instance.Spawn(Characterselection, Vector3.zero, null, Ondonespawnpanelcharacterselection);
        UniversalAddresableLoader.Instance.Spawn(Characterdetail, Vector3.zero, null, Ondonespawnpanelcharacterdetail);
        transformpodium();
        // activatepodiumonmenu();
        initcourier = true;
        afterselectcharacter(characterselected.ToString());
        interneterrormessage = "connected";

        if (connectionlostpanel == null)
        {
            UniversalAddresableLoader.Instance.Spawn(ConnectionlostUI, Vector3.zero, null, Ondonespawnpanellost);
        }

        ConnectingCoroutine = null;
        yield break;
    }

    private void onspawnedpodium(GameObject obj)
    {
        Debug.Log("setpodium");
        if (podiumonmenu == null)
        {
            podiumonmenu = obj;
        }

        var panel = obj.GetComponentsInChildren<Transform>();

        for (int i = 0; i < panel.Length; i++)
        {
            switch (panel[i].tag)
            {
                case "BGmainmenu":
                    driveronmenu = panel[i].gameObject;
                    driveronmenu.SetActive(false);
                    continue;
                case "BGlobby":
                    driveronlobby = panel[i].gameObject;
                    driveronlobby.SetActive(false);
                    continue;
            }
            if (driveronlobby != null && driveronmenu != null)
            {
                Debug.Log("transform change");
                activatepodiumonmenu();
            }
        }
    }

    private void activatepodiumonmenu()
    {
        podiumonmenu.SetActive(true);
        podiumonmenu.transform.SetParent(podiummenu.transform);
        podiumonmenu.transform.localPosition = Vector3.zero;
        podiumonmenu.transform.localEulerAngles = Vector3.zero;
        podiumonmenu.transform.localScale = Vector3.one;
        driveronmenu.SetActive(true);
        driveronlobby.SetActive(false);
        courierpreview.transform.SetParent(driveronmenu.transform);
        courierpreview.transform.localPosition = Vector3.zero;
        courierpreview.transform.localEulerAngles = Vector3.zero;
        courierpreview.transform.localScale = Vector3.one;
    }

    private void activatepodiumlobby()
    {
        podiumonmenu.SetActive(true);
        podiumonmenu.transform.SetParent(podiumlobby.transform);
        podiumonmenu.transform.localPosition = Vector3.zero;
        podiumonmenu.transform.localEulerAngles = Vector3.zero;
        podiumonmenu.transform.localScale = Vector3.one;
        driveronmenu.SetActive(false);
        driveronlobby.SetActive(true);
        courierpreview.transform.SetParent(driveronlobby.transform);
        courierpreview.transform.localPosition = Vector3.zero;
        courierpreview.transform.localEulerAngles = Vector3.zero;
        courierpreview.transform.localScale = Vector3.one;
    }


    private void transformpodium()
    {

        var panel = UICamera.GetComponentsInChildren<Transform>();

        for (int i = 0; i < panel.Length; i++)
        {
            switch (panel[i].tag)
            {
                case "BGmainmenu":
                    podiummenu = panel[i];
                    continue;
                case "BGlobby":
                    podiumlobby = panel[i];
                    continue;
            }
        }
    }

    private void OndonespawnBGpack(GameObject obj)
    {
        if (BGpack == null)
        {
            BGpack = obj;
        }

        Canvas BGset = BGpack.GetComponent<Canvas>();
        BGset.worldCamera = UICamera;
        BGset.planeDistance = 400;

        var panel = obj.GetComponentsInChildren<Transform>();

        for (int i = 0; i < panel.Length; i++)
        {
            switch (panel[i].tag)
            {
                case "BGmainmenu":
                    BGmainmenuUI = panel[i].gameObject;
                    continue;
                case "BGlobby":
                    BGLobbyUI = panel[i].gameObject;
                    continue;
                case "BGCharacterselection":
                    BGcharselectionUI = panel[i].gameObject;
                    continue;
            }
            if (i == panel.Length)
            {
                BGmainmenuUI.SetActive(false);
                BGLobbyUI.SetActive(false);
                BGcharselectionUI.SetActive(false);
            }
        }
    }

    private void Ondonespawnpanelcharacterselection(GameObject obj)
    {

        if (Spawnedcharacterselection == null)
        {
            Spawnedcharacterselection = obj;
        }

        var button = obj.GetComponentsInChildren<Button>();

        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "freemanbutton":
                    button[i].onClick.AddListener(() => afterselectcharacter("freeman"));
                    UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1");
                    continue;
                case "mbahkekokbutton":
                    button[i].onClick.AddListener(() => afterselectcharacter("mbahkekok"));
                    UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1");
                    continue;
                case "taximanbutton":
                    button[i].onClick.AddListener(() => afterselectcharacter("taximan"));
                    UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1");
                    continue;
            }
        }
        BGcharselectionUI.SetActive(false);
        Spawnedcharacterselection.SetActive(false);
    }

    private void Ondonespawnpanelcharacterdetail(GameObject obj)
    {
        var code = UniversalHandler.Instance.getcharactercode();

        if (Spawnedcharacterdetail == null)
        {
            Spawnedcharacterdetail = obj;
        }
        
        var button = obj.GetComponentsInChildren<Button>();
        var Texts = obj.GetComponentsInChildren<TMP_Text>();
        var panel = obj.GetComponentsInChildren<Transform>();



        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "trycourier":
                    //button[i].onClick.AddListener(() => createpractice());
                    continue;
                case "selectcourier":
                    //button[i].onClick.AddListener(() => afterselectcharacter(decisioncourier));
                    continue;
            }
        }

        for (int i = 0; i < panel.Length; i++)
        {
            switch (panel[i].tag)
            {
                case "disconectpanel":
                    disconectpanel = panel[i].gameObject;
                    continue;
                case "reconnectUI":
                    reconnectingpanel = panel[i].gameObject;
                    reconnectingpanel.SetActive(false);
                    continue;
            }
        }

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "DisconnectCause":
                    Disconectcause = Texts[i];
                    if (interneterrormessage.Equals("No internet Access"))
                    {
                        Disconectcause.text = interneterrormessage;
                    }

                    continue;
            }
        }
        activatepodiumonmenu();
        BGcharselectionUI.SetActive(false);
        Spawnedcharacterdetail.SetActive(false);
    }

    private void afterselectcharacter(string courier)
    {
        decisioncourier = courier;
        if (initcourier)
        {
            Debug.Log("if condition");
            initcourier = false;
            Debug.Log("initiate courier selection");
            UniversalHandler.Instance.setcharactercode(int.Parse(courier));
            courierpreview = Instantiate(Resources.Load("PhotonResources/MainMenuPodium/Bike 1") as GameObject);
        }
        else
        {
            Debug.Log("else condition");
            if (PhotonNetwork.InRoom)
            {
                hideroom.renderMode = RenderMode.WorldSpace;
            }

            BGcharselectionUI.SetActive(true);
            Spawnedcharacterdetail.SetActive(true);
            Spawnedcharacterselection.SetActive(false);
            courierdetailheaders.SetActive(true);
            courierselectioninmenuheaders.SetActive(false);
            var charDetail = Spawnedcharacterdetail.GetComponent<CharacterDetailHandler>();

            if (courier.Equals("freeman"))
            {
                Destroy(courierpreview);
                var a = courierpreview;
                courierpreview = Instantiate(Resources.Load("PhotonResources/MainMenuPodium/Bike 1") as GameObject);
                activatepodiumonmenu();
                Debug.Log("preman selected");
                UniversalHandler.Instance.setcharactercode(UniversalHandler.SELECT_FREEMAN);
                StartCoroutine(Updatecourierselected(UniversalHandler.SELECT_FREEMAN.ToString()));        
            }

            else if (courier.Equals("mbahkekok"))
            {
                Destroy(courierpreview);
                var a = courierpreview;
                courierpreview = Instantiate(Resources.Load("PhotonResources/MainMenuPodium/Bike 2") as GameObject);
                activatepodiumonmenu();
                Debug.Log("mbah selected");
                UniversalHandler.Instance.setcharactercode(UniversalHandler.SELECT_MBAH_KEKOK);
                StartCoroutine(Updatecourierselected(UniversalHandler.SELECT_MBAH_KEKOK.ToString()));
            }

            else if (courier.Equals("taximan"))
            {
                Destroy(courierpreview);
                var a = courierpreview;
                courierpreview = Instantiate(Resources.Load("PhotonResources/MainMenuPodium/Bike 3") as GameObject);
                activatepodiumonmenu();
                Debug.Log("mbah selected");
                UniversalHandler.Instance.setcharactercode(UniversalHandler.SELECT_TAXIMAN);
                StartCoroutine(Updatecourierselected(UniversalHandler.SELECT_TAXIMAN.ToString()));
            }

            charDetail.InitParameter();
        }
    }

    private void accseleksicourier() 
    {
        int code = UniversalHandler.Instance.getcharactercode();
        if (code == UniversalHandler.SELECT_FREEMAN) 
        {
            Destroy(courierpreview);
            var a = courierpreview;
            courierpreview = Instantiate(Resources.Load(UniversalHandler.freeman) as GameObject);
            activatepodiumonmenu();
            Debug.Log("preman selected");
        }
        else if (code == UniversalHandler.SELECT_MBAH_KEKOK)
        {
            Destroy(courierpreview);
            var a = courierpreview;
            courierpreview = Instantiate(Resources.Load(UniversalHandler.mbah) as GameObject);
            activatepodiumonmenu();
            Debug.Log("mbah selected");
        }
    }

    private void afterselectchaaracter(string courier, bool selected)
    {
        decisioncourier = courier;
        if (initcourier)
        {
            Debug.Log("if condition");
            initcourier = false;
            Debug.Log("initiate courier selection");
            UniversalHandler.Instance.setcharactercode(int.Parse(courier));
            courierpreview = Instantiate(Resources.Load("PhotonResources/MainMenuPodium/Bike 1") as GameObject);
        }
        else
        {
            Debug.Log("else condition");
            if (PhotonNetwork.InRoom)
            {
                hideroom.renderMode = RenderMode.WorldSpace;
            }
            BGcharselectionUI.SetActive(true);
            Spawnedcharacterdetail.SetActive(true);
            Spawnedcharacterselection.SetActive(false);
            courierdetailheaders.SetActive(true);
            courierselectioninmenuheaders.SetActive(false);

            if (courier.Equals("freeman"))
            {
                Debug.Log("view freman");

                var b = driveronmenu.GetComponentsInChildren<Transform>();
                var c = driveronlobby.GetComponentsInChildren<Transform>();

                for (int i = 1; i < b.Length; i++)
                {
                    if (b[i].gameObject == null)
                    {
                        break;
                    }
                    else
                    {
                        Destroy(b[i].gameObject);
                    }
                }

                for (int i = 1; i <= c.Length; i++)
                {
                    if (c[i].gameObject == null)
                    {
                        break;
                    }
                    else
                    {
                        Destroy(c[i].gameObject);
                    }
                }
                //Destroy(courierpreview);
                var a = courierpreview;
                courierpreview = Instantiate(Resources.Load("PhotonResources/MainMenuPodium/Bike 1") as GameObject);
                activatepodiumonmenu();

                if (selected)
                {
                    Debug.Log("preman selected");
                    UniversalHandler.Instance.setcharactercode(501);
                    StartCoroutine(Updatecourierselected(501.ToString()));
                    selected = false;
                }
                else
                {
                    Debug.Log("just view freman" + a.ToString());
                    courierpreview = a;
                }

            }
            else if (courier.Equals("mbahkekok"))
            {
                Debug.Log("view mbah");

                var b = driveronmenu.GetComponentsInChildren<Transform>();
                var c = driveronlobby.GetComponentsInChildren<Transform>();

                for (int i = 1; i < b.Length; i++)
                {
                    if (b[i].gameObject == null)
                    {
                        break;
                    }
                    else
                    {
                        Destroy(b[i].gameObject);
                    }
                }

                for (int i = 1; i <= c.Length; i++)
                {
                    if (c[i].gameObject == null)
                    {
                        break;
                    }
                    else
                    {
                        Destroy(c[i].gameObject);
                    }
                }

                //Destroy(courierpreview);
                var a = courierpreview;
                courierpreview = Instantiate(Resources.Load("PhotonResources/MainMenuPodium/Bike 2") as GameObject);
                activatepodiumonmenu();

                if (selected)
                {
                    Debug.Log("mbah selected");
                    UniversalHandler.Instance.setcharactercode(502);
                    StartCoroutine(Updatecourierselected(502.ToString()));
                    selected = false;
                }
                else
                {
                    Debug.Log("just view mbah" + a.ToString());
                    courierpreview = a;
                }
            }
        }
    }

    private void Ondonespawnpanellost(GameObject obj)
    {
        if (connectionlostpanel == null)
        {
            Debug.Log("inisialisasi connectionlostpanel");
            connectionlostpanel = obj;
        }

        var button = obj.GetComponentsInChildren<Button>();
        var Texts = obj.GetComponentsInChildren<TMP_Text>();
        var panel = obj.GetComponentsInChildren<Transform>();

        for (int i = 0; i < panel.Length; i++)
        {
            switch (panel[i].tag)
            {
                case "disconectpanel":
                    disconectpanel = panel[i].gameObject;
                    continue;
                case "reconnectUI":
                    reconnectingpanel = panel[i].gameObject;
                    reconnectingpanel.SetActive(false);
                    continue;
            }
        }

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "DisconnectCause":
                    Disconectcause = Texts[i];
                    if (interneterrormessage.Equals("No internet Access"))
                    {
                        Disconectcause.text = interneterrormessage;
                    }
                    continue;
            }
        }

        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "Retry":
                    button[i].onClick.AddListener(() => disconectdialog());
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "Quit":
                    button[i].onClick.AddListener(() => menumanage("quest"));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
            }
        }

        if (!interneterrormessage.Equals("No internet Access"))
        {
            connectionlostpanel.SetActive(false);
        }
    }

    private void Ondonespawnheadersmenu(GameObject obj)
    {

        if (Spawnedheadersmenu == null)
        {
            Spawnedheadersmenu = obj;
        }

        var button = obj.GetComponentsInChildren<Button>();
        var headers = obj.GetComponentsInChildren<Transform>();

        for (int i = 0; i < headers.Length; i++)
        {
            switch (headers[i].tag)
            {
                case "Garageheaders":
                    GarageHeaders = headers[i].gameObject;
                    continue;
                case "LobbyHeaders":
                    LobbyHeaders = headers[i].gameObject;
                    LobbyHeaders.SetActive(false);
                    continue;
                case "RoomHeaders":
                    RoomHeaders = headers[i].gameObject;
                    RoomHeaders.SetActive(false);
                    continue;
                case "courierselectioninmenu":
                    courierselectioninmenuheaders = headers[i].gameObject;
                    courierselectioninmenuheaders.SetActive(false);
                    continue;
                case "courierselctioninroom":
                    courierselectioniroomheaders = headers[i].gameObject;
                    asd = true;
                    //courierselectioniroomheaders.SetActive(false);
                    continue;
                case "characterdetail":
                    courierdetailheaders = headers[i].gameObject;
                    courierdetailheaders.SetActive(false);
                    courierselectioniroomheaders.SetActive(false);
                    continue;
            }
        }

        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "Garage":
                    button[i].onClick.AddListener(() => menumanage("garage"));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "Quest":
                    button[i].onClick.AddListener(() => menumanage("quest"));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "Shop":
                    button[i].onClick.AddListener(() => menumanage("shop"));
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "Setting":
                    //button[i].onClick.AddListener(() => menumanage("setting"));
                    continue;
                case "Backtogarage":
                    button[i].onClick.AddListener(() => backtogarage());
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
                    continue;
                case "Backtolobby":
                    button[i].onClick.AddListener(() => LeaveRoom());
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
                    continue;
                case "Courierselectiontogarage":
                    button[i].onClick.AddListener(() => backtogarage());
                    continue;
                case "courierselectiontoroom":
                    button[i].onClick.AddListener(() => fromselectiontoroom());
                    continue;
                case "courierdetailtoselection":
                    button[i].onClick.AddListener(() => fromchardetailbacktoselection());
                    continue;
            }
        }
    }

    private void fromchardetailbacktoselection()
    {
        Spawnedcharacterdetail.SetActive(false);
        BGcharselectionUI.SetActive(false);
        Spawnedcharacterselection.SetActive(true);
        courierdetailheaders.SetActive(false);
        if (PhotonNetwork.InRoom)
        {
            Debug.Log("didalam room");
            activatepodiumlobby();
            hideroom.renderMode = RenderMode.ScreenSpaceOverlay;
            courierselectioniroomheaders.SetActive(true);
        }
        else
        {
            Debug.Log("diluar room");
            courierselectioninmenuheaders.SetActive(true);
        }
    }

    private void fromselectiontoroom()
    {
        Debug.Log("back to room");
        BGcharselectionUI.SetActive(false);
        Spawnedcharacterselection.SetActive(false);
        courierselectioniroomheaders.SetActive(false);
        RoomHeaders.SetActive(true);
    }

    private void ondonespawnsetting(GameObject obj)
    {
        if (SpawnedSetting == null)
        {
            SpawnedSetting = obj;
        }
        SpawnedSetting.SetActive(false);
    }

    private void ondonespawnshop(GameObject obj)
    {
        if (SpawnedShop == null)
        {
            SpawnedShop = obj;
        }
        SpawnedShop.SetActive(false);
    }

    private void ondonespawnquest(GameObject obj)
    {
        if (SpawnedQuest == null)
        {
            SpawnedQuest = obj;
        }
        SpawnedQuest.SetActive(false);
    }

    private void menumanage(string code)
    {
        if (code == "garage")
        {
            Debug.Log("show garage");
            respawngarage();
            //SpawnedGarage.SetActive(true);
            SpawnedQuest.SetActive(false);
            // SpawnedSetting.SetActive(false);
            SpawnedShop.SetActive(false);
        }
        else if (code == "shop")
        {
            Debug.Log("show shop");
            SpawnedGarage.SetActive(false);
            SpawnedQuest.SetActive(false);
            // SpawnedSetting.SetActive(false);
            SpawnedShop.SetActive(true);
        }
        else if (code == "quest")
        {
            Debug.Log("show quest");
            SpawnedGarage.SetActive(false);
            SpawnedQuest.SetActive(true);
            //  SpawnedSetting.SetActive(false);
            SpawnedShop.SetActive(false);
        }
        else if (code == "setting")
        {
            //Debug.Log("show setting");
            //SpawnedGarage.SetActive(false);
            //SpawnedQuest.SetActive(false);
            //SpawnedSetting.SetActive(true);
            //SpawnedShop.SetActive(false);
        }
    }

    private void ondonespawngarage(GameObject obj)
    {

        if (SpawnedGarage == null)
        {
            SpawnedGarage = obj;
        }

        //Canvas mainmenucanvas = SpawnedGarage.GetComponent<Canvas>();
        //mainmenucanvas.worldCamera = UICamera;
        //mainmenucanvas.planeDistance = 400;

        var button = obj.GetComponentsInChildren<Button>();
        var Texts = obj.GetComponentsInChildren<TMP_Text>();
        var slider = obj.GetComponentsInChildren<ScrollRect>();

        for (int i = 0; i < slider.Length; i++)
        {
            switch (slider[i].tag)
            {
                case "Slider Name":
                    animname = slider[i].GetComponent<Animator>();
                    continue;
            }
        }

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "Player Name":
                    Texts[i].text = PhotonNetwork.NickName;
                    playername = Texts[i].text;

                    if (playername.Length < 14)
                    {
                        animname.SetBool("teks panjang", false);
                        //Debug.Log("teks pendek" + playername);
                    }
                    else if (playername.Length >= 14)
                    {
                        animname.SetBool("teks panjang", true);
                        //  Debug.Log("teks panjang" + playername);
                    }
                    else
                    {
                        //    Debug.Log("nama 14 huruf");
                    }

                    continue;
            }
        }

        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "Play":
                    button[i].onClick.AddListener(() => playgame());
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
                case "Courier":
                    button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    button[i].onClick.AddListener(() => menutocharselection());
                    continue;
            }
        }


    }

    private void menutocharselection()
    {
        BGcharselectionUI.SetActive(true);
        Spawnedcharacterselection.SetActive(true);
        BGmainmenuUI.SetActive(false);
        SpawnedGarage.SetActive(false);
        courierselectioninmenuheaders.SetActive(true);
        GarageHeaders.SetActive(false);
    }

    private void backtogarage()
    {
        BGcharselectionUI.SetActive(false);
        Spawnedcharacterselection.SetActive(false);
        LobbyHeaders.SetActive(false);
        GarageHeaders.SetActive(true);
        respawngarage();
        //SpawnedGarage.SetActive(true);
        BGLobbyUI.SetActive(false);
        SpawnedCreateRoom.SetActive(false);
    }

    private void respawngarage()
    {
        activatepodiumonmenu();
        BGmainmenuUI.SetActive(true);
        SpawnedGarage.SetActive(true);
        if (playername.Length < 14)
        {
            animname.SetBool("teks panjang", false);
            //  Debug.Log("teks pendek"+playername);
        }
        else if (playername.Length >= 14)
        {
            animname.SetBool("teks panjang", true);
            //Debug.Log("teks panjang" + playername);
        }
        else
        {
            //Debug.Log("nama 14 huruf");
        }
    }

    private void playgame()
    {
        if (SpawnedCreateRoom == null)
        {
            UniversalAddresableLoader.Instance.Spawn(LobbyUI, Vector3.zero, null, OnDoneSpawnRoomUI);
            BGmainmenuUI.SetActive(false);
            SpawnedGarage.SetActive(false);
        }
        else
        {
            activatepodiumlobby();
            BGLobbyUI.SetActive(true);
            SpawnedCreateRoom.SetActive(true);
            BGmainmenuUI.SetActive(false);
            SpawnedGarage.SetActive(false);
            LobbyHeaders.SetActive(true);
            GarageHeaders.SetActive(false);

        }
    }

    private void Garage()
    {
        if (SpawnedGarage == null)
        {
            UniversalAddresableLoader.Instance.Spawn(MainMenuUI, Vector3.zero, null, Ondonespawnheadersmenu);
        }
        else
        {
            activatepodiumonmenu();
            BGmainmenuUI.SetActive(true);
            SpawnedGarage.SetActive(true);
        }
    }

    private IEnumerator StartLoadLevel()
    {
        UniversalAddresableLoader.Instance.Spawn(LoadingUI, Vector3.zero, null, OnDoneSpawnLoadingUI);
        yield return LoadingDelay;
        LoadingCoroutine = null;
        yield break;
    }

    private void OnDonePhotonLoadScene()
    {
        //UniversalAddresableLoader.Instance.Spawn(ControllerHUD, Vector3.zero, null, OnDoneSpawnPlayer);
        //PhotonNetwork.Instantiate(Path.Combine("PhotonResources", "Bike"), new Vector3(-4.233918f, -13.44f, 52.79f), Quaternion.identity);
    }

    private void OnDoneSpawnLoadingUI(GameObject obj)
    {
        LoadingInstances = obj;
    }

    private void OnDoneSpawnRandomMatchUI(GameObject obj)
    {
        SpawnedCreateRoom.SetActive(false);
        randommatch = true;
        RandomMatchUI = obj;

        var Texts = obj.GetComponentsInChildren<TMP_Text>();
        var button = obj.GetComponentsInChildren<Button>();

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "WaitingStatus":
                    waitingstatus = Texts[i];
                    continue;

                case "PlayerCount":
                    playerdetail = Texts[i];
                    continue;

                case "Countdown":
                    findmatchcountdown = Texts[i];
                    continue;
            }
        }
        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "Leave Random Room":
                    if (PhotonNetwork.InRoom)
                    {
                        //button[i].onClick.AddListener(() => cancelfindmatch());
                        button[i].onClick.AddListener(() => LeaveRoom());
                        button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
                    }
                    else
                    {
                        button[i].onClick.AddListener(() => cancelfindmatch());
                        button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
                    }

                    continue;
            }
        }
    }

    private void cancelfindmatch()
    {
        if (PhotonNetwork.InRoom)
        {
            LeaveRoom();
        }
        else
        {
            BGLobbyUI.SetActive(true);
            SpawnedCreateRoom.SetActive(true);
            Destroy(RandomMatchUI);
            randommatch = false;
        }
    }

    private void randommode(int a)
    {
        PhotonNetwork.JoinRandomRoom(null, (byte)a);
    }

    void IMatchmakingCallbacks.OnJoinRandomFailed(short returnCode, string message)
    {
        if (PhotonNetwork.IsConnected)
        {
            Debug.Log("make own room");
            UserCreateRoom("random match", maxrandomplayer);

            if (findmatchcountdown != null)
            {
                findmatchcountdown.gameObject.SetActive(true);
                findmatchcountdown.text = "10";
            }

            InvokeRepeating("countdown", 10f, 1f);
        }
        else
        {
            Debug.Log("try to reconnect");
            TryToReconnect();
        }
    }

    private void countdown()
    {
        Debug.Log("countdown : " + findmatchcountdown.text);
        int a = int.Parse(findmatchcountdown.text);
        if (a != 0 && !PhotonNetwork.InRoom)
        {
            a -= 1;
            findmatchcountdown.text = a.ToString();
        }
        else
        {
            if (PhotonNetwork.InRoom)
            {
                findmatchcountdown.text = " ";
                CancelInvoke("countdown");
            }
            else
            {
                cancelfindmatch();
            }
        }
    }

    private void levelselectionmanager(TMP_Dropdown obj)
    {

        if (mapselection == null)
        {
            mapselection = obj;
        }
        mapselection.options.Clear();
        int scenecount = SceneManager.sceneCountInBuildSettings;

        for (int a = 4; a <= scenecount - 1; a++)
        {
            //  Debug.Log("scene level : " + GetSceneNameFromBuildIndex(a));
            mapoption.Add(GetSceneNameFromBuildIndex(a));
        }

        foreach (var item in mapoption)
        {
            mapselection.options.Add(new TMP_Dropdown.OptionData() { text = item });
        }

        mapselection.RefreshShownValue();
        int firstloop = 0;

        if (firstloop != 1)
        {
            getmapselectionindex(0);
            firstloop += 1;
        }

        mapselection.value = 0;
        mapselection.onValueChanged.AddListener((x) => getmapselectionindex(x));

        mapselection.onValueChanged.AddListener((x) => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
    }

    private void OnDoneSpawnCreateOption(GameObject obj)
    {

        if (SpawnedCreateRoomOption == null)
        {
            // Debug.Log("inisialisasi SpawnedCreateRoomOption ");
            SpawnedCreateRoomOption = obj;
        }

        var Con = obj.GetComponent<StartRoomHandler>();
        var Sliders = obj.GetComponentsInChildren<Slider>();
        var Button = obj.GetComponentsInChildren<Button>();
        var IField = obj.GetComponentsInChildren<TMP_InputField>();
        var Texts = obj.GetComponentsInChildren<TMP_Text>();
        var dropdown = obj.GetComponentsInChildren<TMP_Dropdown>();


        if (Con != null)
        {
            LobbyListParent = Con.GetContentsTransform();
        }

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "Max Player Holder":
                    _MaxPlayerHold = Texts[i];
                    continue;
            }
        }

        for (int i = 0; i < dropdown.Length; i++)
        {
            switch (dropdown[i].tag)
            {
                case "Map Selection":
                    mapselection = dropdown[i];
                    levelselectionmanager(dropdown[i]);
                    continue;
            }
        }

        for (int i = 0; i < IField.Length; i++)
        {
            switch (IField[i].tag)
            {
                case "Input Room Name":
                    IField[i].onValueChanged.AddListener((x) => SetCurrentRoomName(x));
                    continue;
            }
        }

        for (int i = 0; i < Sliders.Length; i++)
        {
            switch (Sliders[i].tag)
            {
                case "Input Max Player":
                    Sliders[i].onValueChanged.AddListener((x) => SetCurrentMaxPlayer(x));
                    continue;
            }
        }

        for (int i = 0; i < Button.Length; i++)
        {
            switch (Button[i].tag)
            {
                case "Create Room":
                    Button[i].onClick.AddListener(() => CreateRoom());
                    Button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;

                case "Cancel Room":
                    Button[i].onClick.AddListener(() => CancelCreateRoom());
                    Button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
                    continue;
            }
        }
        if (firstroomoption == 0)
        {
            SpawnedCreateRoomOption.SetActive(false);
            firstroomoption += 1;
        }
    }

    private void OnDoneSpawnRoomUI(GameObject obj)
    {
        if (SpawnedCreateRoom == null)
            SpawnedCreateRoom = obj;

        var Con = obj.GetComponent<StartRoomHandler>();
        var Sliders = obj.GetComponentsInChildren<Slider>();
        var Button = obj.GetComponentsInChildren<Button>();
        var IField = obj.GetComponentsInChildren<TMP_InputField>();
        var Texts = obj.GetComponentsInChildren<TMP_Text>();
        var dropdown = obj.GetComponentsInChildren<TMP_Dropdown>();

        if (Con != null)
        {
            LobbyListParent = Con.GetContentsTransform();
        }

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "Max Player Holder":
                    _MaxPlayerHold = Texts[i];
                    continue;
            }
        }

        for (int i = 0; i < dropdown.Length; i++)
        {
            switch (dropdown[i].tag)
            {
                case "Map Selection":
                    mapselection = dropdown[i];
                    int firstloop = 0;

                    if (firstloop != 1)
                    {
                        getmapselectionindex(0);
                        firstloop += 1;
                    }

                    mapselection.value = 0;
                    mapselection.onValueChanged.AddListener((x) => getmapselectionindex(x));
                    mapselection.onValueChanged.AddListener((x) => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;
            }
        }

        for (int i = 0; i < IField.Length; i++)
        {
            switch (IField[i].tag)
            {
                case "Input Room Name":
                    IField[i].onValueChanged.AddListener((x) => SetCurrentRoomName(x));
                    continue;
            }
        }

        for (int i = 0; i < Sliders.Length; i++)
        {
            switch (Sliders[i].tag)
            {
                case "Input Max Player":
                    Sliders[i].onValueChanged.AddListener((x) => SetCurrentMaxPlayer(x));
                    continue;
            }
        }

        for (int i = 0; i < Button.Length; i++)
        {
            switch (Button[i].tag)
            {
                case "Quick Start":
                    Button[i].onClick.AddListener(() => QuickStart());
                    Button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;

                case "Custom Start":
                    Button[i].onClick.AddListener(() => SetCustomRoomStart());
                    Button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;

                case "Create Room":
                    Button[i].onClick.AddListener(() => CreateRoom());
                    Button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonClick_1"));
                    continue;

                case "Cancel Room":
                    Button[i].onClick.AddListener(() => CancelCreateRoom());
                    Button[i].onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
                    continue;

            }
        }

        if (first == 0)
        {
            BGLobbyUI.SetActive(false);
            SpawnedCreateRoom.SetActive(false);
            first += 1;
        }
        else
        {
            LobbyHeaders.SetActive(true);
            GarageHeaders.SetActive(false);
        }

    }

    private void getmapselectionindex(int map)
    {
        mapindex = map;
        UniversalSceneLoader.Instance.setmapname(mapselection.options[map].text);
    }

    private void CancelCreateRoom()
    {
        BGLobbyUI.SetActive(true);
        SpawnedCreateRoom.SetActive(true);
        SpawnedCreateRoomOption.SetActive(false);
    }

    private void CreateRoom()
    {
        if (CurrentRoomName.Length <= 14)
        {
            if (validatingcourotine == null)
            {
                UniversalAddresableLoader.Instance.Spawn(ValidatingLoadingUI, Vector3.zero, null, OnValidatingProccess);
                validatingcourotine = StartCoroutine(createcustom());
            }
            else
            {
                validatingUI.SetActive(true);
                validatingcourotine = StartCoroutine(createcustom());
            }
        }
        else
        {
            if (namewarningPanel == null)
            {
                UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
            }
            else
            {
                namewarning(namewarningPanel);
            }
        }

    }

    private IEnumerator createcustom()
    {
        Debug.Log("creating room");
        UserCreateRoom(CurrentRoomName, CurrentMaxPlayer);
        yield return WaitForPlayerInRoom;
        RoomHeaders.SetActive(true);
        LobbyHeaders.SetActive(false);
        GarageHeaders.SetActive(false);
        BGLobbyUI.SetActive(false);
        SpawnedCreateRoomOption.SetActive(false);
        UniversalAddresableLoader.Instance.Spawn(MatchRoomUI, Vector3.zero, null, OnDoneCreateMatchRoom);
    }

    private void OnValidatingProccess(GameObject obj)
    {
        if (validatingUI == null)
        {
            validatingUI = obj;
        }
        validatingUI.SetActive(true);
    }



    private void createpractice()
    {
        practicemode = true;
        UserCreateRoom("random match", 1);
    }

    private void OnDoneCreateMatchRoom(GameObject obj)
    {
        Destroy(CreateRoomObject);
        _LobbyHandler = obj.GetComponent<LobbyHandler>();
        _LobbyHandler.GetLeaveRoomButton().onClick.AddListener(() => LeaveRoom());
        _LobbyHandler.courierselectiononroom().onClick.AddListener(() => characterselectiononroom());
        _LobbyHandler.GetLeaveRoomButton().onClick.AddListener(() => UniversalSoundHandler.Instance.MenuPlaySFX1.Invoke("SFX_ButtonBack_1"));
        _LobbyHandler.SubsribeStartGame(OnStartTheGame);
        BGLobbyUI.SetActive(true);
        activatepodiumlobby();
        SpawnedCreateRoom.SetActive(false);
        SpawnedMatchRoom = obj;
        hideroom = SpawnedMatchRoom.GetComponent<Canvas>();
    }

    private void characterselectiononroom()
    {
        courierselectioniroomheaders.SetActive(true);
        BGcharselectionUI.SetActive(true);
        Spawnedcharacterselection.SetActive(true);
        RoomHeaders.SetActive(false);
    }

    public void LeaveRoom()
    {
        Debug.Log("we will leave room");
        UICamera.gameObject.SetActive(true);
        if (PhotonNetwork.InRoom)
        {
            if (!randommatch && !practicemode)
            {
                validatingUI.SetActive(true);
                validatingleave = StartCoroutine(leavecustommatch());
            }
            else if (randommatch || practicemode)
            {
                // Debug.Log("leave random match");
                Debug.Log("leave random match");
                photonView.RPC("RPC_RemoveMasterClientPlayerBoard", RpcTarget.Others, PhotonNetwork.LocalPlayer.GetPlayerID());
                ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
                PhotonNetwork.LocalPlayer.SetCustomProperties(h);
                PhotonNetwork.LeaveRoom();
                BGLobbyUI.SetActive(true);
                SpawnedCreateRoom.SetActive(true);
                Destroy(RandomMatchUI);
                randommatch = false;
                practicemode = false;
            }
        }
        else
        {
            Debug.Log("you are not in any room");
        }
    }

    private IEnumerator leavecustommatch()
    {
        Debug.Log("leave custom match");
        photonView.RPC("RPC_RemoveMasterClientPlayerBoard", RpcTarget.Others, PhotonNetwork.LocalPlayer.GetPlayerID());
        ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
        h.Add("PLAYER_READY", false);
        RemovePlayerBoard(PhotonNetwork.LocalPlayer);
        PhotonNetwork.LocalPlayer.SetCustomProperties(h);
        PhotonNetwork.LeaveRoom();
        yield return WaitForleaveroom;
        validatingUI.SetActive(false);
        BGLobbyUI.SetActive(false);
        Destroy(SpawnedMatchRoom);
        BGLobbyUI.SetActive(true);
        SpawnedCreateRoom.SetActive(true);
        RoomHeaders.SetActive(false);
        GarageHeaders.SetActive(false);
        courierdetailheaders.SetActive(false);
        courierselectioninmenuheaders.SetActive(false);
        courierselectioniroomheaders.SetActive(false);
        LobbyHeaders.SetActive(true);
        randommatch = false;
        practicemode = false;
    }

    public void SetGameStartState(bool val)
    {
        GameStart = val;
    }

    private void OnStartTheGame()  ///aaahaaa
    {
        if (PhotonNetwork.IsMasterClient)
        {
            GameStart = true;
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(UniversalHandler.START_LOADING, null, raiseEventOptions, SendOptions.SendReliable);

            if (!randommatch && !practicemode)
            {
                var k = PhotonNetwork.LocalPlayer;
                ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
                h.Add(MAP_KEY, mapselection.options[mapindex].text);
                k.SetCustomProperties(h);
                Debug.Log("identified host custom match ");
            }
            else
            {
                var k = PhotonNetwork.LocalPlayer;
                ExitGames.Client.Photon.Hashtable h = new ExitGames.Client.Photon.Hashtable();
                h.Add(MAP_KEY, mapselection.options[randommapindex].text);
                k.SetCustomProperties(h);
                Debug.Log("identified host random match");
            }
        }
        if (LoadingCoroutine == null)
        {
            LoadingCoroutine = StartCoroutine(PleaseWait());
        }
    }

    private IEnumerator PleaseWait()
    {
        yield return LoadingDelay;
        //UniversalHandler.Instance.BGScreen.SetActive(false);
        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("run as host");
            if (!randommatch && !practicemode)
            {
                Debug.Log("run custom room");
                UniversalSceneLoader.Instance.PhotonLoadScene(mapselection.options[mapindex].text);
            }
            else if (randommatch || practicemode)
            {
                Debug.Log("run random room" + randommapindex);
                UniversalSceneLoader.Instance.setmapname(mapselection.options[randommapindex].text);
                UniversalSceneLoader.Instance.PhotonLoadScene(mapselection.options[randommapindex].text);
            }
        }
        else
        {
            Debug.Log("run as client");
            UniversalSceneLoader.Instance.setmapname(PhotonNetwork.PlayerList[0].CustomProperties[MAP_KEY].ToString());
            UniversalSceneLoader.Instance.PhotonLoadScene(PhotonNetwork.PlayerList[0].CustomProperties[MAP_KEY].ToString());
        }

        Debug.Log("loading level");
        yield return SceneIsActive;
        Debug.Log("done load level");
        yield return LoadingDelay;
        Debug.Log("done loading");
        _chatgui.disablechat();


        if (randommatch && !practicemode)
        {
            QuickMatchOption.SetActive(false);
            LoadingInstances.SetActive(false);
            RandomMatchUI.SetActive(false);
            BGLobbyUI.SetActive(false);
            SpawnedCreateRoom.SetActive(false);
            Spawnedheadersmenu.SetActive(false);
        }
        else if (!randommatch && practicemode)
        {
            BGLobbyUI.SetActive(false);
            LoadingInstances.SetActive(false);
            QuickMatchOption.SetActive(false);
            SpawnedCreateRoom.SetActive(false);
            Spawnedheadersmenu.SetActive(false);
        }
        else
        {
            BGLobbyUI.SetActive(false);
            LoadingInstances.SetActive(false);
            SpawnedMatchRoom.SetActive(false);
            SpawnedCreateRoom.SetActive(false);
            Spawnedheadersmenu.SetActive(false);
        }
        UICamera.gameObject.SetActive(false);
        // UIongame();
        LoadingCoroutine = null;
        yield break;
    }

    private void UIongame()
    {
        FindObjectOfType<ChatGui>().disablechat();
        _chatgui.disablechat();
        if (!randommatch && !practicemode)
        {
            LoadingInstances.SetActive(false);
            SpawnedMatchRoom.SetActive(false);
            SpawnedCreateRoom.SetActive(false);
            Spawnedheadersmenu.SetActive(false);
        }
        else if (randommatch && !practicemode)
        {
            QuickMatchOption.SetActive(false);
            LoadingInstances.SetActive(false);
            RandomMatchUI.SetActive(false);
            SpawnedCreateRoom.SetActive(false);
            Spawnedheadersmenu.SetActive(false);
        }
        else if (!randommatch && practicemode)
        {
            LoadingInstances.SetActive(false);
            QuickMatchOption.SetActive(false);
            SpawnedCreateRoom.SetActive(false);
            Spawnedheadersmenu.SetActive(false);
        }
    }

    private void OnDoneSpawnConnectingUI(GameObject obj)
    {
        ConnectingInstance = obj;
    }

    private void UserCreateRoom(string name, int MaxPlayer)
    {
        Debug.Log("proccess creating room");
        lobbyoption = new string[1];
        lobbyoption[0] = MAP_KEY;
        if (name == "random match")
        {
            randommapindex = Random.Range(0, 2);
            Debug.Log("random map index : " + randommapindex);
            ExitGames.Client.Photon.Hashtable customProperties = new ExitGames.Client.Photon.Hashtable() { { MAP_KEY, "random" } };
            currentrandomroom++;
            UserRoomOptions = new RoomOptions();
            UserRoomOptions.IsVisible = true;
            UserRoomOptions.IsOpen = true;
            UserRoomOptions.MaxPlayers = (byte)MaxPlayer;
            UserRoomOptions.EmptyRoomTtl = 10000;
            UserRoomOptions.PlayerTtl = 10000;
            UserRoomOptions.PublishUserId = true;
            UserRoomOptions.CustomRoomPropertiesForLobby = lobbyoption;
            UserRoomOptions.CleanupCacheOnLeave = true;
            UserRoomOptions.CustomRoomProperties = customProperties;
            PhotonNetwork.CreateRoom(name + currentrandomroom, UserRoomOptions);
        }
        else if (name != "random match")
        {
            ExitGames.Client.Photon.Hashtable customProperties = new ExitGames.Client.Photon.Hashtable() { { MAP_KEY, mapselection.options[mapindex].text } };
            UserRoomOptions = new RoomOptions();
            UserRoomOptions.IsVisible = true;
            UserRoomOptions.IsOpen = true;
            UserRoomOptions.MaxPlayers = (byte)MaxPlayer;
            UserRoomOptions.EmptyRoomTtl = 10000;
            UserRoomOptions.PlayerTtl = 10000;
            UserRoomOptions.PublishUserId = true;
            UserRoomOptions.CleanupCacheOnLeave = true;
            UserRoomOptions.CustomRoomPropertiesForLobby = lobbyoption;
            UserRoomOptions.CustomRoomProperties = customProperties;
            PhotonNetwork.CreateRoom(name, UserRoomOptions);
        }
    }

    private void UserCreateRoom()
    {
        var RandomNum = UnityEngine.Random.Range(0, 9999);
        var RandomName = new StringBuilder("Room ");
        var RoomName = RandomName.Append(RandomNum.ToString());

        UserRoomOptions = new RoomOptions();
        UserRoomOptions.IsVisible = true;
        UserRoomOptions.IsOpen = true;
        UserRoomOptions.MaxPlayers = 20;
        UserRoomOptions.EmptyRoomTtl = 1000;
        UserRoomOptions.PlayerTtl = 60000;
        UserRoomOptions.PublishUserId = true;
        UserRoomOptions.CleanupCacheOnLeave = true;
        PhotonNetwork.CreateRoom(RoomName.ToString(), UserRoomOptions);
    }

    private void UserCreateRoom(string RoomName, bool Visible, bool Open, byte MaxPlayer)
    {
        UserRoomOptions = new RoomOptions();
        UserRoomOptions.IsVisible = Visible;
        UserRoomOptions.IsOpen = Open;
        UserRoomOptions.MaxPlayers = MaxPlayer;
        UserRoomOptions.EmptyRoomTtl = 1000;
        UserRoomOptions.PlayerTtl = 60000;
        UserRoomOptions.PublishUserId = true;
        UserRoomOptions.CleanupCacheOnLeave = true;
        PhotonNetwork.CreateRoom(RoomName, UserRoomOptions);
    }

    private void TryToReconnect()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            if (ConnectingCoroutine == null)
            {
                ConnectingCoroutine = StartCoroutine(StartConnectingUI());
            }
        }
    }

    #endregion

    #region Photon Callbacks

    public override void OnJoinedLobby()
    {
        PhotonNetwork.FindFriends(new string[] { "1" });
        Debug.Log("Join lobby");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("App Version : " + PhotonNetwork.AppVersion);

        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("player enter room");

        if (SpawnPlayerBoardCoroutine == null && !randommatch && !practicemode)
        {
            Debug.Log("spawnplayerboard = null ");
            //_LobbyHandler.getuserid = PhotonNetwork.Use
            SpawnPlayerBoardCoroutine = StartCoroutine(SpawnPlayerBoard());
        }

        if (PhotonNetwork.IsMasterClient)
        {
            PlayerCount++;
            RoomFull = PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers;

            if (!GameStart)
            {
                PhotonNetwork.CurrentRoom.IsOpen = PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers;
                PhotonNetwork.CurrentRoom.IsVisible = PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers;
            }
        }

        if (randommatch && !practicemode)
        {
            waitingstatus.text = "Waiting For Opponent...";
            playerdetail.text = "connected" + PhotonNetwork.CurrentRoom.PlayerCount + "/ " + PhotonNetwork.CurrentRoom.MaxPlayers;
            if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                waitingstatus.text = "All Opponent Found";
                playerdetail.text = "Match is ready to begin";
                Debug.Log("play random match");
                OnStartTheGame();
            }
        }


        foreach (var item in SpawnedPlayerBoard)
        {
            Debug.Log(item.Value.name);
        }

    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        RemovePlayerBoard(otherPlayer);
        Debug.Log(otherPlayer.NickName + "has left room");
        UICamera.gameObject.SetActive(true);

        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("he's master client");
            PlayerCount--;
            RoomFull = PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers;
            if (!GameStart)
            {
                Debug.Log("game isn't start");
                PhotonNetwork.CurrentRoom.IsOpen = PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers;
                PhotonNetwork.CurrentRoom.IsVisible = PlayerCount < PhotonNetwork.CurrentRoom.MaxPlayers;
            }
        }

        if (randommatch)
        {
            Debug.Log("leave random match");
            waitingstatus.text = "Waiting For Opponent...";
            playerdetail.text = "connected" + PhotonNetwork.CurrentRoom.PlayerCount + "/ " + PhotonNetwork.CurrentRoom.MaxPlayers;
        }

    }

    private void RemovePlayerBoard(Player otherPlayer)
    {
        if (SpawnedPlayerBoard.Count <= 0) return;
        if (SpawnedPlayerBoard.ContainsKey(otherPlayer.GetPlayerID()))
        {
            Destroy(SpawnedPlayerBoard[otherPlayer.GetPlayerID()]);
            SpawnedPlayerBoard.Remove(otherPlayer.GetPlayerID());
        }
    }

    public override void OnConnected()
    {
        if (ConnectingInstance != null && ConnectingInstance.activeInHierarchy)
        {
            ConnectingInstance.SetActive(false);
        }

        if (reconnectproccess)
        {
            Debug.Log("done reconnecting");
            reconnectproccess = false;
            reconnectsucces = true;
        }

        Debug.Log("connected to server.");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Cur room " + PhotonNetwork.CurrentRoom.AutoCleanUp);
        int playerCount = PhotonNetwork.CurrentRoom.PlayerCount;
        // bool custommatch = false;

        if (SpawnPlayerBoardCoroutine == null && randommatch == false && !practicemode)
        {
            LobbyHeaders.SetActive(false);
            RoomHeaders.SetActive(true);
            SpawnPlayerBoardCoroutine = StartCoroutine(SpawnPlayerBoard());
        }
        else if (randommatch && !practicemode)
        {
            findmatchcountdown.gameObject.SetActive(false);
            waitingstatus.text = "Waiting For Opponent...";
            playerdetail.text = "connected" + PhotonNetwork.CurrentRoom.PlayerCount + "/ " + PhotonNetwork.CurrentRoom.MaxPlayers;
        }
        else if (practicemode && !randommatch)
        {
            OnStartTheGame();
        }

    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        Debug.Log("OnplayerPropertiesUpdate");
        ChangePlayerReadyLocal(targetPlayer, changedProps);
    }

    void ChangePlayerReadyLocal(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        Debug.Log("change player local");
        if (!targetPlayer.IsLocal)
        {
            var LobbyItem = SpawnedPlayerBoard[targetPlayer.GetPlayerID()].GetComponent<PlayerLobbyItem>();
            LobbyItem.SetReadyState(changedProps["PLAYER_READY"] == null ? false : (bool)changedProps["PLAYER_READY"]);
        }
    }

    private IEnumerator SpawnPlayerBoard()
    {

        foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
        {
            Debug.Log("name : " + item.NickName + " " + item.GetPlayerID());

            if (!SpawnedPlayerBoard.ContainsKey(item.GetPlayerID()))
            {
                Debug.Log("!spawn");
                yield return WaitForRoomloaded;
                yield return UniversalAddresableLoader.Instance.SpawnIenumerator(_LobbyHandler.GetPlayerListContent(), Vector3.zero, _LobbyHandler.GetPlayerListContentView().gameObject, OnDoneLoadOtherPlayerList);
                yield return WaitForLobbyItem;
                validatingUI.SetActive(false);
                SpawnedPlayerBoard.Add(item.GetPlayerID(), _CurLobbyItem.gameObject);
                _CurLobbyItem.SetPlayerName(item.NickName);
                _CurLobbyItem.SetReadyState(item.CustomProperties["PLAYER_READY"] == null ? false : (bool)item.CustomProperties["PLAYER_READY"]);
                if (item.IsLocal)
                {
                    _LobbyHandler.setReadyGame(_CurLobbyItem.SetReadyState);
                }

                _CurLobbyItem = null;
            }
            else if (SpawnedPlayerBoard.ContainsKey(item.GetPlayerID()))
            {
                Debug.Log("spawn");
                if (SpawnedPlayerBoard[item.GetPlayerID()].gameObject)
                    RemovePlayerBoard(item);

                yield return UniversalAddresableLoader.Instance.SpawnIenumerator(_LobbyHandler.GetPlayerListContent(), Vector3.zero, _LobbyHandler.GetPlayerListContentView().gameObject, OnDoneLoadOtherPlayerList);
                yield return WaitForLobbyItem;
                SpawnedPlayerBoard[item.GetPlayerID()] = _CurLobbyItem.gameObject;
                _CurLobbyItem.SetPlayerName(item.NickName);
                _CurLobbyItem.SetReadyState(item.CustomProperties["PLAYER_READY"] == null ? false : (bool)item.CustomProperties["PLAYER_READY"]);
                if (item.IsLocal)
                {
                    _LobbyHandler.setReadyGame(_CurLobbyItem.SetReadyState);
                }

                _CurLobbyItem = null;
            }
        }
        SpawnPlayerBoardCoroutine = null;
        yield break;
    }

    private void OnDoneLoadOtherPlayerList(GameObject arg)
    {
        arg.transform.SetParent(_LobbyHandler.GetPlayerListContentView());
        arg.transform.localScale = Vector3.one;
        arg.transform.localPosition = Vector3.zero;
        _CurLobbyItem = arg.GetComponent<PlayerLobbyItem>();
    }

    private void OnDoneLoadOtherPlayerListLocal(GameObject arg)
    {
        arg.transform.SetParent(_LobbyHandler.GetPlayerListContentView());
        arg.transform.localScale = Vector3.one;
        arg.transform.localPosition = Vector3.zero;
        _CurLobbyItem = arg.GetComponent<PlayerLobbyItem>();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        if (PhotonNetwork.IsConnected)
        {
            Debug.Log("onjoinrandomfail yang biasa");
            UserCreateRoom(null, maxrandomplayer);
        }
        else
        {
            TryToReconnect();
        }
    }



    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        CachedRoomList = roomList;
        if (SpawnRoomListCoroutine == null)
        {
            Debug.Log("spawn room list");
            SpawnRoomListCoroutine = StartCoroutine(SpawnRoomList(roomList));
        }
    }


    private IEnumerator SpawnRoomList(List<RoomInfo> roomList)
    {
        foreach (var item in roomList)
        {
            if (item.RemovedFromList)
            {
                Debug.Log("is there?");

                if (SpawnedRoomBoard.ContainsKey(item.GetHashCode()) || !item.IsVisible || !item.IsOpen)
                {
                    Destroy(SpawnedRoomBoard[item.GetHashCode()]);
                    SpawnedRoomBoard.Remove(item.GetHashCode());
                }
            }
            else
            {
                if (!SpawnedRoomBoard.ContainsKey(item.GetHashCode()))
                {
                    // Debug.Log(item.Name);
                    if (!item.Name.Contains("random match"))
                    {
                        yield return UniversalAddresableLoader.Instance.SpawnIenumerator(LobbyRoomBoardUI, Vector3.zero, null, OnDoneSpawnRoomList);
                        yield return WaitForRoomItem;
                        _CurRoomListHandler.SetRoomName(item.Name);
                        _CurRoomListHandler.JoinRoomOnClick(OnJoinRoom);
                        _CurRoomListHandler.SetMaxPlayer(item.MaxPlayers == 0 ? 2 : item.MaxPlayers);
                        _CurRoomListHandler.SetCurrentPlayer(item.PlayerCount);
                        _CurRoomListHandler.SetMapName((string)item.CustomProperties[MAP_KEY]);
                        SpawnedRoomBoard.Add(item.GetHashCode(), _CurRoomListHandler.gameObject);
                        _CurRoomListHandler = null;
                    }
                    else if (item.Name.Contains("random match"))
                    {
                        string Roomnumber = Regex.Replace(item.Name, "[^0-9]", "");
                        currentrandomroom = int.Parse(Roomnumber);
                        _CurRoomListHandler = null;
                    }

                }
                else if (SpawnedRoomBoard.ContainsKey(item.GetHashCode()))
                {
                    _CurRoomListHandler = SpawnedRoomBoard[item.GetHashCode()].GetComponent<RoomListHandler>();
                    yield return WaitForRoomItem;
                    _CurRoomListHandler.SetMaxPlayer(item.MaxPlayers == 0 ? 2 : item.MaxPlayers);
                    _CurRoomListHandler.SetCurrentPlayer(item.PlayerCount);
                    _CurRoomListHandler = null;
                }
            }
        }

        SpawnRoomListCoroutine = null;
        yield break;
    }

    private IEnumerator RefreshRoomLists(List<RoomInfo> roomList)
    {

        foreach (var item in roomList)
        {
            if (item.RemovedFromList || !item.IsVisible || !item.IsOpen)
            {
                Debug.Log("is there?");

                if (SpawnedRoomBoard.ContainsKey(item.GetHashCode()))
                {
                    Destroy(SpawnedRoomBoard[item.GetHashCode()]);
                    SpawnedRoomBoard.Remove(item.GetHashCode());
                }
            }
            else
            {
                if (!SpawnedRoomBoard.ContainsKey(item.GetHashCode()))
                {
                    yield return UniversalAddresableLoader.Instance.SpawnIenumerator(LobbyRoomBoardUI, Vector3.zero, null, OnDoneSpawnRoomList);
                    yield return WaitForRoomItem;
                    _CurRoomListHandler.SetRoomName(item.Name);
                    _CurRoomListHandler.JoinRoomOnClick(OnJoinRoom);
                    _CurRoomListHandler.SetMaxPlayer(item.MaxPlayers == 0 ? 2 : item.MaxPlayers);
                    _CurRoomListHandler.SetCurrentPlayer(item.PlayerCount);
                    SpawnedRoomBoard.Add(item.GetHashCode(), _CurRoomListHandler.gameObject);
                    _CurRoomListHandler = null;
                }
                else if (SpawnedRoomBoard.ContainsKey(item.GetHashCode()))
                {
                    _CurRoomListHandler = SpawnedRoomBoard[item.GetHashCode()].GetComponent<RoomListHandler>();
                    yield return WaitForRoomItem;
                    _CurRoomListHandler.SetMaxPlayer(item.MaxPlayers == 0 ? 2 : item.MaxPlayers);
                    _CurRoomListHandler.SetCurrentPlayer(item.PlayerCount);
                    _CurRoomListHandler = null;
                }
            }

        }

        SpawnRoomListCoroutine = null;
        RefreshLobby = false;
        yield break;
    }

    private void OnJoinRoom(string obj)
    {

        if (validatingUI == null)
        {
            UniversalAddresableLoader.Instance.Spawn(ValidatingLoadingUI, Vector3.zero, null, OnValidatingProccess);
        }
        else
        {
            validatingUI.SetActive(true);
        }

        UniversalAddresableLoader.Instance.Spawn(MatchRoomUI, Vector3.zero, null, OnDoneCreateMatchRoom);
        //PhotonNetwork.JoinRoom(obj);
        waitjoinprocess = StartCoroutine(joinprocess(obj));

    }

    private IEnumerator joinprocess(string obj)
    {
        Debug.Log("testing join");
        PhotonNetwork.JoinRoom(obj);
        yield return LoadingDelay;
        if (!PhotonNetwork.InRoom)
        {
            // waitjoinprocess = StopCoroutine(joinprocess());
            waitjoinprocess = StartCoroutine(joinprocess(obj));
        }
    }

    private void OnDoneSpawnRoomList(GameObject arg)
    {
        arg.transform.SetParent(LobbyListParent);
        arg.transform.localScale = Vector3.one;
        _CurRoomListHandler = arg.GetComponent<RoomListHandler>();
        Debug.Log("inisialisasi _CurRoomListHandler");
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        //PhotonNetwork.JoinRoom(CurrentRoomName);
    }

    #endregion

    #region RPC & Raise Events 

    [PunRPC]
    void RPC_RemoveMasterClientPlayerBoard(int id)
    {
        Player p = PhotonNetwork.PlayerList.ToList().Find(x => x.GetPlayerID() == id);
        RemovePlayerBoard(p);
    }

    void SpawnLoadingPlease()
    {
        enabled = false;
        if (LoadingInstances == null)
        {
            UniversalAddresableLoader.Instance.Spawn(LoadingUI, Vector3.zero, null, OnDoneInstantiateLoadingInstance);
        }
        else if (randommatch && !practicemode)
        {
            LoadingInstances.SetActive(true);
            waitingstatus.text = "Match is ready";
            playerdetail.text = "waiting other player";
        }
        else
        {
            LoadingInstances.SetActive(true);
        }


        if (LoadingCoroutine == null)
        {
            LoadingCoroutine = StartCoroutine(PleaseWait());
        }
    }

    private void OnDoneInstantiateLoadingInstance(GameObject obj)
    {

        if (LoadingInstance == null)
        {
            LoadingInstances = obj;
        }

        else
        {
            LoadingInstances.SetActive(true);
        }
    }

    void IOnEventCallback.OnEvent(EventData photonEvent)
    {
        switch (photonEvent.Code)
        {
            case UniversalHandler.START_LOADING:
                SpawnLoadingPlease();
                break;

            case UniversalHandler.SEND_PLAYER_BOARD:
                break;
        }
    }

    #endregion

    #region firebase
    private void loginpagemanager()
    {
        var button = GameLauncherUI.GetComponentsInChildren<Button>();
        var Texts = GameLauncherUI.GetComponentsInChildren<TMP_Text>();
        var panel = GameLauncherUI.GetComponentsInChildren<Transform>();
        var input = GameLauncherUI.GetComponentsInChildren<TMP_InputField>();

        for (int i = 0; i < panel.Length; i++)
        {
            switch (panel[i].tag)
            {
                case "loginpage":
                    loginUI = panel[i].gameObject;
                    continue;
                case "registerpage":
                    registerUI = panel[i].gameObject;
                    registerUI.SetActive(false);
                    continue;
            }
        }

        for (int i = 0; i < input.Length; i++)
        {
            switch (input[i].tag)
            {
                case "emaillogin":
                    input[i].onValueChanged.AddListener((x) => Setemaillogin(x));
                    continue;
                case "passwordlogin":
                    input[i].onValueChanged.AddListener((x) => Setpasswordlogin(x));
                    continue;
                case "emailregister":
                    input[i].onValueChanged.AddListener((x) => SetEmailregister(x));
                    continue;
                case "usernameregister":
                    input[i].onValueChanged.AddListener((x) => Setusernameregister(x));
                    continue;
                case "passwordregister":
                    input[i].onValueChanged.AddListener((x) => Setpasswordregister(x));
                    continue;
                case "passwordcofirmregister":
                    input[i].onValueChanged.AddListener((x) => setpasswordregistervverify(x));
                    continue;
            }
        }

        for (int i = 0; i < Texts.Length; i++)
        {
            switch (Texts[i].tag)
            {
                case "warninglogintext":
                    warningLoginText = Texts[i];
                    continue;
                case "confirmlogintext":
                    confirmLoginText = Texts[i];
                    continue;
                case "warningregistertext":
                    warningRegisterText = Texts[i];
                    continue;
            }
        }

        for (int i = 0; i < button.Length; i++)
        {
            switch (button[i].tag)
            {
                case "loginbutton":
                    button[i].onClick.AddListener(() => LoginButton());
                    continue;
                case "toregisterpanelbutton":
                    button[i].onClick.AddListener(() => RegisterScreen());
                    continue;
                case "registerbutton":
                    button[i].onClick.AddListener(() => RegisterButton());
                    continue;
                case "backtologinpagebutton":
                    button[i].onClick.AddListener(() => LoginScreen());
                    continue;
            }
        }
    }
    public void Setemaillogin(string emaillogin)
    {
        emailLoginField = emaillogin;
    }
    public void Setpasswordlogin(string passwordlogin)
    {
        passwordLoginField = passwordlogin;
    }
    public void SetEmailregister(string emailregister)
    {
        emailRegisterField = emailregister;
    }
    public void Setusernameregister(string usernameregister)
    {
        usernameRegisterField = usernameregister;
    }
    public void Setpasswordregister(string passwordregister)
    {
        passwordRegisterField = passwordregister;
    }
    public void setpasswordregistervverify(string passwordregistervalidation)
    {
        passwordRegisterVerifyField = passwordregistervalidation;
    }

    public void loadnickname(string _nickname)
    {
        Nicknameplayer = _nickname;
    }

    public void loadidplayer(string _Idplayer)
    {
        IDplayer = _Idplayer;
    }

    public void loadlevel(int _level)
    {
        level = _level;
    }

    public void loadexpplayer(float _exp)
    {
        exp = _exp;
    }

    public void scoreplayer(float _score)
    {
        score = _score;
    }

    public void characterselectedcode(int _codeplayer)
    {
        characterselected = _codeplayer;
    }

    public void createnewdata()
    {
        //StartCoroutine(UpdateIDDatabase(1.ToString()));
        StartCoroutine(UpdateUsernameDatabase(usernameRegisterField));
        StartCoroutine(PlayerIDgenerator());
        StartCoroutine(UpdateLevelDatabase(1.ToString()));
        StartCoroutine(UpdateEXPDatabase(0.ToString()));
        StartCoroutine(UpdatescoreDatabase(0.ToString()));
        StartCoroutine(UpdatenumberofmatchDatabase(0.ToString()));
        StartCoroutine(UpdatecustommatchplayedDatabase(0.ToString()));
        StartCoroutine(UpdatequickmatchplayedDatabase(0.ToString()));
        StartCoroutine(UpdateteammatchplayedDatabase(0.ToString()));
        StartCoroutine(UpdatedeathmatchplayedDatabase(0.ToString()));
        StartCoroutine(UpdatenumberofmvpDatabase(0.ToString()));
        StartCoroutine(UpdatenumberofteamwinsDatabase(0.ToString()));
        StartCoroutine(UpdatenumberoflosingDatabase(0.ToString()));
        StartCoroutine(UpdatepremangameplayedDatabase(0.ToString()));
        StartCoroutine(UpdatembahgameplayedDatabase(0.ToString()));
        StartCoroutine(UpdatealiengameplayedDatabase(0.ToString()));
        StartCoroutine(Updatecourierselected(501.ToString()));
    }

    public void SaveusernameButton()
    {
        StartCoroutine(UpdateUsernameDatabase(usernameRegisterField));
    }


    public void LoginScreen() //Back button
    {
        loginUI.SetActive(true);
        registerUI.SetActive(false);
    }

    public void RegisterScreen() // Regester button
    {
        loginUI.SetActive(false);
        registerUI.SetActive(true);
    }

    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        //Set the authentication instance object
        auth = FirebaseAuth.DefaultInstance;
        DBreference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    //Function for the login button
    public void LoginButton()
    {
        //Call the login coroutine passing the email and password
        StartCoroutine(Login(emailLoginField, passwordLoginField));
    }
    //Function for the register button
    public void RegisterButton()
    {
        //Call the register coroutine passing the email, password, and username
        StartCoroutine(Register(emailRegisterField, passwordRegisterField, usernameRegisterField));
    }


    private IEnumerator Login(string _email, string _password)
    {
        //Call the Firebase auth signin function passing the email and password
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        //Wait until the task completes
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

        if (LoginTask.Exception != null)
        {
            //If there are errors handle them
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            string message = "Login Failed!";
            switch (errorCode)
            {
                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WrongPassword:
                    message = "Wrong Password";
                    break;
                case AuthError.InvalidEmail:
                    message = "Invalid Email";
                    break;
                case AuthError.UserNotFound:
                    message = "Account does not exist";
                    break;
            }
            loginwarningtemp = message;
            UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
        }
        else
        {
            //User is now logged in
            //Now get the result
            User = LoginTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", User.DisplayName, User.Email);
            StartCoroutine(LoadUserData());

            //confirmLoginText.text = "Logged In";
        }
    }

    private IEnumerator LoadUserData()
    {
        //Get the currently logged in user data
        var DBTask = DBreference.Child("users").Child(User.UserId).GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            DataSnapshot snapshot = DBTask.Result;
            IDplayer = snapshot.Child("ID_Player").Value.ToString();
            Nicknameplayer = snapshot.Child("username").Value.ToString();
            level = int.Parse(snapshot.Child("Level").Value.ToString());
            exp = float.Parse(snapshot.Child("EXP").Value.ToString());
            score = float.Parse(snapshot.Child("Score").Value.ToString());
            characterselected = int.Parse(snapshot.Child("Hero_selected").Value.ToString());
        }
        LoginToGame();
    }

    private IEnumerator reloadcharacterselection()
    {
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }

        else
        {
            DataSnapshot snapshot = DBTask.Result;
            characterselected = int.Parse(snapshot.Child("Hero_selected").Value.ToString());
        }
    }

    private IEnumerator Register(string _email, string _password, string _username)
    {
        if (_username == "")
        {
            //If the username field is blank show a warning
            loginwarningtemp = "Missing Username";
            UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
        }
        else if (passwordRegisterField != passwordRegisterVerifyField)
        {
            //If the password does not match show a warning
            loginwarningtemp = "Password Does Not Match!";
            UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
        }
        else
        {
            //Call the Firebase auth signin function passing the email and password
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
            //Wait until the task completes
            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                //If there are errors handle them
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                string message = "Register Failed!";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WeakPassword:
                        message = "Weak Password";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        message = "Email Already In Use";
                        break;
                }
                loginwarningtemp = message;
                UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
            }
            else
            {
                //User has now been created
                //Now get the result
                User = RegisterTask.Result;

                if (User != null)
                {
                    //Create a user profile and set the username
                    UserProfile profile = new UserProfile { DisplayName = _username };

                    //Call the Firebase auth update user profile function passing the profile with the username
                    var ProfileTask = User.UpdateUserProfileAsync(profile);
                    //Wait until the task completes
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        //If there are errors handle them
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                        loginwarningtemp = "Username Set Failed!";
                        UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
                    }
                    else
                    {
                        //Username is now set
                        //Now return to login screen
                        createnewdata();
                        LoginScreen();
                        loginwarningtemp = "Register success";
                        UniversalAddresableLoader.Instance.Spawn(NameWarningUI, Vector3.zero, null, namewarning);
                    }
                }
            }
        }
    }

    private IEnumerator PlayerIDgenerator()
    {
        Debug.Log("check ID");
        //Get all the users data ordered by kills amount
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").OrderByChild("ID_Player").GetValueAsync();
        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Data has been retrieved
            DataSnapshot snapshot = DBTask.Result;
            int i = 0;
            foreach (DataSnapshot childSnapshot in snapshot.Children.Reverse<DataSnapshot>())
            {
                if (childSnapshot.Child("ID_Player").Value != null)
                {
                    int AnotherplayerID = int.Parse(childSnapshot.Child("ID_Player").Value.ToString());
                    Debug.Log("ID :" + AnotherplayerID + " on interation : " + i);
                    IDplayertemp[i] = AnotherplayerID;
                    i++;
                    Array.Resize<int>(ref IDplayertemp, IDplayertemp.Length + 1);
                }
                else
                {
                    i--;
                    Array.Resize<int>(ref IDplayertemp, IDplayertemp.Length - 1);
                    Debug.Log("int i now : " + i + " with value : " + IDplayertemp[i]);
                    int IDplayer = IDplayertemp[0] + 1;
                    Debug.Log("setting player ID : " + IDplayertemp[0] + " + 1 =" + IDplayer);
                    fixPlayerID = IDplayer;
                    StartCoroutine(UpdateIDDatabase(IDplayer.ToString()));
                }
            }
        }
    }


    private IEnumerator UpdateUsernameDatabase(string _username)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("username").SetValueAsync(_username);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database username is now updated
        }
    }

    private IEnumerator Updatecourierselected(string _selectedheroes)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Hero_selected").SetValueAsync(_selectedheroes);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database _selectedheroes is now updated
        }
    }

    private IEnumerator UpdateIDDatabase(string _idplayer)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("ID_Player").SetValueAsync(_idplayer);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database _idplayer is now updated
        }
    }

    private IEnumerator UpdateLevelDatabase(string _LevelPlayer)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Level").SetValueAsync(_LevelPlayer);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Level is now updated
        }
    }

    private IEnumerator UpdateEXPDatabase(string _EXPplayer)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("EXP").SetValueAsync(_EXPplayer);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database EXP is now updated
        }
    }

    private IEnumerator UpdatescoreDatabase(string _scoreplayer)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Score").SetValueAsync(_scoreplayer);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database score is now updated
        }
    }

    private IEnumerator UpdatenumberofmatchDatabase(string _numberofmatch)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Number of Match Played").SetValueAsync(_numberofmatch);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database numberofmatch is now updated
        }
    }

    private IEnumerator UpdatecustommatchplayedDatabase(string _custommatchplayed)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Custom Match Played").SetValueAsync(_custommatchplayed);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database custommatchplayed is now updated
        }
    }

    private IEnumerator UpdatequickmatchplayedDatabase(string _quickmatchplayed)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Quick Match Played").SetValueAsync(_quickmatchplayed);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Quick Match Played is now updated
        }
    }

    private IEnumerator UpdateteammatchplayedDatabase(string _teammatchplayed)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Team Match Played").SetValueAsync(_teammatchplayed);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team Match Played is now updated
        }
    }

    private IEnumerator UpdatedeathmatchplayedDatabase(string _deathmatchplayed)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Deathmatchplayed").SetValueAsync(_deathmatchplayed);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team Match Played is now updated
        }
    }

    private IEnumerator UpdatenumberofmvpDatabase(string _numberofmvp)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Number of MVP").SetValueAsync(_numberofmvp);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team number of mvp is now updated
        }
    }

    private IEnumerator UpdatenumberofteamwinsDatabase(string _numberofwins)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Number of Team Match Wins").SetValueAsync(_numberofwins);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team Number of Team Match Wins is now updated
        }
    }

    private IEnumerator UpdatenumberoflosingDatabase(string _numberoflosing)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Number of Lose match").SetValueAsync(_numberoflosing);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team Number of Number of Lose match is now updated
        }
    }

    private IEnumerator UpdatepremangameplayedDatabase(string _premangameplayed)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Preman Game Played").SetValueAsync(_premangameplayed);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team Number of Preman Game Played is now updated
        }
    }

    private IEnumerator UpdatembahgameplayedDatabase(string _mbahgameplayed)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Mbah game Played").SetValueAsync(_mbahgameplayed);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team Number of Mbah game Played is now updated
        }
    }

    private IEnumerator UpdatealiengameplayedDatabase(string _aliengameplayed)
    {
        //Set the currently logged in user username in the database
        var DBTask = DBreference.Child("crazy-delivery-rumble-default-rtdb:").Child(User.UserId).Child("Alien Game Played").SetValueAsync(_aliengameplayed);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database Team Number of Alien Game Played is now updated
        }
    }

    #endregion
}
