﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using System;
using Photon.Pun;

public class UniversalSceneLoader : Singleton<UniversalSceneLoader>
{
    int CurrentScene;
    string mapname;
    Action _loadCallback;
    Action _UnloadCallback;
    AsyncOperation sceneOperation = null;

    public void LoadScene(string name)
    {
        CurrentScene = SceneManager.GetSceneByName(name).buildIndex;

        var g = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        sceneOperation = g;
    }

    public float LoadingProgress()
    {
        return sceneOperation.progress;
    }

    public bool isreloadlobbyanimation ( bool reload){
        return true;
    }

    public bool IsSceneActive(string Scene)
    {
        if (SceneManager.GetActiveScene().name == Scene)
        {
            return true;
        }

        return false;
    }

    public void PhotonLoadScene(string name)
    {
        CurrentScene = SceneManager.GetActiveScene().buildIndex;

        if (SceneManager.GetSceneByName(name).isLoaded)
        {
            return;
        }

        PhotonNetwork.LoadLevel(name, LoadSceneMode.Additive);
    }

    public void PhotonLoadScene(string name, Action callback)
    {
        CurrentScene = SceneManager.GetActiveScene().buildIndex;
        if (SceneManager.GetSceneByName(name).isLoaded)
        {
            //RemoveActiveScene();
            //SetActiveScene(name);
            //callback();
            return;
        }


        sceneOperation = null;

        _loadCallback = callback;

        //PhotonNetwork.LoadLevel(name, out sceneOperation, LoadSceneMode.Additive);
        sceneOperation.completed += LoadSceneCompleted;
    }

    internal void TransferObjectScene(GameObject gameObject, string v)
    {
        Scene _scene = SceneManager.GetSceneByName(v);
        if (_scene.isLoaded)
        {
            SceneManager.MoveGameObjectToScene(gameObject, _scene);
        }
    }

    private void RemoveActiveScene()
    {
        Scene _scene = SceneManager.GetActiveScene();
        UnloadScene(_scene.name);
    }

    public void PhotonLoadScene(int index)
    {
        CurrentScene = index;

        //PhotonNetwork.LoadLevel(index, out sceneOperation, LoadSceneMode.Additive);
    }

    public void PhotonLoadScene(int index, Action callback)
    {
        sceneOperation = null;
        CurrentScene = index;
        _loadCallback = callback;

        //PhotonNetwork.LoadLevel(index, out sceneOperation, LoadSceneMode.Additive);
        sceneOperation.completed += LoadSceneCompleted;
    }

    public void LoadScene(string name, Action callback)
    {
        CurrentScene = SceneManager.GetSceneByName(name).buildIndex;
        _loadCallback = callback;

        var g = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        sceneOperation = g;
        g.completed += LoadSceneCompleted;
    }

    public void LoadScene(int i)
    {
        CurrentScene = i;

        var g = SceneManager.LoadSceneAsync(i, LoadSceneMode.Additive);
        sceneOperation = g;
    }

    public void LoadScene(int i, Action callback)
    {
        CurrentScene = i;
        _loadCallback = callback;

        var g = SceneManager.LoadSceneAsync(i, LoadSceneMode.Additive);
        sceneOperation = g;
        g.completed += LoadSceneCompleted;
    }

    private void LoadSceneCompleted(AsyncOperation obj)
    {
        _loadCallback?.Invoke();
        obj.completed -= LoadSceneCompleted;
    }

    public void SetActiveScene(int i)
    {
        var g = SceneManager.GetSceneAt(i);
        if (g.isLoaded)
        {
            SceneManager.SetActiveScene(g);
        }
    }
    
    

    public void SetActiveScene(string name)
    {
        var g = SceneManager.GetSceneByName(name);
        if (g.isLoaded)
        {
            SceneManager.SetActiveScene(g);
        }
    }

    public bool IsSceneLoaded(string name)
    {
        var scenes = SceneManager.GetSceneByName(name);
        return scenes.isLoaded;
    }

    
    public void setmapname(string map) {
        Debug.Log("set map name with : " + map);
        mapname = map;
    }

    public string getmapname() {
        return this.mapname;
    }

    public bool IsSceneLoaded(int index)
    {
        var scenes = SceneManager.GetSceneByBuildIndex(index);
        return scenes.isLoaded;
    }

    public void UnloadScene(string name)
    {
        var g = SceneManager.UnloadSceneAsync(name);
        g.completed += UnloadSceneCompleted;
    }

    public void UnloadScene(string name, Action callback)
    {
        var g = SceneManager.UnloadSceneAsync(name);
        _UnloadCallback = callback;
        g.completed += UnloadSceneCompleted;
    }

    public void UnloadScene(int i)
    {
        var g = SceneManager.UnloadSceneAsync(i);
    }

    public void UnloadScene(int i, Action callback)
    {
        var g = SceneManager.UnloadSceneAsync(i);
        _UnloadCallback = callback;
        g.completed += UnloadSceneCompleted;
    }

    private void UnloadSceneCompleted(AsyncOperation obj)
    {
        _UnloadCallback?.Invoke();
        obj.completed -= UnloadSceneCompleted;
    }

    internal Scene GetActiveScene()
    {
        return SceneManager.GetActiveScene();
    }
}
