﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using Cinemachine;
using UnityEngine.Audio;
using UnityStandardAssets.CrossPlatformInput;
using System;
using UnityEngine.AddressableAssets;

public class GameSettingHandler : MonoBehaviour
{
    [SerializeField] AudioData AudioList;
    [SerializeField] GameSettingsObject m_GameSettings;
    [SerializeField] CinemachineVirtualCamera VirtualPlayerCam;
    [SerializeField] AssetReference GameSettingsReference;
    [SerializeField] GameObject BGScreen;
    [SerializeField] AudioMixer Mixer;
    Volume PostProcessingVolume;
    Camera MainCamera;
    GameObject GameSettingsInstance;

    private void Start()
    {
        PostProcessingVolume = GetComponent<Volume>();
        MainCamera = Camera.main;
        InitApplySettings();
        UniversalHandler.Instance.InitGameSettings(this);
        UniversalHandler.Instance.SetBGScreen(BGScreen);

        //UniversalSoundHandler.Instance.MenuPlayBGM.Invoke("BGM_MainMenu_1");
    }

    public GameSettingsObject GetGameSettings()
    {
        return m_GameSettings;
    }

    private void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Setting Button"))
        {
            SettingState(true);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) || CrossPlatformInputManager.GetButtonDown("Done Setting Button"))
        {
            SettingState(false);
            ApplySettings();
        }

    }

    private void SettingState(bool t)
    {
        UniversalSoundHandler.Instance.MenuPlaySFX2.Invoke(t ? "SFX_ButtonClick_1" : "SFX_ButtonBack_1");

        if (GameSettingsInstance == null)
        {
            //spawn first.
            UniversalAddresableLoader.Instance.Spawn(GameSettingsReference, Vector3.zero, null, OnDoneSpawn);
        }
        else
        {
            GameSettingsInstance.SetActive(t);
        }
    }

    private void OnDoneSpawn(GameObject obj)
    {
        GameSettingsInstance = obj;
        GameSettingsInstance.GetComponent<GameSettingsInstanceHandler>().SetGameOptions(m_GameSettings, this);
    }

    public bool IsAutoPickup()
    {
        return m_GameSettings.m_GameOptions.AutoPickUp;
    }

    float ConvertRange(float Val, float OldMin, float newMin, float newMax, float oldMax)
    {
        return (((Val - OldMin) * newMax) / oldMax) + newMin;
    }

    void InitApplySettings()
    {
        //set Volume
        Mixer.SetFloat("MasterVol", Mathf.Log(m_GameSettings.m_GameOptions.MasterVolume) * 20);
        Mixer.SetFloat("BGMVol", Mathf.Log(m_GameSettings.m_GameOptions.BGMVolume) * 20);
        Mixer.SetFloat("SFXVol", Mathf.Log(m_GameSettings.m_GameOptions.SFXVolume) * 20);
        Mixer.SetFloat("VAVol", Mathf.Log(m_GameSettings.m_GameOptions.VoiceVolume) * 20);

        //set texture quality.
        QualitySettings.SetQualityLevel((int)m_GameSettings.m_GameOptions.Quality);

        //set shared postprocessing profile.
        List<VolumeComponent> myComponent = PostProcessingVolume.sharedProfile.components;

        Debug.Log((int)m_GameSettings.m_GameOptions.PostProcessingQuality);
        PostProcessingVolume.sharedProfile = m_GameSettings.GetVolumeProfile((int)m_GameSettings.m_GameOptions.PostProcessingQuality);

        //set draw distance
        VirtualPlayerCam.m_Lens.FarClipPlane = m_GameSettings.m_GameOptions.MaxDrawDistance;

        //enable AA.
        MainCamera.GetComponent<UniversalAdditionalCameraData>().antialiasing = m_GameSettings.m_GameOptions.AntiAliasing ? AntialiasingMode.FastApproximateAntialiasing : AntialiasingMode.None;

        //individual postprocessing. more to come.
        myComponent.Find(x => x.name.Equals("Bloom")).active = m_GameSettings.m_GameOptions.Bloom;
    }

    public void ApplySettings()
    {
        //set Volume
        Mixer.SetFloat("MasterVol", Mathf.Log(m_GameSettings.m_GameOptions.MasterVolume) * 20);
        Mixer.SetFloat("BGMVol", Mathf.Log(m_GameSettings.m_GameOptions.BGMVolume) * 20);
        Mixer.SetFloat("SFXVol", Mathf.Log(m_GameSettings.m_GameOptions.SFXVolume) * 20);
        Mixer.SetFloat("VAVol", Mathf.Log(m_GameSettings.m_GameOptions.VoiceVolume) * 20);

        //set texture quality.
        QualitySettings.SetQualityLevel((int)m_GameSettings.m_GameOptions.Quality);

        //set shared postprocessing profile.
        List<VolumeComponent> myComponent = PostProcessingVolume.sharedProfile.components;

        Debug.Log((int)m_GameSettings.m_GameOptions.PostProcessingQuality);
        PostProcessingVolume.sharedProfile = m_GameSettings.GetVolumeProfile((int)m_GameSettings.m_GameOptions.PostProcessingQuality);

        //set draw distance
        VirtualPlayerCam.m_Lens.FarClipPlane = m_GameSettings.m_GameOptions.MaxDrawDistance;

        //enable AA.
        MainCamera.GetComponent<UniversalAdditionalCameraData>().antialiasing = m_GameSettings.m_GameOptions.AntiAliasing ? AntialiasingMode.FastApproximateAntialiasing : AntialiasingMode.None;

        //individual postprocessing. more to come.
        myComponent.Find(x => x.name.Equals("Bloom")).active = m_GameSettings.m_GameOptions.Bloom;
    }

    //dummy
    public void DoDisconnect()
    {
        Photon.Pun.PhotonNetwork.Disconnect();
    }



}
