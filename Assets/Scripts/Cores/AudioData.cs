﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Sounds Settings", menuName = "CDS/Sounds Settings")]
public class AudioData : ScriptableObject
{
    public List<AudioFiles> SoundList;
}
