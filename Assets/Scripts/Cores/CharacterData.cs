﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Character Data", menuName = "CDS/Character Data")]
public class CharacterData : ScriptableObject
{
    //[SerializeField] CharacterDataStruct MyData;
    //[SerializeField] public Sprite CharAttribute;
    //[SerializeField] public Sprite CharSkill;

    [SerializeField] List<CharacterDataStruct> Characters;

    public CharacterDataStruct GetMyData(int i)
    {
        return Characters.Find(x => x.CharId == i);
    }
}


[System.Serializable]
public struct CharacterDataStruct
{
    public int CharId;
    public string CharName;
    public string CharDesc;
    public string SkillName;
    public CharacterAttributes Attributes;
    [Range(0f, 1f)] public float TopSpeed, Accel, Handling;

    public Sprite CharAttribute;
    public Sprite CharSkill;
}

public enum CharacterAttributes
{
    Offensive,
    Defensive,
    Utility
}

